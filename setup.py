#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from setuptools import setup, find_packages

from os import path

here = path.abspath(path.dirname(__file__))

setup(
    name='icms-common',
    version='0.3',
    description = 'Scripts and library methods used by new iCMS web applications',
    long_description = 'Scripts and library methods used by new iCMS web applications',
    url='https://gitlab.cern.ch/cms-icmsweb/icms-common/',
    author='iCMS-support',
    author_email='icms-support@cern.ch',
    license='?',
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 5 - Beta',

        # Indicate who your project is intended for
        'Intended Audience :: Developers',
        'Topic :: Software Development',

        # Pick your license as you wish (should match "license" above)
        'License :: OSI Approved :: MIT License',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        'Programming Language :: Python :: 3.7',
    ],
    keywords='CERN iCMS',
    packages = ['icmscommon', 'icmsutils'],
    install_requires = ['psycopg2', 'Alchy', 'python-ldap', 'pytz'],
    entry_points={}
)

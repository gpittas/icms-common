from email_templates.email_template import EmailTemplateBase


class EmailTemplateEprDueCorrection(EmailTemplateBase):
    _sender = 'icms-support@cern.ch'
    _recipients = '{recipient_email}'
    _subject = '[EPR] EPR due {action} applied'
    _cc = '{team_leader_emails}'
    _bcc = 'icms-support@cern.ch, cms-secretariat@cern.ch, cms-engagement-office@cern.ch'
    _source_app = 'common'
    _body = u'''Dear Colleagues,

The requested EPR {action} has been applied, please see below for the details. 

For any questions please contact the team of the Engagement Office.

Details of {action}:
- affected person: {in_favour_of}
- for EPR year: {year}
- EPR amount: {amount}
- reason for {action}: {reason}
- remarks: {remarks}


Best regards,
iCMS mailing system'''

    def __init__(self, recipient, recipient_email, team_leader_emails, year, amount, reason, remarks, sender_email=None, action='correction'):
        super(EmailTemplateEprDueCorrection, self).__init__(
            in_favour_of=recipient,
            recipient_email=recipient_email,
            team_leader_emails=team_leader_emails,
            year=year,
            amount=amount,
            reason=reason,
            remarks=remarks,
            action=action
        )
        if sender_email:
            self._sender = sender_email


class EmailTemplateEprDueReduction(EmailTemplateEprDueCorrection):

    def __init__(self, recipient, recipient_email, team_leader_emails, year, amount, reason, remarks, sender_email=None):
        super().__init__(recipient, recipient_email, team_leader_emails, year, amount, reason, remarks, sender_email, action='reduction')


class EmailTemplateEprDueIncrease(EmailTemplateEprDueCorrection):
    def __init__(self, recipient, recipient_email, team_leader_emails, year, amount, reason, remarks,
                 sender_email=None):
        super().__init__(recipient, recipient_email, team_leader_emails, year, amount, reason, remarks, sender_email, action='increase')
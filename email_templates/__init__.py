#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from .al_emails import EmailTemplateAuthorListAddPerson
from .auth_app_emails import EmailTemplateAutoSuspension

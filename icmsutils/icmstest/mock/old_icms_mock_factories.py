from icmsutils.timelinesutils import year_fraction_left
from datetime import datetime as dt
from icms_orm.cmspeople import Person, PersonHistory, Institute, MemberActivity, User, PersonCernData, MoData, PeopleFlagsAssociation, Flag as DbFlag
from icmsutils.businesslogic.flags import Flag
from icms_orm.epr import Category, TimeLineUser as TLU, Pledge, Task
from icms_orm.metadata import ChangeLog, OperationValues
from icmsutils.businesslogic import authorship
from .base_classes import MockFactory
from icms_orm.epr import CMSActivity as EprActivity, Project
import sqlalchemy as sa
import string
import random, datetime
import numbers
from sqlalchemy import inspect


def make_mock_person(cms_id, inst_code, activity_name, status_cms='CMS'):
    p = Person()
    p.cmsId = cms_id
    p.instCode = inst_code
    p.activity = MemberActivity()
    p.activity.name = activity_name
    p.status = status_cms
    p.dateCreation = datetime.datetime(1990, 1, 1, 0, 0)
    return p


def make_mock_time_line_user(cms_id, ic, d_ap=0.0, d_au=0.0, is_au=False, is_sus=False, stat='CMS', cat=1, ts=None,
                             yf=None, year=None):

    ts = ts or dt.now()
    yf = yf or year_fraction_left(ts)
    year = year or ts.year
    return {
        TLU.instCode: ic,
        TLU.dueApplicant: d_ap,
        TLU.dueAuthor: d_au,
        TLU.isAuthor: is_au,
        TLU.cmsId: cms_id,
        TLU.status: stat,
        TLU.category: cat,
        TLU.isSuspended: is_sus,
        TLU.comment: 'test!',
        TLU.yearFraction: yf,
        TLU.year: year,
        TLU.timestamp: ts
    }


def make_mock_pledge(overrides):
    pledge = Pledge(**dict(taskId=None, userId=None, instId=None, workTimePld=None, workTimeAcc=None, workTimeDone=None,
                status=None, year=None, workedSoFar=0, isGuest=False))
    for key, value in overrides.items():
        setattr(pledge, hasattr(key, 'key') and key.key or key, value)
    return pledge


def make_mock_task(overrides):
    fake_act = type('SmallNothing', (object, ), {'id': None, 'tasks': []})()
    setattr(fake_act, 'id', None)
    task = Task(name=None, desc=None, act=fake_act, needs=None, pctAtCERN=None, tType=None, comment=None, code=None,
                year=2015, level=1, parent='', shiftTypeId=-1, earliestStart='', latestEnd='', status='ACTIVE',
                locked=False, kind='CORE')
    for k, v in overrides.items():
        setattr(task, hasattr(k, 'key') and k.key or k, v)
    return task


def make_mock_activity(overrides):
    pass


def make_mock_project(overrides):

    pass


def icms_inst(code, status, name, country, date_cms_in=None):
    return Institute.from_ia_dict({
        Institute.code: code,
        Institute.cmsStatus: status,
        Institute.name: name,
        Institute.country: country,
        Institute.dateCmsIn: date_cms_in
    })


def icms_user(cms_id, hr_id, first_name, last_name, status, inst_code, activity_id, suspended, author, login, created,
              pri_enabled=True, ph_access=True, zh_flag=True):
    person = Person.from_ia_dict({
        Person.cmsId: cms_id,
        Person.hrId: hr_id,
        Person.firstName: first_name,
        Person.lastName: last_name,
        Person.status: status,
        Person.instCode: inst_code,
        Person.activityId: activity_id,
        Person.isAuthorSuspended: suspended,
        Person.isAuthor: author,
        Person.loginId: login,
        Person.niceLogin: login,
        Person.dateCreation: created,
        Person.priEnabled: pri_enabled,
        Person.physicsAccess: ph_access,
        Person.zhFlag: zh_flag
    })
    history = PersonHistory.from_ia_dict({PersonHistory.cmsId: cms_id, PersonHistory.history: ''})
    return person, history


class MockInstFactory(MockFactory):
    _defaults = {
        Institute.cmsStatus: 'YES',
        Institute.dateCreate: datetime.date(year=1990, month=1, day=1),
        Institute.dateCmsIn: None,
    }

    @classmethod
    def create_instance(cls, instance_overrides=None):
        mock = cls._create_basic_mock(instance_overrides, Institute, cls._defaults)
        if not getattr(mock, Institute.code.key):
            setattr(mock, Institute.code.key, cls._pick_institute_code())
        return mock

    @classmethod
    def _pick_institute_code(cls):
        return ''.join([l(string.ascii_uppercase) for l in ([lambda x: random.choice(x)] * random.randint(5, 8))])


class MockFlagFactory(MockFactory):
    @classmethod
    def create_instance(cls, instance_overrides={}, person=None, flag_id=None):
        mock = cls._create_basic_mock(instance_overrides=instance_overrides, mocked_class=PeopleFlagsAssociation, defaults={})
        if person:
            mock.set(PeopleFlagsAssociation.person, person)
        if flag_id:
            mock.set(PeopleFlagsAssociation.flagId, flag_id)
        return mock

    @classmethod
    def create_all(cls):
        db_flags = []
        _used_flag_ids = set()
        for field_name in dir(Flag):
            if field_name.isupper() and field_name[0] != '_':
                flag_id = getattr(Flag, field_name)
                if flag_id not in _used_flag_ids:
                    _used_flag_ids.add(flag_id)
                    db_flags.append(DbFlag.from_ia_dict({DbFlag.id: flag_id, DbFlag.desc: field_name}))
        return db_flags


class MockPersonFactory(MockFactory):
    __used_logins = dict()
    _names_f = 'Alice;Jane;Kate Mary;Anne Rose;Helen;Tracy;Sarah;Elisabeth;Lea;Anne Marie;Rose Mary'.split(';')
    _names_m = 'John;Matthew;Lucas;Peter;Alan;Thomas;Paul;Steve;Andrew;Dennis;Donald;Lee;Kurt Donald;Brian Adam'\
        ';John Jack;Kevin Patrick;Ping Pong'.split(';')
    _surnames = 'Smith Gordon McDonald Andersen Trumpet Doe Kowalski Schmidt Miller Tailor Crown Ivanov Dupont Boer' \
                ' Wei Nowak Tamer Singer Ferreira Souza'.split(' ')

    _defaults = {
        Person.status: 'CMS',
        Person.instCode: None,
        Person.isAuthorSuspended: False,
        Person.isAuthor: False,
        Person.dateCreation: datetime.date(year=2010, month=1, day=1),
        Person.priEnabled: True,
        Person.physicsAccess: True,
        Person.zhFlag: True
    }

    @classmethod
    def _next_cms_id(cls):
        return cls.__get_next_static_counter('__next_cms_id_val', 0, 1)

    @classmethod
    def _next_hr_id(cls):
        return cls.__get_next_static_counter('__next_hr_id_val', 1000, 11)

    @classmethod
    def reset_static_counters(cls):
        for key in ['__next_cms_id_val', '__next_hr_id_val']:
            setattr(cls, key, 0)

    @classmethod
    def __get_next_static_counter(cls, field_name, start_value=0, increment=1):
        setattr(cls, field_name, getattr(cls, field_name, start_value) + increment)
        return getattr(cls, field_name)

    @classmethod
    def _pick_last_name(cls):
        return random.choice(cls._surnames)

    @classmethod
    def _pick_first_name(cls, gender=None):
        if gender and gender.lower()[0] == 'm':
            return random.choice(cls._names_m)
        elif gender and gender.lower()[0] == 'f':
            return random.choice(cls._names_f)
        return random.choice(cls._names_m + cls._names_f)

    @classmethod
    def create_instance(cls, instance_overrides=None):
        person = cls._create_basic_mock(instance_overrides, Person, cls._defaults)

        assert(person is not None)

        if not person.cmsId:
            person.cmsId = cls._next_cms_id()
        if ( ( getattr(person, Person.hrId.key) is None ) or
               not (getattr(person, Person.hrId.key) > 0 ) ):
            person.hrId = cls._next_hr_id()
        if not person.firstName:
            person.firstName = cls._pick_first_name()
        if not person.lastName:
            person.lastName = cls._pick_last_name()
        if not person.loginId:
            login = base_login = ('%s%s' % (person.firstName[0], person.lastName[0:min(len(person.lastName), 7)])).lower()
            if base_login in cls.__used_logins:
                login = '%s%d' % (base_login, cls.__used_logins[base_login])
            cls.__used_logins[base_login] = cls.__used_logins.get(base_login, 0) + 1
            person.loginId = login
        if not Person.niceLogin:
            person.niceLogin = person.loginId


        person.loginId = person.loginId or person.niceLogin
        person.niceLogin = person.niceLogin or person.loginId

        person.history = PersonHistory.from_ia_dict({PersonHistory.cmsId: person.cmsId, PersonHistory.history: ''})
        return person


class MockUserFactory(MockFactory):

    _defaults = {}

    @classmethod
    def create_instance(cls, instance_overrides=None, person=None):
        mock = cls._create_basic_mock(instance_overrides=instance_overrides, mocked_class=User, defaults=cls._defaults)
        if person:
            mock.cmsId = person.cmsId
            mock.sex = 'F' if person.firstName in MockPersonFactory._names_f else 'M'
        return mock


class MockPersonMoFactory(MockFactory):

    _defaults = {x: 'NO' for x in MoData.ia_list() if ('Ic' not in x.key and 'Fa' not in x.key and ('Mo' in x.key or 'mo' in x.key))}

    # _defaults = {MoData.freeMo2006: 'NO'}

    @classmethod
    def create_instance(cls, instance_overrides=None, person=None):
        mock = cls._create_basic_mock(instance_overrides=instance_overrides, mocked_class=MoData, defaults=cls._defaults)
        if person:
            mock.cmsId = person.cmsId
        return mock


class MockActivityFactory(MockFactory):

    _default_raw_data = [(1, 'Administrative'), (9, 'Doctoral Student'), (2, 'Engineer'), (17, 'Engineer Electronics'),
                 (18, 'Engineer Mechanical'), (3, 'Engineer Software'), (13, 'Non-Doctoral Student'), (5, 'Other'),
                 (6, 'Physicist'), (15, 'Technician'), (7, 'Theoretical Physicist')]

    _default_iter = iter(_default_raw_data)

    @classmethod
    def create_instance(cls, instance_overrides=None):
        """
        Will keep creating the old activities unless provided some override. Once it runs out of examples, will keep returning None
        :param instance_overrides:
        :return:
        """
        data = instance_overrides
        if not data:
            try:
                default = cls._default_iter.__next__()
                data = {MemberActivity.id: default[0], MemberActivity.name: default[1]}
            except StopIteration as stop:
                return None
        return cls._create_basic_mock(data, MemberActivity, {})

    @classmethod
    def create_all(cls):
        stuff = []
        try:
            _rd_iter = iter(cls._default_raw_data)
            while True:
                act_id, act_name = _rd_iter.__next__()
                stuff.append(
                    cls._create_basic_mock({MemberActivity.id: act_id, MemberActivity.name: act_name}, MemberActivity, {}))
        except StopIteration as stop:
            pass
        return stuff


class MockChangeLogFactory(MockFactory):

    _defaults = {ChangeLog.operation: OperationValues.UPDATE}

    @classmethod
    def create_instance(cls, instance_overrides=None, master_object=None):
        data = {}
        instance_overrides = instance_overrides or {}
        if master_object:
            info = inspect(master_object)
            data[ChangeLog.ref_schema] = info.mapper.local_table.schema
            data[ChangeLog.ref_table] = info.mapper.local_table.name
            pkey_vals = info.identity_key[1]
            if len(pkey_vals) == 1 and isinstance(pkey_vals[0], numbers.Number):
                data[ChangeLog.ref_id] = pkey_vals[0]
            else:
                data[ChangeLog.ref_hash_id] = sa.func.md5(sa.func.concat_ws('|', *pkey_vals))
            if ChangeLog.last_update not in instance_overrides:
                data[ChangeLog.last_update] = datetime.datetime.now()

        data.update(instance_overrides)
        return cls._create_basic_mock(data, mocked_class=ChangeLog, defaults=cls._defaults)


class MockEprProjectFactory(MockFactory):
    _defaults = {Project.code: 'MOCK_PROJECT_2015', Project.year: 2015, Project.name: 'Mock Project', Project.maxTaskLevel: 1,
                 Project.status: 'ACTIVE'}

    @classmethod
    def create_instance(cls, instance_overrides=None):
        mock = cls._create_basic_mock(instance_overrides=instance_overrides, mocked_class=Project, defaults=cls._defaults)
        mock.set(Project.code, mock.get(Project.code).replace('2015', str(mock.get(Project.year))))
        return mock


class MockEprActivityFactory(MockFactory):
    _defaults = {EprActivity.code: 'MOCK_ACTIVITY_2015', EprActivity.year: 2015, EprActivity.name: 'Mock Activity',
                 EprActivity.project: None, EprActivity.status: 'ACTIVE'}

    @classmethod
    def create_instance(cls, instance_overrides=None):
        mock = cls._create_basic_mock(instance_overrides=instance_overrides, mocked_class=EprActivity,
                                      defaults=cls._defaults)
        mock.set(EprActivity.code, mock.get(EprActivity.code).replace('2015', str(mock.get(EprActivity.year))))
        if mock.get(EprActivity.project) is None:
            mock.set(EprActivity.project, MockEprProjectFactory.get_default_instance())
        return mock


class MockEprTaskFactory(MockFactory):
    _defaults = {Task.code: 'MOCK_TASK_2015', Task.year: 2015, Task.name: 'A Mock Task', Task.tType: 'HardWork',
                 Task.shiftTypeId: -1, Task.level: 1, Task.status: 'ACTIVE', Task.locked: False, Task.kind: 'CORE'}

    @classmethod
    def create_instance(cls, instance_overrides=None):
        task = cls._create_basic_mock(instance_overrides, Task, cls._defaults)
        task.set(Task.code, task.get(Task.code).replace('2015', str(task.get(Task.year))))
        if task.get(Task.activity) is None:
            task.set(Task.activity, MockEprActivityFactory.get_default_instance())
        return task


# TODO: probably the stuff below can be removed, after checking the usages etc
activity_names = ['Engineer', 'Non-Doctoral Student', 'Doctoral Student', 'Physicist', 'Other']
eng, stu, doc, phy, oth = range(1, 6)


def setup_capacities(db):
    # setup activities: they don't need the same fixed numbers but let's make sure their iCMS\EPR IDs don't match
    for i, n in enumerate(activity_names, start=1):
        db.session.add(MemberActivity.from_ia_dict({MemberActivity.id: i, MemberActivity.name: n}))
    for idx, n in enumerate(reversed(activity_names), start=1):
        _c = Category(n, n in authorship.get_primary_authorship_eligible_capacity_names())
        _c.id = idx
        db.session.add(_c)
    db.session.commit()

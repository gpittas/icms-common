from sqlalchemy.inspection import inspect

class MockFactory(object):

    _overrides = dict()
    _default_instance = None
    _mocked_class = None
    _defaults = dict()

    @classmethod
    def create_instance(cls, instance_overrides=None):
        if cls._mocked_class:
            return cls._create_basic_mock(instance_overrides)
        raise NotImplementedError('To be implemented by subclasses!')

    @classmethod
    def reset_overrides(cls, mocked_class=None):
        if mocked_class:
            cls._overrides[mocked_class] = dict()
        else:
             cls._overrides = dict()

    @classmethod
    def set_overrides(cls, mocked_class, overrides):
        cls._overrides[mocked_class] = cls._overrides.get(mocked_class, dict())
        cls._overrides[mocked_class].update(overrides)

    @classmethod
    def _create_basic_mock(cls, instance_overrides, mocked_class=None, defaults=None):
        mocked_class = mocked_class or cls._mocked_class
        defaults = defaults or cls._defaults
        params = dict(defaults)
        params.update(cls._overrides.get(mocked_class, dict()))
        if instance_overrides:
            params.update(instance_overrides)
        mock = mocked_class.from_ia_dict(params)
        return mock

    @classmethod
    def _init_default_value(cls):
        cls._default_instance = cls.create_instance()

    @classmethod
    def get_default_instance(cls):
        """
        :return: Returns a default object whenever one is needed but is not otherwise provided
        """
        if not cls._default_instance:
            cls._init_default_value()
        else:
            state = inspect(cls._default_instance)
            if state.detached:
                # todo: most likely the entry is no longer in the DB. It should however be checked in a more robust way
                cls._init_default_value()

        return cls._default_instance
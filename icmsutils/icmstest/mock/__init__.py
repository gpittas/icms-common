from .old_icms_mock_factories import *
from .new_icms_mock_factories import *
from .old_notes_mock_factories import *

from datetime import date, datetime
mock_date_param_name = 'mock_date'


class FakeDateTimeProvider(object):
    def __init__(self):
        self._datetime = datetime.now()
    
    def now(self):
        return self._datetime

    def today(self):
        return self._datetime.date()

    def set_date(self, new_date: date):
        new_date = new_date or date.today()
        self._datetime = datetime(new_date.year, new_date.month, new_date.day, self._datetime.hour, self._datetime.minute, self._datetime.second)

    def set_time(self, new_datetime: datetime):
        self._datetime = datetime

_provider = FakeDateTimeProvider()

def today():
    return _provider.today()

def set_mock_date(new_date: date):
    _provider.set_date(new_date)
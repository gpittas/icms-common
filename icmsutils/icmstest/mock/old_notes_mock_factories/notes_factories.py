from icms_orm.old_notes import Note, NoteFile, NoteAuthors, NoteCategoriesRel, NoteCategory, NoteReferees
from icmsutils.icmstest.mock.base_classes import MockFactory
from datetime import date, datetime


class MockOldNoteFactory(MockFactory):
    _mocked_class = Note
    _defaults = {Note.modificationDate: date.today(), Note.submitDate: date.today()}


class MockOldNoteFileFactory(MockFactory):
    _mocked_class = NoteFile
    _defaults = {NoteFile.modifDate: date.today()}


class MockOldNoteAuthorsFactory(MockFactory):
    _mocked_class = NoteAuthors
    _defaults = dict()


class MockOldNoteRefereesFactory(MockFactory):
    _mocked_class = NoteReferees
    _defaults = dict()


class MockOldNoteCategoryFactory(MockFactory):
    _mocked_class = NoteCategory
    _default = dict()


class MockOldNoteCategoriesRelFactory(MockFactory):
    _mocked_class = NoteCategoriesRel
    _default = dict()


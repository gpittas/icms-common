from icmscommon import ConfigGovernor as ConGov, DbGovernor


def count_in_db(ent_class, filters_dict, lambdas_list=None):
    q = DbGovernor.get_db_manager().session.query(ent_class)
    for _key, _val in filters_dict.items():
        q = q.filter(_key == _val)
    for _lambda in lambdas_list or []:
        q = _lambda(q)
    return q.count()


def assert_attr_val(ent_class, filters_dict, assertions_dict, msg='', db_fn=lambda: DbGovernor.get_db_manager(), lambdas_list=None):
    """
    example: assert_attr_value(Person, {Person.cmsId: 9981}, {Person.lastName: 'Bawej'},
    :param ent_class: mapper class so that the correct DB table can be queried
    :param filters_dict: a dict of {attr: value} format to pick the right DB entry
    :param assertions_dict: format like above, but these will be checked against the entry retrieved from the DB
    :param msg: something to print in case the assertion fails
    :param db_fn: usually nothing needs to be provided here as most likely the db object will have been imported by
     the time this method is called and therefore the default will do just fine resolving the variable
    :param lambdas: lambdas to be applied on a query in a q=fn(q) fashion
    :return: nothing, hopefully
    """
    records = db_fn().session.query(ent_class)
    for key, val in filters_dict.items():
        records = records.filter(key == val)
    for _lambda in lambdas_list or []:
        records = _lambda(records)
    records = records.all()
    query_params_string = ' and '.join(['%s=%s' % (key.key, str(val)) for key, val in filters_dict.items()])
    for record in records:
        for attr, expected in assertions_dict.items():
            actual = getattr(record, attr.key)
            assert actual == expected, msg or 'Expected value of %s was %s but found %s for query params %s' % \
                                              (attr.key, str(expected), str(actual), query_params_string)
    if not records:
        raise AssertionError('No records of type %s found for query params: %s' % (str(ent_class.__name__), query_params_string))


def close_enough(x, y, abs_epsilon, rel_epsilon=None):
    diff = abs(x - y)
    if diff < abs_epsilon:
        return True
    elif rel_epsilon is not None and 2 * diff / (x + y) < rel_epsilon:
        return True
    return False
# from icmsutils.icmstest.assertables import *
# from icmsutils.icmstest.ddl import *
# from icmsutils.icmstest.functions import *
from icmscommon import DbGovernor, ConfigGovernor


def _check_test_config_enabled():
    """
    Will raise an exception whenever this package is imported and config is not set for testing.
    """
    if not ConfigGovernor.check_test_mode():
        raise RuntimeError('Testing option disabled in the config. Please enable it if you are indeed trying to run the '
                           'tests. Otherwise please neither use nor import this package.')

# RUN IT AS SOON AS THE INTERPRETER CAN - THIS CODE SHOULD ONLY RUN OR BE IMPORTED DURING TESTS.
_check_test_config_enabled()
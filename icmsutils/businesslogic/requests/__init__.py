from .requests_manager import RequestsManager
from .base_classes import BaseRequestProcessor, BaseRequestExecutor
from .exceptions import *

from .person_status_requests import PersonStatusChangeExecutor, PersonStatusChangeProcessor


class RequestNames(object):
    CHANGE_PERSON_STATUS = 'CHANGE_PERSON_STATUS'
    CHANGE_PERSON_ACTIVITY = 'CHANGE_PERSON_ACTIVITY'


RequestsManager.register_request_type_name_with_processor_and_executor(
    request_type_name=RequestNames.CHANGE_PERSON_STATUS,
    processor_class=PersonStatusChangeProcessor,
    executor_class=PersonStatusChangeExecutor
)

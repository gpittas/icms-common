from .base_classes import BaseRequestProcessor, BaseRequestExecutor
from icms_orm.cmspeople import Person

from icmsutils.businesslogic.flags import Flag
from icmsutils.datatypes import ActorData
from icmsutils.prehistory import PersonAndPrehistoryWriter as Writer
from icms_orm.common import LegacyFlag, InstituteLeader, Affiliation, Request, RequestStep, RequestStepStatusValues
import sqlalchemy as sa


class PersonStatusChangeProcessor(BaseRequestProcessor):

    def __init__(self, request):
        super(PersonStatusChangeProcessor, self).__init__(request)
        self.s = LegacyFlag.session()

    def get_cms_ids_allowed_to_act(self, action=None, step=None):
        """
        Just returning the CMS IDs of all the SECR members
        """
        return {_r[0] for _r in self.s.query(LegacyFlag.cms_id).filter(sa.and_(
            LegacyFlag.end_date == None, LegacyFlag.flag_code == Flag.ICMS_SECR
        )).all()}

    def create_next_steps(self, actor_data, action=None):
        # create the approval step for secretariat upon request's creation
        if action == BaseRequestProcessor.Action.CREATE:
            return [RequestStep.from_ia_dict({
                RequestStep.request_id: self.request.id,
                RequestStep.creator_cms_id: actor_data.cms_id,
                RequestStep.status: RequestStepStatusValues.PENDING,
                RequestStep.deadline: None,
                RequestStep.title: 'CMS Secretariat Approval',
                RequestStep.remarks: None
            })]
        return []

    def can_create(self, actor_data):
        """
        Checking if person in question is the team leader for the member whose status is about to be changed
        """
        assert isinstance(actor_data, ActorData)
        return self.s.query(InstituteLeader.cms_id).join(Affiliation, InstituteLeader.inst_code == Affiliation.inst_code).\
            filter(sa.and_(
                Affiliation.end_date == None,
                InstituteLeader.end_date == None,
                Affiliation.is_primary == True,
                Affiliation.cms_id == self._cms_id,
                InstituteLeader.cms_id == actor_data.cms_id
        )).count() > 0

    @property
    def _cms_id(self):
        return self.request.get(Request.processing_data)['cms_id']

    @property
    def _creator_cms_id(self):
        return self.request.get(Request.processing_data)['creator_cms_id']

    @property
    def _new_status(self):
        return self.request.get(Request.processing_data)['new_status']


class PersonStatusChangeExecutor(BaseRequestExecutor):
    def __init__(self, cms_id, creator_cms_id, new_status):
        self.cms_id = cms_id
        self.creator_cms_id = creator_cms_id
        self.new_status = new_status

    def execute(self):
        people = {p.get(Person.cmsId): p for p in Person.session().query(Person).filter(Person.cmsId.in_({self.cms_id, self.creator_cms_id})).all()}
        writer = Writer(db_session=Person.session(), subject_person=people.get(self.cms_id), actor_person=people.get(self.creator_cms_id))
        writer.set_new_value(Person.status, self.new_status)
        writer.apply_changes(do_commit=True)

from icmsutils.prehistory import PersonAndPrehistoryWriter
from icms_orm.cmspeople.people import Person

class Flag(object):
    MISC_AUTHORYES = 'MISC_authoryes'
    MISC_AUTHORNO = 'MISC_authorno'
    MISC_AUTHORNOMO = 'MISC_authornomo'
    EXECUTIVE_BOARD = 'COM_eb'
    FINANCE_BOARD = 'COM_fb'
    FINANCE_BOARD_EXTENDED = 'COM_fball'
    MANAGEMENT_BOARD = 'COM_mb'
    SPOKESPERSON_AND_DEPUTIES = 'COM_sp'
    ICMS_ADMIN = 'ICMS_admin'
    ICMS_ROOT = 'ICMS_root'
    PROJECT_MANAGER = 'MISC_pm'
    AUTHOR_NO = 'MISC_authorno'
    INST_REP = 'CB_cbi'
    INST_DEP = 'CB_cbd'
    ICMS_SECR = 'ICMS_secr'
    PUBC_MEMBER = 'PUBC_MEMBER'
    ENGAGEMENT_OFFICE = 'ENGT_OFFICE'
    AUTHORSHIP_BOARD = 'COM_ab'


def has_flag(flag_code, person):
    return flag_code in {f.id for f in getattr(person, Person.flags.key)}


def has_any_flag(flag_codes, person):
    owned_codes = {f.id for f in getattr(person, Person.flags.key)}
    for flag_code in flag_codes:
        if flag_code in owned_codes:
            return True
    return False


def has_all_flags(flag_codes, person):
    owned_codes = {f.id for f in getattr(person, Person.flags.key)}
    for flag_code in flag_codes:
        if flag_code not in owned_codes:
            return False
    return True


def set_flag(flag_code, person, actor, do_commit=True):
    writer = PersonAndPrehistoryWriter(person.query.session, person, actor)
    writer.grant_flag(flag_code)
    writer.apply_changes(do_commit=do_commit)

def unset_flag(flag_code, person, actor, do_commit=True):
    writer = PersonAndPrehistoryWriter(person.query.session, person, actor)
    writer.revoke_flag(flag_code)
    writer.apply_changes(do_commit=do_commit)
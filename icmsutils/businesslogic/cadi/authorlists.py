import logging

from icms_orm.cmsanalysis import PaperAuthorHistory as PAH, PaperAuthorHistoryActions as PAHActions, PaperAuthor, Analysis
from icms_orm.cmsanalysis import PaperAuthor as PA, PaperAuthorStatusValues
from icms_orm.cmspeople import Person, User, MemberActivity, re
from datetime import datetime, date
from icmsutils.exceptions import CadiException, AuthorListException
import sqlalchemy as sa

# todo: authorizations are NOT IMPLEMENTED AT ALL!!!!!


def mark_in_db_as_having_al_files_generated(code, actor, date_override=None, defer_commit=False):
    # todo: exists only for client code's compatibility. Find uses, refactor, remove!
    m = AuthorListModel(code=code, actor=actor, date_override=date_override, defer_commits=defer_commit)
    m.close_author_list()


def mark_in_db_as_closed(code, actor, date_override=None, defer_commit=False):
    # todo: exists only for client code's compatibility. Find uses, refactor, remove!
    m = AuthorListModel(code=code, actor=actor, date_override=date_override, defer_commits=defer_commit)
    m.close_author_list()


def add_to_author_lists(new_author, codes, actor, date_override=None, defer_commit=False):
    """
    :param new_author: Person instance (or a mock with name, first name, inst code, cms id and hr id set)
    :param codes: a collection of AN codes
    :param actor: Person instance (or a mock, like for the new_author)
    :param date_override: can be provided if the date needs to be specific and different than present
    :param defer_commit: leaves the session open, allowing for further changes or a rollback in the client code
    :return:
    """
    assert isinstance(new_author, Person)
    # just in case we got a single code only
    if isinstance(codes, str):
        codes = [codes]

    ssn = PA.session()
    existing_entries = {e.get(PA.code): e for e in
                        ssn.query(PA).filter(PA.cmsid == new_author.get(Person.cmsid)).filter(PA.code.in_(codes)).all()}
    already_on = set()
    for entry in existing_entries.values():
        if entry.get(PA.status) in {PaperAuthorStatusValues.IN, PaperAuthorStatusValues.IN_NEW}:
            already_on.add(entry.get(PA.code))
    if already_on:
        ssn.rollback()
        raise CadiException(
            '%d already on the author list for %s.' % (new_author.get(Person.cmsId), ', '.join(already_on)))
    for code in codes:
        ssn.add(PA.from_ia_dict({
            PA.cmsid: new_author.get(Person.cmsId),
            # needs to be a string:
            PA.hrid: new_author.get(Person.hrId),
            PA.code: code,
            PA.nameSignature: AuthorListModel.get_sign_name(new_author.firstName, new_author.lastName, new_author.user.get(User.nameSignature)),
            PA.status: PaperAuthorStatusValues.IN_NEW,
            PA.statusCms: new_author.get(Person.status),
            PA.activity: new_author.activity.get(MemberActivity.name),
            PA.instCode: new_author.get(Person.instCode),
            PA.instCodeNow: new_author.get(Person.instCodeNow),
            PA.instCodeAlso: None,
            PA.instCodeForced: None,
            PA.infn: new_author.get(Person.infn),
            PA.univOther: new_author.get(Person.univOther),
            PA.inspireid: new_author.get(Person.authorId),
            PA.name: new_author.get(Person.lastName),
            PA.namf: new_author.get(Person.firstName),
        }))


def generate_db_author_list(code, actor, date_override=None, defer_commit=False):
    # todo: mark the AL as open in the remote file... but that will not work from icms-tools (no config.ini), so
    # we'll need to rely on the daemon to create it!
    m = AuthorListModel(code=code, actor=actor)
    m.open_author_list()


class AuthorListModel(object):
    """
    A class encapsulating all the AL operations: creation, closing, adding, removing, swapping authors etc.
    """
    def __init__(self, code, actor, resync_after_commits=False, defer_commits=False, date_override=False):
        self._ssn = Person.session()
        self._code = code
        self._actor = isinstance(actor, Person) and actor or self._ssn.query(Person).filter(Person.cmsId == actor).one()
        self._history = []
        self._authors = {}
        # setting to True so that the initial sync can be performed regardless of the flag value
        self._resync_after_commits = True
        self._resync_if_set_to_do_so()
        self._resync_after_commits = resync_after_commits
        self._the_date = date_override or date.today()
        self._defer_commits = defer_commits

    @property
    def db_fmt_date(self):
        return self._the_date.strftime('%d/%m/%Y')

    def _resync_if_set_to_do_so(self):
        """
        Updates the model after DB transactions. Not needed for stateless environments, like webpages.
        """
        if self._resync_after_commits:
            self._history = self._ssn.query(PAH).filter(PAH.code == self._code).all()
            self._authors = {int(e.get(PaperAuthor.cmsid)): e for e in
                             self._ssn.query(PaperAuthor).filter(PaperAuthor.code == self._code).all()}

    def _get_personal_details(self, filters_dict=None, filters_lambdas=None):
        """
        Takes care of querying the source DB and reformatting the output as a dictionary
        :param filters_dict: for simple filters to be applied in the "1 and 2 and 3 and ..." fashion
        :param filters_lambdas: for anything more elaborate - to be applied in a q=fn(q) fashion
        :return:
        """
        q_cols = [Person.cmsId, Person.instCode, Person.firstName, Person.lastName, User.nameSignature, Person.status,
                  MemberActivity.name, Person.hrId, Person.instCodeNow, Person.instCodeOther, Person.infn,
                  Person.authorId,
                  Person.univOther]
        q = self._ssn.query(*q_cols).join(User, Person.cmsId == User.cmsId). \
            join(MemberActivity, MemberActivity.id == Person.activityId)
        for _key, _val in (filters_dict or {}).items():
            q = q.filter(_key == _val)
        return [{col: val for col, val in zip(q_cols, row)} for row in q.all()]

    def __perform_epilogue(self):
        """Meant to be private: commits (if instructed to at creation), re-syncs (if instructed to)"""
        if not self._defer_commits:
            self._ssn.commit()
            self._resync_if_set_to_do_so()

    def __perform_add_authors_to_existing_al(self, people_data, action):
        authors_in = self._authors
        for person_data in people_data:
            cms_id = person_data.get(Person.cmsId)
            pa = authors_in.get(cms_id)
            if pa:
                if pa.get(PaperAuthor.status) in {PaperAuthorStatusValues.IN, PaperAuthorStatusValues.IN_NEW}:
                    logging.debug('Author {cms_id} already on the list for {code}. Skipping.'.
                                  format(cms_id=cms_id, code=self._code))
                    continue
                else:
                    # set the author status depending on the presently executed action
                    pa.set(PaperAuthor.status, {
                        PAHActions.ADD_MEMBER: PaperAuthorStatusValues.IN,
                        PAHActions.MOVE_MEMBER_IN: PaperAuthorStatusValues.IN,
                        PAHActions.SWAP_MEMBER_IN: PaperAuthorStatusValues.IN_NEW
                    }.get(action))
            else:
                pa = PaperAuthor.from_ia_dict({
                    PaperAuthor.cmsid: person_data.get(Person.cmsId),
                    PaperAuthor.hrid: person_data.get(Person.hrId),
                    PaperAuthor.code: self._code,
                    PaperAuthor.nameSignature: self.get_sign_name(*[person_data.get(x) for x in [Person.firstName, Person.lastName, User.nameSignature]]),
                    PaperAuthor.status: PaperAuthorStatusValues.IN,
                    PaperAuthor.statusCms: person_data.get(Person.status),
                    PaperAuthor.activity: person_data.get(MemberActivity.name),
                    PaperAuthor.instCode: person_data.get(Person.instCode),
                    PaperAuthor.instCodeNow: person_data.get(Person.instCodeNow) or '',
                    PaperAuthor.instCodeAlso: person_data.get(Person.instCodeOther) or '',
                    PaperAuthor.instCodeForced: None,
                    PaperAuthor.infn: person_data.get(Person.infn),
                    PaperAuthor.univOther: person_data.get(Person.univOther),
                    PaperAuthor.inspireid: person_data.get(Person.authorId),
                    PaperAuthor.name: person_data.get(Person.lastName),
                    PaperAuthor.namf: person_data.get(Person.firstName)
                })
            self._ssn.add(pa)

    @staticmethod
    def get_sign_name(first_name, last_name, sign_name):
        return sign_name or '{0} {1}'.format(re.sub(r'[a-z ]+', '.', first_name), last_name)

    def open_author_list(self):
        if self.is_open():
            raise AuthorListException('Author list for %s is already open!' % self._code)
        elif self.is_closed():
            raise AuthorListException('Author list for %s is already closed!' % self._code)

        ssn, code, actor = self._ssn, self._code, self._actor

        _data = self._get_personal_details(filters_dict={Person.isAuthor: True})

        self.__perform_add_authors_to_existing_al(people_data=_data, action=PAHActions.LIST_GEN)

        pah = pah = PAH.from_ia_dict({
            PAH.cmsid: str(actor.get(Person.cmsId)),
            PAH.hrid: str(actor.get(Person.hrId)),
            PAH.instCode: actor.get(Person.instCode),
            PAH.code: code,
            PAH.action: PAHActions.LIST_GEN,
            PAH.history: 'Author List generated for ancode %s' % code,
            PAH.date: self.db_fmt_date,
            PAH.nicelogin: actor.get(Person.loginId)
        })
        ssn.add(pah)
        self.__perform_epilogue()

    def swap_authors(self, incoming_cms_id, leaving_cms_id):
        if not self.is_open():
            raise AuthorListException('Author list for %s is NOT open!' % self._code)
        pass

    def has_author(self, cms_id, only_active=True):
        if cms_id in self._authors:
            if not only_active:
                return True
            elif self._authors.get(cms_id).get(PaperAuthor.status) in \
                    {PaperAuthorStatusValues.IN_NEW, PaperAuthorStatusValues.IN}:
                return True
        return False

    def add_author(self, incoming_cms_id):
        if not self.is_open():
            raise AuthorListException('Author list for %s is NOT open!' % self._code)
        if self.has_author(incoming_cms_id, only_active=False):
            raise AuthorListException('Author list for %s already mentions %d' % (self._code, incoming_cms_id))
        _data = self._get_personal_details(filters_dict={Person.cmsId: incoming_cms_id})
        self.__perform_add_authors_to_existing_al(people_data=_data, action=PAHActions.ADD_MEMBER)

        for _entry in _data:
            inst_code = _entry.get(Person.instCode)
            sign_name = self.get_sign_name(*[_entry.get(x) for x in [Person.firstName, Person.lastName, User.nameSignature]])
            # todo: add a mailer and insert the correct email address. Till then let it be as it is here.
            email = None

            self._ssn.add(PAH.from_ia_dict({
                PAH.history: '{sign_name} added to {inst_code} (mail sent to {email})'.format(inst_code=inst_code,
                        sign_name=sign_name, email=email),
                PAH.code: self._code,
                PAH.date: self.db_fmt_date,
                PAH.action: PAHActions.ADD_MEMBER,
                PAH.cmsid: self._actor.get(Person.cmsId),
                PAH.hrid: self._actor.get(Person.hrId),
                PAH.instCode: inst_code,
                PAH.nicelogin: self._actor.get(Person.loginId)
            }))

        self.__perform_epilogue()

    def add_external_author(self, author_data):
        if not self.is_open():
            raise AuthorListException('Author list for %s is NOT open!' % self._code)
        pass

    def remove_author(self, leaving_cms_id):
        if not self.is_open():
            raise AuthorListException('Author list for %s is NOT open!' % self._code)
        pass

    def close_author_list(self):
        if not self.is_open():
            raise AuthorListException('Author list for %s is NOT open!' % self._code)
        ssn, actor = self._ssn, self._actor
        ssn.add(PAH.from_ia_dict({
            PAH.history: 'Author List files generated for ancode %s' % self._code,
            PAH.code: self._code,
            PAH.date: self.db_fmt_date,
            PAH.action: PAHActions.LIST_CLOSE,
            PAH.cmsid: actor.get(Person.cmsId),
            PAH.hrid: actor.get(Person.hrId),
            PAH.instCode: actor.get(Person.instCode),
            PAH.nicelogin: actor.get(Person.loginId)
        }))
        self.__perform_epilogue()

    @property
    def _history_actions(self):
        return {pah.get(PAH.action) for pah in self._history}

    def is_open(self):
        """
        A list is open once it's generated in the DB and remains so until it gets closed.
        """
        return PAHActions.LIST_GEN in self._history_actions and not self.is_closed()

    def is_closed(self):
        """
        A list is closed once files are generated and no more modifications are therefore allowed.
        """
        return PAHActions.LIST_CLOSE in self._history_actions

    @classmethod
    def add_to_all_open_author_lists(cls, author_data, actor):
        pass
    # sa.func.min(sa.func.str_to_date(PAH.date, '%d/%m/%Y')).label('opened_on')

    @classmethod
    def _get_al_status_map(cls):
        q_cols = [PAH.code,
                  sa.case(
                      [(sa.func.max(sa.case([(PAH.action == PAHActions.LIST_CLOSE, 1)], else_=0)) == 1, 'closed')],
                      else_='open').label('status')
                  ]
        q = PAH.session().query(*q_cols)
        q = q.filter(PAH.action.in_({PAHActions.LIST_CLOSE, PAHActions.LIST_GEN}))
        q = q.group_by(PAH.code)
        return {code: status for code, status in q.all()}

    @classmethod
    def get_open_author_list_codes(cls):
        return {code for code, status in cls._get_al_status_map().items() if status == 'open'}

    @classmethod
    def get_closed_author_list_codes(cls):
        return {code for code, status in cls._get_al_status_map().items() if status == 'closed'}


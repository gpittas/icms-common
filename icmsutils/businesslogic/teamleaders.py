from icms_orm.cmspeople import Person, Institute, PeopleFlagsAssociation
from datetime import timedelta, date
from icmsutils.businesslogic.flags import Flag
from icmsutils.exceptions import IcmsInsufficientRightsException, IcmsException
from icmsutils.prehistory import PersonAndPrehistoryWriter
import sqlalchemy as sa
import logging, traceback


def members_epr_change_suspension(db_session, suspendable_cms_ids, actor_cms_id, up_or_down):
    """
    :param db_session:
    :param suspendable_cms_ids:
    :param actor_cms_id:
    :param up_or_down: true or false, true to suspend
    :return:
    """
    try:
        people = db_session.query(Person).filter(
            sa.or_(Person.cmsId.in_(suspendable_cms_ids), Person.cmsId==actor_cms_id)).all()
        actor = None
        for person in people:
            if person.cmsId == actor_cms_id:
                actor = person
                break
        for person in people:
            if person.cmsId in suspendable_cms_ids:
                un_suspend_person(db_session, subject_person=person, actor_person=actor, up_or_down=up_or_down)
        db_session.commit()
    except Exception as e:
        db_session.rollback()
        logging.warning(traceback.format_exc())
        raise e


def un_suspend_person(db_session, subject_person, actor_person, up_or_down):

    if subject_person.isAuthor:
        raise IcmsException('Cannot change suspension status of an active author!')

    for ts in [subject_person.dateAuthorUnsuspension, subject_person.dateAuthorSuspension]:
        if ts and date.today() - timedelta(days=7) > ts > date.today() - timedelta(days=28):
            raise IcmsException('Suspension status changed too recently!')

    if not can_change_suspension_for(db_session, actor=actor_person, subject=subject_person):
        raise IcmsInsufficientRightsException('Only Team Leaders and admins can do that!')

    if getattr(subject_person, Person.isAuthorSuspended.key) != up_or_down:
        writer = PersonAndPrehistoryWriter(db_session=db_session, actor_person=actor_person, subject_person=subject_person)
        writer.set_new_value(Person.isAuthorSuspended, up_or_down)
        writer.apply_changes(do_commit=False)


def can_change_suspension_for(db_session, actor, subject):
    authorized_cms_ids = db_session.query(Institute.cbiCmsId, Institute.cbdCmsId, Institute.cbd2CmsId).filter(Institute.code == subject.instCode).one()
    if actor.cmsId in authorized_cms_ids:
        return True
    else:
        admins = {r[0] for r in db_session.query(PeopleFlagsAssociation.cmsId).filter(PeopleFlagsAssociation.flagId == Flag.ICMS_ADMIN).all()}
    return actor.cmsId in admins
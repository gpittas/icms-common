from icmsutils.domain_object_model.people import Person
from icmsutils.domain_object_model.institutes import Institute


class VotingEntity(object):
    def __init__(self, name: str, country: str):
        self._name = name
        self._country = country

    @property
    def name(self) -> str:
        return self._name

    @property
    def country(self) -> str:
        return self._country


class VotingInstitute(VotingEntity):
    def __init__(self, institute: Institute):
        self._name = institute.code
        self._country = institute.country
        self._institute = institute

    @property
    def institute(self) -> Institute:
        return self._institute


class VotingCluster(VotingEntity):
    def __init__(self, *institutes: Institute):
        self._institutes = institutes

    @property
    def name(self) -> str:
        return ' & '.join([_i.name for _i in self.institutes])

    @property
    def country(self) -> str:
        return ' & '.join({_i.country for _i in self.institutes})

    @property
    def institutes(self):
        return tuple(self._institutes)

    def __str__(self) -> str:
        return '&'.join([_i.code for _i in self.institutes])


class VotingCountry(VotingEntity):
    def __init__(self, country: str):
        self._name = None
        self._country = country


class VotingParticipant(object):
    def __init__(self, person: Person, voting_entity: VotingEntity, votes: int = 1, remarks: str = None,
                 delegate: Person = None):
        self._person = person
        self._delegate = delegate
        self._voting_entity = voting_entity
        self._remarks = remarks
        self._votes = votes

    @property
    def person(self) -> Person: return self._person

    @property
    def voting_entity(self) -> VotingEntity: return self._voting_entity

    @property
    def votes(self) -> int: return self._votes

    @property
    def remarks(self) -> str: return self._remarks

    @property
    def delegate(self) -> Person: return self._delegate

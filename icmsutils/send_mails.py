

'''Common code for sending emails from/to CERN via its SMTP servers for all new iCMS services.
'''

__author__ = 'Miguel Ojeda'
__copyright__ = 'Copyright 2017, CERN CMS'
__credits__ = ['Miguel Ojeda', 'Andreas Pfeiffer', 'Tomasz Adrian Bawej']
__license__ = 'Unknown'
__maintainer__ = 'Andreas Pfeiffer'
__email__ = 'andreas.pfeiffer@cern.ch'

#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import logging
import smtplib
import socket
import os
import time
import json

from email.mime.text import MIMEText
import email.header

from threading import Thread


class Message:
    """
    A helper class to collect all the info for an eMail message
    """
    def __init__(self):
        # set some defaults:
        self.fromAddress  = None
        self.toAddresses  = None
        self.ccAddresses  = None
        self.bccAddresses = None
        self.subject = ''
        self.body = ''

    def isValid(self):
        # define valid message to have at least a "to", "from", and "body" ... also a "subject" ?
        return (self.toAddresses is not None and
                self.fromAddress is not None and
                self.body is not None and self.body.strip() is not '' )

    def __repr__(self):
        # to print the content of the message
        # return self.toXML()
        return json.dumps( json.loads(self.toJson()), indent=4 ).replace('\\n', '\n')

    def toXML(self):

        retVal = '<message>'
        content = json.loads( self.toJson() )
        for item in ['from', 'to', 'cc', 'bcc', 'subject', 'body']:
            if item in content and content[item]:
                retVal += '\n   <%s> %s </%s>' % (item, content[ item ], item)
            else:
                if item in ['subject', 'body']:
                    retVal += '\n   <%s> [no %s] </%s>' % (item, item, item)
        retVal += "\n</message>"
        return retVal

    def toJson(self):
        # provide info in json format
        content = { 'from' : self.fromAddress,
                    'to' : self.toAddresses,
                    'cc' : self.ccAddresses,
                    'bcc' : self.bccAddresses,
                    'subject'  : self.subject,
                    'body' : self.body
                    }
        return json.dumps(content)

    def fromJson(self, jsonString):
        # re-set self from json string

        content = json.loads(jsonString)
        self.fromAddress = content['from']
        self.toAddresses = content['to']
        self.ccAddresses = content['cc']
        self.bccAddresses = content['bcc']
        self.subject = content['subject']
        self.body = content['body']


def send_email_async(fromAddr, toAddrs, subject, msgBody, ccAddrs=None, bccAddrs=None):
    """
    send the email asynchronuous, not waiting for the thread to finish

    :param fromAddr: a single address to use for the "from" field.
    :param toAddrs:  a list of email addresses for the "to" field
    :param subject:  the subject of the mail
    :param msgBody:  the body of the mail
    :param ccAddrs:  optional: a list of email addresses for the "CC" field
    :param bccAddrs: optional: a list of email addresses for the "BCC" field
    :return: the thread object of the actual sending thread
    """

    if not toAddrs:
        logging.warning('send_email> no addressees given (subj:%s), not sending mail ... ' % subject)
        return

    msg = Message()

    msg.subject = subject

    msg.fromAddress = fromAddr
    msg.toAddresses = toAddrs if type(toAddrs) == type(list()) else [ toAddrs ]
    if ccAddrs  : msg.ccAddresses  = ccAddrs  if type(ccAddrs)  == type(list()) else [ ccAddrs ]
    if bccAddrs : msg.bccAddresses = bccAddrs if type(bccAddrs) == type(list()) else [ bccAddrs ]

    msg.body = msgBody

    if not msg.isValid():
        logging.warning('send_email_async> invalid message found (%s), not sending mail ... ' % str(msg) )
        return None

    thr = Thread(target=send_async_email, args=[msg])
    thr.start()
    return thr


def send_email(fromAddr, toAddrs, subject, msgBody, ccAddrs=None, bccAddrs=None):
    """
    send the email directly, waiting for the process to finish

    :param fromAddr: a single address to use for the "from" field.
    :param toAddrs:  a list of email addresses for the "to" field
    :param subject:  the subject of the mail
    :param msgBody:  the body of the mail
    :param ccAddrs:  optional: a list of email addresses for the "CC" field
    :param bccAddrs: optional: a list of email addresses for the "BCC" field
    :return: the return code of the actual sending
    """

    if not toAddrs:
        logging.warning('send_email> no addressees given (subj:%s), not sending mail ... ' % subject)
        return

    msg = Message()

    msg.subject = subject

    msg.fromAddress = fromAddr
    msg.toAddresses = toAddrs if type(toAddrs) == type(list()) else [ toAddrs ]
    if ccAddrs  : msg.ccAddresses  = ccAddrs  if type(ccAddrs)  == type(list()) else [ ccAddrs ]
    if bccAddrs : msg.bccAddresses = bccAddrs if type(bccAddrs) == type(list()) else [ bccAddrs ]

    msg.body = msgBody

    if not msg.isValid():
        logging.warning('send_email_async> invalid message found (%s), not sending mail ... ' % str(msg) )
        return None

    return SMTP().sendEmail(msg.subject, msg.body, msg.fromAddress, msg.toAddresses, msg.ccAddresses, msg.bccAddresses)


# the callback used by the thread to send the actual mail:
def send_async_email( msg ):
    SMTP().sendEmail(msg.subject, msg.body, msg.fromAddress, msg.toAddresses, msg.ccAddresses, msg.bccAddresses)


class SMTP(object):
    '''Class used for sending emails from/to CERN via its SMTP servers.
    '''

    def __init__(self, password = None, server = None):
        '''If password is None, the anonymous SMTP server, cernmx.cern.ch will be used.
        Otherwise, the normal SMTP server, smtp.cern.ch, will be used.

        The server can also be overriden if it is not None.

        STARTTLS authentication will be used if password is not None.

        Note that the anonymous SMTP server has restrictions, see:
        https://espace.cern.ch/mmmservices-help/ManagingYourMailbox/Security/Pages/AnonymousPosting.aspx
        '''

        self.password = password

        if self.password is None:
            self.server = 'cernmx.cern.ch'
        else:
            self.server = 'smtp.cern.ch'

        if server is not None:
            self.server = server


    def __str__(self):
        return 'SMTP %s' % self.server


    def sendEmail(self, subject, body, fromAddress, toAddresses, ccAddresses = (), bccAddresses = ()):
        '''Sends an email.

        Note that toAddresses and [b]ccAddresses are lists of emails.
        '''

        logging.debug('%s: Email from %s with subject %s: Preparing...', self, fromAddress, repr(subject))
        text = MIMEText(body.decode() if isinstance(body, bytes) else body)
        text['Subject'] = email.header.Header( subject )
        text['From'] = fromAddress

        allAddrs = set(toAddresses)
        text['To'] = ', '.join(toAddresses)
        if ccAddresses and len(ccAddresses) > 0:
            text['Cc'] = ', '.join( ccAddresses )
            allAddrs |= set( ccAddresses )
        if bccAddresses and len(bccAddresses) > 0:
            text['Bcc'] = ', '.join( bccAddresses )
            allAddrs |= set( bccAddresses )

        logging.debug('%s: Email from %s with subject %s: Connecting...', self, fromAddress, repr(subject))

        smtp = smtplib.SMTP(self.server)
        if self.password is not None:
            logging.debug('%s: Email from %s with subject %s: Logging in...', self, fromAddress, repr(subject))
            smtp.starttls()
            smtp.ehlo()
            smtp.login(fromAddress, self.password)

        logging.debug('%s: Email from %s with subject %s: Sending...', self, fromAddress, repr(subject))
        smtp.sendmail(fromAddress, allAddrs, text.as_string())

        smtp.quit()

def test():
    # _test(send_email)
    # _test(send_email_async)

    msg = Message()
    msg.fromAddress = 'andreas.pfeiffer@cern.ch'
    msg.toAddresses = ['andreas.pfeiffer@cern.ch', 'apfeiffer1@gmail.com']
    print(msg)

    msg.ccAddresses = ['foo@bar.com']
    msg.bccAddresses = ['bar@foo.com']
    msg.subject = 'json-test - what else '
    msg.body = 'json test body .... '

    j0 = msg.toJson()

    msg1=Message()
    msg1.fromJson( j0 )

    assert (msg.fromAddress == msg1.fromAddress)
    assert (msg.toAddresses == msg1.toAddresses)
    assert (msg.ccAddresses== msg1.ccAddresses)
    assert (msg.bccAddresses== msg1.bccAddresses)
    assert (msg.subject == msg1.subject)
    assert (msg.body == msg1.body)

    print(msg)
    print(msg1)

def _test(sendFunc):
    sendFunc( fromAddr='andreas.pfeiffer@cern.ch',
                toAddrs='andreas.pfeiffer@cern.ch',
                subject='test-subject 0',
                msgBody='a test, what else ... str/none/none',
                ccAddrs=None,
                bccAddrs=None )

    sendFunc( fromAddr='andreas.pfeiffer@cern.ch',
                toAddrs=['andreas.pfeiffer@cern.ch'],
                subject='test-subject 1 ',
                msgBody='a test, what else ... list/string/none',
                ccAddrs='andreas.pfeiffer@cern.ch',
                bccAddrs=None )

    sendFunc( fromAddr='andreas.pfeiffer@cern.ch',
                toAddrs='andreas.pfeiffer@cern.ch',
                subject='test-subject 2',
                msgBody='a test, what else ... string/list/list1',
                ccAddrs=['andreas.pfeiffer@cern.ch'],
                bccAddrs=['apfeiffer1@gmail.com'] )

    sendFunc( fromAddr='andreas.pfeiffer@cern.ch',
                toAddrs='andreas.pfeiffer@cern.ch',
                subject='test-subject 3',
                msgBody=None)
    sendFunc( fromAddr='andreas.pfeiffer@cern.ch',
                toAddrs='andreas.pfeiffer@cern.ch',
                subject='test-subject 4',
                msgBody='  ')
    sendFunc( fromAddr='andreas.pfeiffer@cern.ch',
                toAddrs='andreas.pfeiffer@cern.ch',
                subject=None,
                msgBody='')

if __name__ == '__main__':
    test()

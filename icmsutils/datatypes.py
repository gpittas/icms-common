#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from icms_orm.cmspeople import Person, User, MoData
from icms_orm.common import Person as NuPerson


class ActorData(object):
    """
    A wrapper object for the actor info that often needs to be passed to business logic methods
    """
    def __init__(self, cms_id=None, old_db_person=None, old_db_user=None, old_db_mo=None, new_db_person=None):
        if not cms_id and not isinstance(old_db_person, Person) and not isinstance(new_db_person, NuPerson):
            raise Exception('ActorData object instantiation attempted without a CMS ID or a Person object.')
        elif not cms_id:
            cms_id = old_db_person and old_db_person.get(Person.cmsId) \
                     or new_db_person and new_db_person.get(NuPerson.cms_id)
        self._cms_id = cms_id
        self._old_db_person = old_db_person
        self._old_db_user = old_db_user
        self._old_db_mo = old_db_mo
        self._new_db_person = new_db_person

    def __init_old_db_data(self):
        self._old_db_person, self._old_db_user, self._old_db_mo = Person.session().query(Person, User, MoData).\
            join(User, Person.cmsId == User.cmsId).join(MoData, Person.cmsId == MoData.cmsId).\
            filter(Person.cmsId == self.cms_id).one()

    def __init_new_db_data(self):
        self._new_db_person = NuPerson.session().query(NuPerson).filter(NuPerson.cms_id == self._cms_id).one()

    @property
    def cms_id(self):
        if not self._cms_id:
            if not self._new_db_person:
                self.__init_new_db_data()
        return self._cms_id

    @property
    def old_db_person(self):
        if not self._old_db_person:
            self.__init_old_db_data()
        return self._old_db_person

    @property
    def old_db_mo_data(self):
        if not self._old_db_mo:
            self.__init_old_db_data()
        return self._old_db_mo

    @property
    def old_db_user(self):
        if not self._old_db_user:
            self.__init_old_db_data()
        return self._old_db_user

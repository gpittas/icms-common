#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import re

PATTERN_EMAIL = r'[\w\.\!\#\$\%\&\'\*\+\-\/\=\?\^\_\`\{\|\}\~]+@[\w\.-]+\.\w+'


def extract_all_email_addresses(some_text):
    return some_text and re.findall(PATTERN_EMAIL, some_text) or None

class PrehistoryException(Exception):
    pass


class NoRegistrationLineException(PrehistoryException):
    pass


class MultipleRegistrationLineException(PrehistoryException):
    pass


class RegistrationIncongruenceException(PrehistoryException):
    pass
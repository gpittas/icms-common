from datetime import datetime as dt, date as date, datetime
from icms_orm.cmspeople import PersonHistory, Person, MemberActivity, PERSON_STATUS_CMS
import logging
from icms_orm.common import PrehistoryTimeline
from icmsutils.prehistory import PrehistoryParser, PrehistoryException


class PrehistoryModel(object):
    def __init__(self, cms_id, person=None, history=None):
        """
        Constructs a PrehistoryModel instance for given person
        :param cms_id: CMS ID of the subject
        :param person: (optional) when provided (with history) taken as is without querying the DB
        :param history: (optional) when provided (with person) taken as is without querying the DB
        """
        self._person, self._history = person and history and (person, history) or Person.session(). \
            query(Person, PersonHistory).join(PersonHistory, Person.cmsId == PersonHistory.cmsId). \
            filter(Person.cmsId == cms_id).one()

        assert isinstance(self._history, PersonHistory), 'History object is an instance of %s' % history.__class__.__name__
        assert isinstance(self._person, Person)

        pp = PrehistoryParser(history_string=self._history.get(PersonHistory.history), person_obj=self._person)
        self._timelines = []
        self._exception = None
        try:
            for time_line in pp.yield_timelines():
                # let's skip the 0-length timelines as they must be surrounded by something more meaningful
                if time_line.get(PrehistoryTimeline.end_date) != time_line.get(PrehistoryTimeline.start_date):
                    self._timelines.append(time_line)
            if len(self._timelines) == 0:
                raise PrehistoryException('No timelines extracted for CMS ID %d' % cms_id)
        except PrehistoryException as pe:
            self._exception = pe

    def is_tainted(self):
        return self._exception is not None

    def get_prehistory_time_lines(self):
        return self._timelines

    def get_last_effective_arrival_date(self, ignore_absences_up_to=730):
        earliest_in = None
        # tainted history means that generating timelines stopped at some point so we cannot use them as reliable
        # approximation of the most recent entry date
        if not self.is_tainted():
            for tl in self._timelines:
                if (lambda s: s and s.startswith('CMS'))(tl.get(PrehistoryTimeline.status_cms)):
                    earliest_in = tl.get(PrehistoryTimeline.start_date)
                elif earliest_in and (earliest_in - tl.start_date).days > ignore_absences_up_to:
                    return earliest_in
        return earliest_in or (lambda x: isinstance(x, datetime) and x.date() or x)(
            self._person.get(Person.dateCreation))

    def get_authorship_application_start_date(self):
        fail_message = None
        if self._person.get(Person.isAuthorSuspended):
            fail_message = 'is suspended!'
        elif not MemberActivity.does_activity_name_allow_signing(self._person.activity.get(MemberActivity.name)):
            fail_message = 'current activity [%s] does not go together with authorship' % self._timelines[0].get(PrehistoryTimeline.activity_cms)
        elif self._person.get(Person.status) != PERSON_STATUS_CMS:
            fail_message = 'not a CMS member'
        if fail_message:
            logging.debug('%d is not an authorship applicant: %s' % (self._person.cmsId, fail_message))
            return None

        best_guess_yet = (lambda x: isinstance(x, dt) and x.date() or x)(self._person.dateAuthorUnsuspension or self._person.dateCreation)

        # here we only look for the last favorable(*) activity change [* wrt authorship application]
        for i in range(0, len(self._timelines) - 1):
            now, before = [self._timelines[n] for n in [i, i+1]]
            if now.activity_cms != before.activity_cms and \
                    MemberActivity.does_activity_name_trigger_application(now.activity_cms) and \
                    not MemberActivity.does_activity_name_trigger_application(before.activity_cms):
                best_guess_yet = now.get(PrehistoryTimeline.start_date)
                break
        # cross-checking our best guess that with the last effective arrival date and the unsuspension date
        _guesses = [best_guess_yet, self.get_last_effective_arrival_date(), self._person.dateAuthorUnsuspension]
        return max([isinstance(x, datetime) and x.date() or x for x in _guesses if x])

    @property
    def person(self):
        return self._person

    @property
    def history(self):
        return self._history


from icms_orm.cmspeople import MemberActivity


class ActivityResolver(object):
    __cache = dict()

    @staticmethod
    def name_by_id(id):
        if id not in ActivityResolver.__cache:
            row = MemberActivity.session().query(MemberActivity.name).filter(MemberActivity.id == id).one_or_none()
            ActivityResolver.__cache[id] = (row or ['UNKNOWN ACTIVITY #%d' % id])[0]
        return ActivityResolver.__cache.get(id)

from datetime import datetime
import logging
import math


def make_histograms_for_periods(samples, cutoff, months_per_partition, bucket_bounds):
    """
    Creates a set of histograms, one per time-period
    :param samples: all the data samples, tuples like (id, time_start, time_end)
    :param cutoff:
    :param months_per_partition:
    :param bucket_bounds:
    :return:
    """
    logging.debug('We have %d samples to sort out' % len(samples))
    partitions_count = int(math.ceil(((datetime.now() - cutoff).days) / 30.0 / months_per_partition))
    logging.debug('Will use %d partitions' % partitions_count)
    data_partitions = [[] for x in range(partitions_count)]
    histograms = []
    for code, started, ended in samples:
        if started and started > cutoff:
            selector = (started - cutoff).days / 30 / months_per_partition
            data_partitions[selector].append((code, started, ended))
    for partition in data_partitions:
        histograms.append(divide_into_buckets(partition, bucket_bounds))
    return histograms


def divide_into_buckets(data, bucket_bounds):
    buckets = [[] for x in range(len(bucket_bounds) + 1)]
    for code, started, ended in data:
        elapsed = (ended - started).seconds
        bucket_no = len(bucket_bounds)
        for i, border in enumerate(bucket_bounds):
            if border > elapsed:
                bucket_no = i
                break
        buckets[bucket_no].append(code)
    return buckets


def print_paritions_and_buckets(partitions, bucket_bounds, bucket_label_fn = lambda value: str(value)):
    for buckets in partitions:
        buckets_total = sum(len(x) for x in buckets)
        print((' | '.join(['[%s]: %03d (%04.01f%%)' % (i < len(bucket_bounds) and bucket_label_fn(bucket_bounds[i]) or '+', len(bucket), 100.0 * len(bucket) / buckets_total) for i, bucket in
                           enumerate(buckets)])))


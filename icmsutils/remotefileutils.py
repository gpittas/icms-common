#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from subprocess import Popen, PIPE


class SshFileTools(object):

    """
    ssh icmsbox 'cat /data/iCMSfiles/an/activepapers.properties'
    ssh icmsbox 'sed -i "s/EXO-17-003=open/EXO-17-003=closed/g" /data/iCMSfiles/an/activepapers.properties'
    """

    @staticmethod
    def read_file(host, path):
        p = Popen(['ssh', host, 'cat %s' % path], stdin=PIPE, stdout=PIPE, stderr=PIPE)
        output, err = p.communicate()
        if p.returncode == 0:
            return output
        else:
            raise Exception(err)

    @staticmethod
    def replace_in_file(host, path, needle, replacement):
        p = Popen(['ssh', host,  'sed -i "s/%s/%s/g"' % (needle, replacement), path], stdin=PIPE, stdout=PIPE, stderr=PIPE)
        output, err = p.communicate()
        if p.returncode == 0:
            return output
        else:
            raise Exception(err)


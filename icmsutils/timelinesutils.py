#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import abc
from icms_orm.cmspeople import Person, MemberActivity, Institute, PersonHistory
from icms_orm.epr import EprInstitute as EprInst, EprUser, Category, TimeLineInst, TimeLineUser
from datetime import date, datetime
import logging as logger
from .businesslogic import servicework as srvwrk, authorship
from sqlalchemy import desc

from icmsutils.businesslogic.authorship.queries import determine_authorship_application_start_date
from icmsutils.wrappers import CachedMap


def session_wrapper(db_session=None):
    """
    It's just a storage box for session object so that it's not passed to every method that ends up calling a method needing session
    Side-product of moving the code from icms-timelines project
    :param db_session:
    :return:
    """
    __key = '__the_session'
    if db_session:
        setattr(session_wrapper, __key, db_session)
    return hasattr(session_wrapper, __key) and getattr(session_wrapper, __key) or None


def split_timeline(older, newer):
    """
    TODO: should changing the activity/status affect previously calculated due??
    :param older: TimeLineUser
    :param newer: TimeLineUser
    :return: a collection of objects to be committed to database
    """

    if older.timestamp.toordinal() > newer.timestamp.toordinal():
        temp = older
        older = newer
        newer = temp
        logger.warning('Warning: new time (%s) line passed as the old (%s) and vice versa. Swapping!', newer.timestamp,
                       older.timestamp)

    # The following addresses the case of people being suspended quite late
    if older.isSuspended is False and newer.isSuspended is True and older.instCode == newer.instCode:
        SuspensionsRectifier.get_instance().add_person_id(older.cmsId)

    older.yearFraction -= newer.yearFraction

    if older.dueAuthor > 0:
        older.dueAuthor = 1.0 * older.dueAuthor * (older.yearFraction / (older.yearFraction + newer.yearFraction))
    if older.dueApplicant > 0:
        older.dueApplicant = 1.0 * older.dueApplicant * (older.yearFraction / (older.yearFraction + newer.yearFraction))

    return older, newer


def is_time_line_split_needed(older, newer):
    """
    Examines two timelines and determines whether a splitting is needed
    :param older:
    :param newer:
    :return:
    """
    constant_fields = (TimeLineUser.year, TimeLineUser.cmsId)
    for field in constant_fields:
        new_val, old_val = (getattr(x, field.key) for x in [newer, older])
        if new_val != old_val:
            logger.error('No way, splitting time lines for differing %ss: %s and %s makes NO SENSE!', field.key,
                         old_val, new_val)
            raise ValueError('Comparing timelines revealed an unexpected change in %s attribute (%s to %s)!' %
                             (field.key, str(old_val), str(new_val)))

    volatile_fields = (TimeLineUser.mainProj, TimeLineUser.isAuthor, TimeLineUser.isSuspended, TimeLineUser.status,
                       TimeLineUser.category, TimeLineUser.instCode)
    for field in volatile_fields:
        name = field.key
        old_val, new_val = (to_none_if_empty(getattr(x, name)) for x in [older, newer])
        if old_val != new_val:
            logger.debug('Splitting timelines for CMSid %d over the param %s: >%s< %s vs >%s< %s (dumpstamps: %s vs %s)',
                         older.cmsId, name, str(old_val), type(old_val), str(new_val), type(new_val), older.timestamp,
                         newer.timestamp)
            return True
    return False


def to_none_if_empty(some_val):
    if isinstance(some_val, str) and some_val.strip() == '':
        return None
    return some_val


def is_authorship_allowing_activity_id(activity_id, db_session=None):
    if not hasattr(is_authorship_allowing_activity_id, 'data'):
        if db_session:
            setattr(is_authorship_allowing_activity_id, 'data',
                    {x[0] for x in MemberActivity.get_authorship_eligible_activities(db_session)})
        else:
            logger.warn('No DB session provided and no data on author-allowing categories stored')
    return activity_id in getattr(is_authorship_allowing_activity_id, 'data')



def create_time_line(record, *args, **kwargs):
    """
    Some things may look peculiar but they stem from the general assumption that a time line will be generated every day.
    However, in most cases it will not result in modifying the previous time line (most likely the one from Jan 1)
    Exceptions will include: changing institutes, CMS status etc.
    In these cases the dues computed after such a change will be used for the remaining part of the year and the previous
    dues will get this outstanding amount subtracted, resulting in one new time line being created and the most recent one
    being modified.

    :param record:
    :param now:
    :param args: pass here anything that might be useful. if it is, the function will find it by type and content (mess)
    :param kwargs: now - mock timestamp (if need be)
    :return:
    """
    now = kwargs.get('now', None) or datetime.now()
    year = now.year
    year_fraction = year_fraction_left(now)

    if isinstance(record, Person):
        if not hasattr(create_time_line, '_stats') or create_time_line._stats.year != year:
            create_time_line._stats = srvwrk.AppDueStats(session_wrapper(), current_year=year)
        due_stats = create_time_line._stats
        due_author = 0
        due_applicant = 0
        the_x = 0
        if record.status == 'CMS':
            if record.isAuthor:
                # we only compute what would have to be done if the now-being-created timeline, starting today,
                # was to be preserved (that is there is some other change justifying the timeline split)
                due_author = srvwrk.get_annual_author_due(year) * year_fraction
            # applicants' due might still need to be fulfilled by some authors if they completed the application fast
            if record.activity and (record.activity.name in authorship.get_primary_authorship_eligible_capacity_names()
                    or record.activity.name in authorship.get_secondary_authorship_eligible_capacity_names()) \
                    and not record.isAuthorSuspended:
                # from that date until the app start the work will be counted towards the X (in 6-X)
                # parse the pre-history and get that date precisely for returning users
                window_start_date = record.get(Person.dateAuthorUnsuspension) or record.get(Person.dateCreation)
                inferred_start_date = determine_authorship_application_start_date(cms_id=record.get(Person.cmsId), person=record,
                    history=([a for a in args if (isinstance(a, PersonHistory) and a.get(PersonHistory.cmsId) == record.get(Person.cmsId))] or [None])[0])

                app_target = srvwrk.get_annual_applicant_due(inferred_start_date.year)
                if app_target > 0:
                    # apply the 6-X rule here: computing the X
                    for year_no in range(window_start_date.year, srvwrk.AppDueStats.get_first_due_year(due_stats, record.cmsId) or now.year):
                        the_x += srvwrk.get_person_worked(record.cmsId, year_no, session_wrapper())
                        the_x += srvwrk.get_person_shifts_done(record.cmsId, year_no, session_wrapper())
                    app_target = max(app_target - the_x, 0)
                    logger.debug('App target for %d is %.2f' % (record.cmsId, app_target))

                if srvwrk.get_annual_applicant_due(inferred_start_date.year) > 0 \
                        and not srvwrk.AppDueStats.is_epr_burden_expired(due_stats, record.cmsId):
                    total_prior = srvwrk.AppDueStats.get_total_before_this_year(due_stats, record.cmsId)
                    due_applicant = year_fraction * max((app_target - total_prior), 0)

        category = record.activity and category_mapper(record.activity.name) or None
        time_line = TimeLineUser.from_ia_dict({
            TimeLineUser.cmsId: record.cmsId, TimeLineUser.instCode: record.instCode, TimeLineUser.year: year,
            TimeLineUser.mainProj: record.project, TimeLineUser.isAuthor: record.isAuthor,
            TimeLineUser.isSuspended: record.isAuthorSuspended, TimeLineUser.status: record.status,
            TimeLineUser.category: category, TimeLineUser.dueApplicant: due_applicant, TimeLineUser.dueAuthor: due_author,
            TimeLineUser.yearFraction: year_fraction, TimeLineUser.timestamp: now,
            TimeLineUser.comment: the_x > 0 and 'Applicant due reduced in accordance with the 6-X rule: X=%.2f' % the_x or ''
        })
        return time_line
    elif isinstance(record, Institute):
        return TimeLineInst.from_ia_dict({
            TimeLineInst.code: record.code, TimeLineInst.year: year, TimeLineInst.cmsStatus: record.cmsStatus,
            TimeLineInst.timestamp: now
        })
    return None


class EprCategoryMapper(CachedMap):
    """
    Helps to translate the old-DB's activity names into the new DB's categories' IDs
    """
    def reload_data(self):
        self._data = {cat.name: cat.id for cat in Category.session().query(Category).all()}


class EprInstCodeMapper(CachedMap):

    def reload_data(self):
        self._data = {inst.code: inst.id for inst in EprInst.session().query(EprInst).all()}


category_mapper = EprCategoryMapper()
institute_mapper = EprInstCodeMapper()


def year_fraction_left(start_date):
    """
    Returns the year fraction between the start_date and the end of the year
    :param start_date:
    :return:
    """
    if isinstance(start_date, datetime):
        start_date = start_date.date()
    days_a_year = (date(start_date.year, 12, 31) - date(start_date.year-1, 12, 31)).days
    return 1.0 * (date(start_date.year+1, 1, 1) - start_date).days / days_a_year


class CMSDbException(Exception):
    def __init__(self, message):
        super(CMSDbException, self).__init__(message)


class SuspensionsRectifier(object):

    __instance = None

    @classmethod
    def get_instance(cls):
        if cls.__instance == None:
            cls.__instance = SuspensionsRectifier()
        return cls.__instance

    def __init__(self):
        self.cms_ids = set()

    def add_person_id(self, cms_id):
        logger.debug('Registering %d CMS id with Suspensions Rectifier.' % cms_id)
        self.cms_ids.add(cms_id)

    def rectify_suspensions(self, session):
        """
        :param session: database session
        :return: modified TimeLineUser objects, to be committed or rolled back
        """

        modified = []

        if self.cms_ids:
            tlus = session.query(TimeLineUser).filter(TimeLineUser.cmsId.in_(self.cms_ids)). \
                order_by(TimeLineUser.cmsId, desc(TimeLineUser.year), desc(TimeLineUser.timestamp)).all()

            per_person = dict()
            for tlu in tlus:
                if tlu.cmsId not in per_person:
                    per_person[tlu.cmsId] = []
                per_person[tlu.cmsId].append(tlu)

            for cmsId in per_person:
                inside = False

                for tlu in per_person[cmsId]:
                    if not inside and tlu.isSuspended:
                        # skipping the most recent entries - the person is already suspended here
                        logger.debug('No rectification yet, %d still suspended on %s' % (tlu.cmsId, tlu.timestamp))
                        continue
                    elif not tlu.isSuspended:
                        # "entering" the range of entries where the person is not suspended, although should be
                        inside = True
                        comment = '%s: retroactively suspended and waived %.2f applicant due; ' % \
                                  (datetime.now().strftime('%Y.%m.%d'), tlu.dueApplicant)
                        if not tlu.comment:
                            tlu.comment = ''
                        tlu.comment = comment + tlu.comment
                        tlu.isSuspended = True
                        tlu.dueApplicant = 0
                        modified.append(tlu)
                        logger.debug('Rectifying suspension for %d in time line from %d, %s (YF %.2f)'
                                     % (cmsId, tlu.year, tlu.timestamp, tlu.yearFraction))

                    elif inside and tlu.isSuspended:
                        # reaching the other side - some older time when the person was suspended, take the next person
                        logger.debug('No more rectification, %d already suspended on %s' % (tlu.cmsId, tlu.timestamp))
                        break
        return modified





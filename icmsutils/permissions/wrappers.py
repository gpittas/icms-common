class AccessRuleWrapper():
    """
    A thin wrapper meant to remain backwards-compatible with list/dict-based checkers
    while allowing some headroom for the logic that might be added to the rules
    (eg. syntax validation).
    """
    def __init__(self, rule_object):
        self._rule_object = rule_object

    @property
    def rule(self):
        return self._rule_object
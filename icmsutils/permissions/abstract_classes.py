import abc
from icmsutils.permissions.wrappers import AccessRuleWrapper
from icmsutils.permissions.rule_checkers import RuleSetChecker
from icms_orm.toolkit import RestrictedActionTypeValues
import logging


class AbstractRestrictedResource(metaclass=abc.ABCMeta):
    @property
    def id(self):
        return self._get_id()

    @abc.abstractmethod
    def _get_id(self):
        pass


class AbstractAccessRulesProvider(metaclass=abc.ABCMeta):
    @abc.abstractclassmethod
    def get_rules_for_resource_and_action(cls, resource: AbstractRestrictedResource, action: str) -> [AccessRuleWrapper]:
        """
        Rules at the moment can be dictionaries or lists thereof. Crude...
        """
        pass


class AbstractPermissionsManager(metaclass=abc.ABCMeta):
    @abc.abstractclassmethod
    def get_rules_provider_class(cls):
        pass

    @abc.abstractclassmethod
    def get_resources_manager_class(cls):
        pass

    @classmethod
    def can_perform(cls, url: str, request_params: dict, user, action: str):
        assert action in RestrictedActionTypeValues.values(), 'Unknown action {0}'.format(action)

        resources_manager_class = cls.get_resources_manager_class()
        assert issubclass(resources_manager_class, AbstractRestrictedResourcesManager)

        resource = resources_manager_class.get_resource_by_url_and_params(url, request_params)

        if resource is None:
            logging.debug('Path %s does not match any restricted resources - falling back to default rules', url)
        else:
            logging.debug('Path %s matches restricted resource %s', url, resource.id)

        rule_set_checker = RuleSetChecker(user, request_params)
        return cls.can_perform_action_on_resource(action=action, resource=resource, rule_set_checker=rule_set_checker)

    @classmethod
    def can_perform_action_on_resource(cls, action: RestrictedActionTypeValues, resource: AbstractRestrictedResource, rule_set_checker: RuleSetChecker):
        assert action in RestrictedActionTypeValues.values(), 'Unknown action {0}'.format(action)
        rules_provider_class = cls.get_rules_provider_class()

        assert issubclass(rules_provider_class, AbstractAccessRulesProvider), '{0} seems not to subclass {1}'.format(rules_provider_class.__name__, AbstractAccessRulesProvider.__name__)
        rule_wrappers = rules_provider_class.get_rules_for_resource_and_action(resource, action)
        rules = []
        for _wrapper in rule_wrappers:
            assert isinstance(_wrapper, AccessRuleWrapper)
            rules.append(_wrapper.rule)
        result = rule_set_checker.check_rule_set(rules)
        return result


class AbstractRestrictedResourcesManager(metaclass=abc.ABCMeta):
    @abc.abstractclassmethod
    def get_resource_by_url_and_params(cls, url: str, request_params: dict) -> AbstractRestrictedResource:
        pass

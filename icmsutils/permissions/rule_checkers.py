import logging
from collections import Iterable


class RuleSetChecker():
    def __init__(self, user, request_params):
        self._user = user
        self._request_params = request_params

    def check_rule_set(self, rule_set):
        rule_checker = RuleChecker(self._user, self._request_params)
        for _rule in rule_set:
            if rule_checker.check_rule(_rule):
                return True
        return False


class RuleChecker():
    def __init__(self, user, request_params):
        self._user = user
        self._request_params = request_params
        self._predicate_checker = PredicateChecker(user, request_params)

    def check_rule(self, rule):
        if isinstance(rule, list):
            # a list of dicts means a list of alternative rules. Special case [] => False
            for _sub_rule in rule:
                if self.check_rule(_sub_rule):
                    return True
        elif isinstance(rule, dict):
            # any contraDiction => False, empty dict => True (no contradictions)
            if len(rule) == 0:
                logging.debug('Encountered an empty set of constraints: %s - nothing will be filtered out.', rule)
                return True
            for predicate_left, predicate_right in rule.items():
                if not self._predicate_checker.check_predicate(predicate_left, predicate_right):
                    logging.debug('CMS ID %s and request params %s do not fulfill the requirements of rule %s: %s',
                                  self._user.cms_id, self._request_params, predicate_left, predicate_right)
                    return False
            logging.debug('Rule %s met for CMS ID %s', rule, self._user.cms_id)
            return True
        logging.warning('Unexpected rule: %s. Evaluating as False.', rule)
        return False


class PredicateChecker():
    def __init__(self, user, request_params):
        self._user = user
        self._request_params = request_params

    def substitute_placeholder(self, possible_placeholder):
        if not isinstance(possible_placeholder, str):
            # placeholders can only be strings
            return possible_placeholder

        key, store = None, None
        if possible_placeholder[:2] == 'r.':
            store = self._request_params
            key = possible_placeholder[2:]
        elif possible_placeholder[:2] == 'u.':
            store = self._user
            key = possible_placeholder[2:]

        if key is not None or store is not None:
            if key not in store and store == self._user:
                # request can not contain necessary parameters but in case of a user there's most likely some mistake
                raise Exception('Cannot substitute placeholder {0} with any attribute describing a user.'.format(possible_placeholder))
            return store.get(key)
        return possible_placeholder

    def check_predicate(self, parameter, expected_value):
        left_value = self.substitute_placeholder(parameter)
        right_value = self.substitute_placeholder(expected_value)

        for possibility in (right_value if isinstance(right_value, Iterable) else [right_value]):
            # beware of 1 not equaling '1'
            if str(left_value) == str(possibility):
                return True
        return False

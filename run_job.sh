#!/bin/bash

cd "$(dirname "$0")"

source /opt/rh/rh-postgresql95/enable
source vnv/bin/activate

(PYTHONPATH=./ python scripts/$1)
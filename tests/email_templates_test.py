#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import re

import pytest, sqlalchemy as sa
from icms_orm.common import EmailMessage, EmailLog

from email_templates.author_flag_emails import EmailTemplateAuthorshipStatusChange
from icmsutils.icmstest.ddl import recreate_common_db
from email_templates import EmailTemplateAuthorListAddPerson
from email_templates import EmailTemplateAutoSuspension


@pytest.fixture(scope='module')
def email_tests_setup():
    recreate_common_db()


def _store_mail_and_retrieve_with_log(email):
    email.generate_message(store_in_db=True)
    msg, log = EmailMessage.session().query(EmailMessage, EmailLog). \
        join(EmailLog, EmailMessage.id == EmailLog.email_id).order_by(sa.desc(EmailMessage.last_modified)).first()
    return msg, log


class TestAuthorListEmails(object):

    test_data = [
        ('OMG-19-987', 'J.J. Doe', 'jjdoe@cern.ch', 'EAST-INST', 'iCMS Root (CERN)', 'icmsroot@cern.ch'),
        ('GHI-33-123', 'A.B. Cee', 'abcee@cern.ch', 'WEST-INST', 'iCMS Rude (CERN)', 'icmsrude@cern.ch')
    ]

    @pytest.mark.parametrize('al_code, sign_name, author_email, inst_code, sender_signature, sender_email', test_data)
    def test_author_added_message(self, email_tests_setup, al_code, sign_name, author_email, inst_code, sender_signature, sender_email):
        params = dict(al_code=al_code, sign_name=sign_name, author_email=author_email, inst_code=inst_code,
                      sender_signature=sender_signature, sender_email=sender_email)

        email = EmailTemplateAuthorListAddPerson(**params)

        assert email.subject == 'Change in author list for %s' % al_code
        assert 'icms-support@cern.ch' in email.cc
        assert 'cms-ab-support@cern.ch' in email.cc
        assert email.recipients == author_email
        assert email.sender == sender_email
        assert '%s has been added to the author list of %s' % (sign_name, inst_code) in email.body
        assert sender_signature in email.body

    @pytest.mark.parametrize('al_code, sign_name, author_email, inst_code, sender_signature, sender_email', test_data)
    def test_author_added_message_in_db(self, email_tests_setup, al_code, sign_name, author_email, inst_code, sender_signature, sender_email):
        params = dict(al_code=al_code, sign_name=sign_name, author_email=author_email, inst_code=inst_code,
                      sender_signature=sender_signature, sender_email=sender_email)

        msg, log = _store_mail_and_retrieve_with_log(EmailTemplateAuthorListAddPerson(**params))

        assert isinstance(msg, EmailMessage)
        assert msg.subject == 'Change in author list for %s' % al_code
        assert 'icms-support@cern.ch' in msg.cc
        assert 'cms-ab-support@cern.ch' in msg.cc
        assert author_email == msg.to
        assert sender_email == msg.sender
        assert '%s has been added to the author list of %s' % (sign_name, inst_code) in msg.body
        assert sender_signature in msg.body

        assert isinstance(log, EmailLog)
        assert log.status == EmailLog.Status.NEW
        assert log.action == EmailLog.Action.ENQUEUE


class TestAutoSuspensionEmail(object):

    test_data = [
        ('Peter Smith', 'psmith@cern.ch', 66.8, 0.123, None),
        ('Anna Snow', 'asnow@cern.ch', 47.8, 12, None),
        ('Roger Sand', 'rsand@cern.ch', 55.7, 0, 'icmsroot@cern.ch')
    ]

    @pytest.mark.parametrize('recipient_name, recipient_email, app_duration, work_done, sender_email', test_data)
    def test_auto_suspension_email_in_db(self, email_tests_setup, recipient_name, recipient_email, app_duration, work_done, sender_email):
        params = dict(recipient_name=recipient_name, recipient_email=recipient_email, app_duration=app_duration,
                      work_done=work_done, sender_email=sender_email)
        msg, log = _store_mail_and_retrieve_with_log(EmailTemplateAutoSuspension(**params))

        assert isinstance(msg, EmailMessage)
        assert EmailTemplateAutoSuspension._subject == msg.subject
        assert msg.sender == (sender_email or 'icms-support@cern.ch')
        assert msg.to == recipient_email
        assert msg.bcc == EmailTemplateAutoSuspension._bcc
        assert recipient_name in msg.body
        m = re.search(r'(provided .+ months)', msg.body)
        assert 'provided {work_done} months'.format(work_done=isinstance(work_done, float) and '%.2f' % work_done or
                                                              work_done) == m.group(0)
        assert 'started {app_duration} days ago'.format(app_duration=app_duration) in msg.body


class TestAuthorStatusChangeEmail(object):
    test_data = [
        ('Peter Smith', 'smith@cern.ch', 'boss@cern.ch, deputy@cern.ch', True),
        ('John Smith', 'smith@cern.ch', 'boss@cern.ch, deputy@cern.ch', False)
    ]

    @pytest.mark.parametrize('recipient, email_to, emails_cc, will_sign', test_data)
    def test_author_status_notification_in_db(self, recipient, email_to, emails_cc, will_sign):
        params = dict(recipient_name=recipient, recipient_email=email_to, team_leader_emails=emails_cc,
                      bool_has_rights=will_sign)
        msg, log = _store_mail_and_retrieve_with_log(EmailTemplateAuthorshipStatusChange(**params))
        assert isinstance(msg, EmailMessage)
        assert msg.subject == '[iCMS] Author status %s' % ('earned' if will_sign else 'lost')
        assert msg.cc == emails_cc
        assert msg.to == email_to




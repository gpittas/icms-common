#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from scripts.prescript_setup import db
from icmsutils.icmstest.mock.new_icms_mock_factories import MockMoFactory as MoFactory
from icmsutils.icmstest.mock.new_icms_mock_factories import MockNewPersonFactory as PersonFactory
from icmsutils.icmstest.mock.new_icms_mock_factories import MockFundingAgencyFactory as FAFactory
from icmsutils.icmstest.mock.new_icms_mock_factories import MockCountryFactory as CountryFactory
from icmsutils.icmstest.mock.new_icms_mock_factories import MockInstituteFactory as InstFactory
from icmsutils.icmstest.mock.old_icms_mock_factories import MockPersonFactory as PersonFactoryOld
from icmsutils.icmstest.mock.old_icms_mock_factories import MockInstFactory as InstFactoryOld
from icmsutils.icmstest.mock.old_icms_mock_factories import MockPersonMoFactory as MoFactoryOld
from icms_orm.common import MO, Country, FundingAgency, Person, MoStatusValues as MoStatus, Institute
from icms_orm.cmspeople import MoData as OldMO, Person as OldPerson, Institute as OldInst
from scripts.backsync.__main__ import run_agents
from icmsutils.icmstest.assertables import count_in_db, assert_attr_val
from icmsutils.icmstest.functions import cartesian
from icmsutils.icmstest.ddl import recreate_common_db, recreate_people_db
import pytest
import copy


class TestData(object):
    _factories_order = [CountryFactory, FAFactory, InstFactory, PersonFactory, MoFactory]
    _mocks_data = {
        CountryFactory: [
            {Country.name: 'Switzerland', Country.code: 'CH'},
            {Country.name: 'France', Country.code: 'FR'}
        ],
        FAFactory: [
            {FundingAgency.name: 'CH-1', FundingAgency.country_code: 'CH', FundingAgency.id: 1},
            {FundingAgency.name: 'CH-2', FundingAgency.country_code: 'CH', FundingAgency.id: 2},
            {FundingAgency.name: 'France', FundingAgency.country_code: 'FR', FundingAgency.id: 3}
        ],
        PersonFactory: [
            {Person.cms_id: 1, },
            {Person.cms_id: 2, },
            {Person.cms_id: 3, }
        ],
        MoFactory: [
            {MO.year: 2020, MO.cms_id: 1, MO.status: MoStatus.PROPOSED},
            {MO.year: 2019, MO.cms_id: 1, MO.status: MoStatus.APPROVED_FREE_EXTENDED_RIGHTS},
            {MO.year: 2018, MO.cms_id: 1, MO.status: MoStatus.APPROVED_FREE_GENERIC},
            {MO.year: 2017, MO.cms_id: 1, MO.status: MoStatus.APPROVED_FREE_LATECOMER},
            {MO.year: 2016, MO.cms_id: 1, MO.status: MoStatus.APPROVED_FREE_NEW_INST},
            {MO.year: 2015, MO.cms_id: 1, MO.status: MoStatus.APPROVED_FREE_GENERIC},

            {MO.year: 2020, MO.cms_id: 2, MO.status: MoStatus.PROPOSED},
            {MO.year: 2019, MO.cms_id: 2, MO.status: MoStatus.APPROVED_LATE, MO.fa_id: 1, MO.inst_code: 'GVA'},
            {MO.year: 2018, MO.cms_id: 2, MO.status: MoStatus.APPROVED_SWAPPED_IN},
            {MO.year: 2017, MO.cms_id: 2, MO.status: MoStatus.SWAPPED_OUT, MO.fa_id: 2, MO.inst_code: 'ZRH'},
            {MO.year: 2016, MO.cms_id: 2, MO.status: MoStatus.APPROVED},
            {MO.year: 2015, MO.cms_id: 2, MO.status: MoStatus.APPROVED},

            {MO.year: 2020, MO.cms_id: 3, MO.status: MoStatus.PROPOSED},
            {MO.year: 2019, MO.cms_id: 3, MO.status: MoStatus.APPROVED, MO.fa_id: 3, MO.inst_code: 'BDX'},
            {MO.year: 2018, MO.cms_id: 3, MO.status: MoStatus.APPROVED, MO.fa_id: 2, MO.inst_code: 'ZRH'},
            {MO.year: 2017, MO.cms_id: 3, MO.status: MoStatus.APPROVED, MO.fa_id: 1, MO.inst_code: 'GVA'},
            {MO.year: 2016, MO.cms_id: 3, MO.status: MoStatus.PROPOSED},
            {MO.year: 2015, MO.cms_id: 3, MO.status: MoStatus.PROPOSED},
        ],
        InstFactory: [
            {Institute.code: 'ZRH', Institute.name: 'Zurich', Institute.country_code: 'CH'},
            {Institute.code: 'GVA', Institute.name: 'Geneva', Institute.country_code: 'CH'},
            {Institute.code: 'BDX', Institute.name: 'Bordeaux', Institute.country_code: 'FR'}
        ]
    }

    @staticmethod
    def alter_data():
        db.session.query(MO).filter(MO.cms_id == 1).filter(MO.year == 2020).update({MO.status.key: MoStatus.REJECTED})
        db.session.query(MO).filter(MO.cms_id == 2).filter(MO.year == 2016).update({MO.status.key: MoStatus.REJECTED})
        db.session.query(MO).filter(MO.cms_id == 3).filter(MO.year == 2020).update({
            MO.status.key: MoStatus.APPROVED,
            MO.fa_id: 2,
            MO.inst_code: 'GVA'
        })
        db.session.commit()
        TestData._mocks_data[MoFactory] = [r.to_ia_dict() for r in db.session.query(MO).all()]


    @staticmethod
    def fa_id_to_name(fa_id):
        return {1: 'CH-1', 2: 'CH-2', 3: 'France'}.get(fa_id)

    @staticmethod
    def cms_ids():
        return [p.get(Person.cms_id) for p in TestData._mocks_data.get(PersonFactory)]

    @staticmethod
    def inst_codes():
        return [i.get(Institute.code) for i in TestData._mocks_data.get(InstFactory)]

    @staticmethod
    def years():
        return sorted({_m.get(MO.year) for _m in TestData._mocks_data.get(MoFactory)})

    @staticmethod
    def mo_status(cms_id, year):
        for x in TestData._mocks_data.get(MoFactory):
            if x.get(MO.year) == year and x.get(MO.cms_id) == cms_id:
                return x.get(MO.status)
        return None

    @staticmethod
    def phd_mo_fa(cms_id, year):
        fa_id = \
        [_x for _x in TestData._mocks_data.get(MoFactory) if _x.get(MO.year) == year and _x.get(MO.cms_id) == cms_id][
            0].get(MO.fa_id)
        return TestData.fa_id_to_name(fa_id)

    @staticmethod
    def phd_mo_ic(cms_id, year):
        return \
            [_x for _x in TestData._mocks_data.get(MoFactory) if
             _x.get(MO.year) == year and _x.get(MO.cms_id) == cms_id][
                0].get(MO.inst_code)

    @classmethod
    def register_mocks(cls):
        for factory_class in TestData._factories_order:
            override_dicts_array = TestData._mocks_data.get(factory_class)
            _overrides_dict: dict
            for _overrides_dict in override_dicts_array:
                instance = factory_class.create_instance(instance_overrides=_overrides_dict)
                db.session.add(instance)
                db.session.flush()
            db.session.commit()
        # create placeholders in the old DB, ALERT: magic number!
        for _cms_id in TestData.cms_ids():
            person = PersonFactoryOld.create_instance(instance_overrides={OldPerson.cmsId: _cms_id})
            mo = MoFactoryOld.create_instance(person=person)
            db.session.add(person)
            db.session.flush()
            db.session.add(mo)
        for _inst_code in TestData.inst_codes():
            db.session.add(InstFactoryOld.create_instance({OldInst.code: _inst_code}))
        db.session.commit()


@pytest.fixture(scope='class')
def setup():
    recreate_people_db()
    recreate_common_db()
    TestData.register_mocks()


@pytest.fixture(scope='class')
def run_backsync(setup):
    run_agents(dry_run_override=False)


@pytest.fixture(scope='class')
def alter_and_re_sync(run_backsync):
    TestData.alter_data()
    run_agents(dry_run_override=False)


class TestStateBeforeBacksync(object):
    def test_recipient_mo_data_in_place_before_backsync(self, setup):
        assert len(TestData.cms_ids()) == count_in_db(OldMO, {})

    @pytest.mark.parametrize('_class', [Person, MO, Country, FundingAgency])
    def test_there_is_source_data_before_backsync(self, _class, setup):
        assert 0 < count_in_db(_class, {}), 'Expected to find some entries for {0} class in the source database'.format(
            _class.__name__)


class TestStateAfterInitialBacksync(object):

    def test_some_mo_data_after_backsync(self, run_backsync):
        assert 0 < count_in_db(OldMO, {})

    @pytest.mark.parametrize('year, cms_id', cartesian(TestData.years(), TestData.cms_ids()))
    def test_individuals_mo_status(self, run_backsync, cms_id, year):
        source_status = TestData.mo_status(cms_id, year)

        (mo_col, phd_mo_col, free_mo_col, phd_ic_col, phd_fa_col) = OldMO.get_columns_for_year(year)

        expected = {
            mo_col: 'YES' if source_status in {MoStatus.PROPOSED} or source_status.startswith(MoStatus.APPROVED) else 'NO',
            phd_mo_col: 'YES' if source_status in {MoStatus.SWAPPED_OUT, MoStatus.APPROVED} else 'NO',
            free_mo_col: 'YES' if source_status in {MoStatus.APPROVED_SWAPPED_IN, MoStatus.APPROVED_LATE} or source_status.startswith('APPROVED_FREE') else 'NO',
            phd_ic_col: TestData.phd_mo_ic(cms_id, year),
            phd_fa_col: TestData.phd_mo_fa(cms_id, year)
        }

        assert_attr_val(OldMO, {OldMO.cmsId: cms_id}, expected)


class TestStateAfterAnotherBacksync(object):
    @pytest.mark.parametrize('year, cms_id', cartesian(TestData.years(), TestData.cms_ids()))
    def test_individuals_mo_status(self, alter_and_re_sync, cms_id, year):
        source_status = TestData.mo_status(cms_id, year)

        (mo_col, phd_mo_col, free_mo_col, phd_ic_col, phd_fa_col) = OldMO.get_columns_for_year(year)

        expected = {
            mo_col: 'YES' if source_status in {MoStatus.PROPOSED} or source_status.startswith(
                MoStatus.APPROVED) else 'NO',
            phd_mo_col: 'YES' if source_status in {MoStatus.SWAPPED_OUT, MoStatus.APPROVED} else 'NO',
            free_mo_col: 'YES' if source_status in {MoStatus.APPROVED_SWAPPED_IN,
                                                    MoStatus.APPROVED_LATE} or source_status.startswith(
                'APPROVED_FREE') else 'NO',
            phd_ic_col: TestData.phd_mo_ic(cms_id, year),
            phd_fa_col: TestData.phd_mo_fa(cms_id, year)
        }

        assert_attr_val(OldMO, {OldMO.cmsId: cms_id}, expected)

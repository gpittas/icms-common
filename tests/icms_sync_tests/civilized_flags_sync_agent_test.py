"""
More tests for sync_agents, but this time with proper fixtures and so on
"""
import logging

from scripts.prescript_setup import db
from datetime import datetime, date
import pytest
from icmsutils.businesslogic.flags import Flag
from icms_orm.cmspeople import Person, PeopleFlagsAssociation
from icms_orm.common import LegacyFlag
from scripts.sync import ForwardSyncManager, PeopleSyncAgent, LegacyFlagsSyncAgent
from scripts.sync.brokers import ChangeLogAwareBroker, DefaultBroker
from icmsutils.icmstest.mock import OldIcmsMockPersonFactory as PersonMF
from icmsutils.icmstest.ddl import recreate_common_db, recreate_people_db
from icmsutils.icmstest.mock import set_mock_date
from icmsutils.icmstest.mock import MockUserFactory as UserMF, MockFlagFactory as FlagMF, MockChangeLogFactory as ChangeLogMF
from icms_orm.metadata import ChangeLog
from icmsutils.icmstest.assertables import assert_attr_val, count_in_db
import time


class SyncManagerProxy(object):
    """
    Adding it here so that all the tests can easily re-run using a different Broker class
    The class with tests got a subclass, so that every fixture runs again and the setup fixture will set the broker
    """
    _broker_index = 0
    _broker_classes = [DefaultBroker, ChangeLogAwareBroker]

    @classmethod
    def toggle_broker_class(cls):
        cls._broker_index = (cls._broker_index + 1) % len(cls._broker_classes)
        print('Broker index is now %d' % cls._broker_index)

    @classmethod
    def sync(cls):
        return ForwardSyncManager.launch_sync(
            agent_classes_override=[PeopleSyncAgent, LegacyFlagsSyncAgent],
            broker_class_override=cls.get_broker_class())

    @classmethod
    def get_broker_class(cls):
        return cls._broker_classes[cls._broker_index]


def clean_slate():
    print('Clean slate running!')
    SyncManagerProxy.toggle_broker_class()
    PersonMF.reset_overrides()
    recreate_common_db()
    recreate_people_db()


@pytest.fixture(scope='class', name='clean_slate')
def clean_slate_fixture():
    return clean_slate()


@pytest.fixture(scope='class')
def create_mocks(clean_slate):
    _ppl = []

    for _cms_id in range(1, 5):
        _mp = PersonMF.create_instance({Person.cmsId: _cms_id})
        _mu = UserMF.create_instance(person=_mp)
        _ppl.append(_mp)
        db.session.add(_mp)
        db.session.add(_mu)

    for _f in FlagMF.create_all():
        db.session.add(_f)
    db.session.commit()

    for _mp in _ppl:
        db.session.add(ChangeLogMF.create_instance(master_object=_mp))
    db.session.commit()


def fixture_body(day_number):
    _date = FlagTestData.get_dates()[day_number-1]
    set_mock_date(_date)
    _ssn = db.session()
    _flags_added = []
    _flags_removed = []
    for _cms_id in range(1, FlagTestData.MAX_CMS_ID + 1):
        for _flag in FlagTestData.get_flags_to_add(_cms_id, _date):
            logging.info('%d will get %s' % (_cms_id, _flag))
            _mf = FlagMF.create_instance(
                instance_overrides={PeopleFlagsAssociation.cmsId: _cms_id, PeopleFlagsAssociation.flagId: _flag})
            _flags_added.append(_mf)
            _ssn.add(_mf)
        for _flag in FlagTestData.get_flags_to_divest(_cms_id, _date):
            _mf = _ssn.query(PeopleFlagsAssociation).filter(PeopleFlagsAssociation.flagId == _flag).filter(
                PeopleFlagsAssociation.cmsId == _cms_id).one()
            _flags_removed.append(_mf)
            _ssn.delete(_mf)
            logging.info('%d will lose %s' % (_cms_id, _flag))

    _ssn.commit()
    if SyncManagerProxy.get_broker_class() == ChangeLogAwareBroker:
        for _f in _flags_added:
            _ssn.add(ChangeLogMF.create_instance(instance_overrides={ChangeLog.operation: 'insert'}, master_object=_f))
        for _f in _flags_removed:
            _ssn.add(ChangeLogMF.create_instance(instance_overrides={ChangeLog.operation: 'delete'}, master_object=_f))
        _ssn.commit()
        # changes logged in the database need to be at least 1 second apart
        # time.sleep(2)
    SyncManagerProxy.sync()
    return _date


@pytest.fixture(scope='class')
def day_one(create_mocks):
    return fixture_body(1)


@pytest.fixture(scope='class')
def day_two(day_one):
    return fixture_body(2)


@pytest.fixture(scope='class')
def day_three(day_two):
    return fixture_body(3)


@pytest.fixture(scope='class')
def day_four(day_three):
    return fixture_body(4)


class FlagTestData(object):

    MAX_CMS_ID = 4
    FA = Flag.MISC_AUTHORYES
    FB = Flag.MISC_AUTHORNO
    FC = Flag.MISC_AUTHORNOMO
    FX = Flag.FINANCE_BOARD
    FY = Flag.EXECUTIVE_BOARD

    _date_strings = ('20171213', '20171225', '20180105', '20180210')
    _dates = [datetime.strptime(_ds, '%Y%m%d').date() for _ds in _date_strings]

    _to_assign = {
        _dates[0]: {
            1: {FX, FY},
            2: {FA, FB, FC},
            3: {},
            4: {FC}
        },
        _dates[1]: {},
        # 1 will have X Y B; 3 will have B
        _dates[2]: {
            1: {FB},
            3: {FB}
        },
        # 3 will have A B C
        _dates[3]: {
            3: {FA, FC}
        },
    }

    _to_divest = {
        _dates[0]: {},
        # 4 will have no more flags
        _dates[1]: {
            4: {FC}
        },
        # 2 will have A C
        _dates[2]: {
            2: {FB}
        },
        # 2 will have no more flags
        _dates[3]: {
            2: {FA, FC}
        }
    }

    @staticmethod
    def to_date(something):
        if isinstance(something, date):
            return something
        elif isinstance(something, datetime):
            return something.date()
        else:
            return datetime.strptime(something, '%Y%m%d').date()

    @classmethod
    def get_dates(cls):
        return cls._dates

    @classmethod
    def get_flags_as_of(cls, cms_id, date_or_str):
        _date = cls.to_date(date_or_str)
        _flags = set()
        for _d in cls._dates:
            if _d > _date: break
            for _flag in cls._to_assign[_d].get(cms_id, set()):
                _flags.add(_flag)
            for _flag in cls._to_divest[_d].get(cms_id, set()):
                _flags.remove(_flag)
        return _flags

    @classmethod
    def get_flags_to_add(cls, cms_id, date):
        date = cls.to_date(date)
        return cls._to_assign.get(date, {}).get(cms_id, set())

    @classmethod
    def get_flags_to_divest(cls, cms_id, date):
        date = cls.to_date(date)
        return cls._to_divest.get(date, {}).get(cms_id, set())


class BaseTestLegacyFlagsSync(object):
    @staticmethod
    def __perform_flag_checks(cms_id, date_):
        _ref = FlagTestData.get_flags_as_of(cms_id, date_)
        assert len(_ref) == count_in_db(LegacyFlag, {LegacyFlag.cms_id: cms_id, LegacyFlag.end_date: None})
        for _flag in _ref:
            assert_attr_val(LegacyFlag, {LegacyFlag.cms_id: cms_id, LegacyFlag.flag_code: _flag},
                            {LegacyFlag.end_date: None})

    def test_that_correct_broker_is_enabled(self, clean_slate):
        raise Exception('Not implemented!')

    @pytest.mark.parametrize('cms_id', range(1, FlagTestData.MAX_CMS_ID + 1))
    def test_day_one_flags_sync(self, day_one, cms_id):
        self.__perform_flag_checks(cms_id, day_one)

    @pytest.mark.parametrize('cms_id', range(1, FlagTestData.MAX_CMS_ID + 1))
    def test_day_two_flags_sync(self, day_two, cms_id):
        self.__perform_flag_checks(cms_id, day_two)

    @pytest.mark.parametrize('cms_id', range(1, FlagTestData.MAX_CMS_ID + 1))
    def test_day_three_flags_sync(self, day_three, cms_id):
        self.__perform_flag_checks(cms_id, day_three)

    @pytest.mark.parametrize('cms_id', range(1, FlagTestData.MAX_CMS_ID + 1))
    def test_day_four_flags_sync(self, day_four, cms_id):
        self.__perform_flag_checks(cms_id, day_four)


class TestLegacyFlagsSyncAgentWithSmartBroker(BaseTestLegacyFlagsSync):
    def test_that_correct_broker_is_enabled(self, clean_slate):
        assert SyncManagerProxy.get_broker_class() == ChangeLogAwareBroker


class TestLegacyFlagsSyncAgentWithDefaultBroker(BaseTestLegacyFlagsSync):
    def test_that_correct_broker_is_enabled(self, clean_slate):
        assert SyncManagerProxy.get_broker_class() == DefaultBroker

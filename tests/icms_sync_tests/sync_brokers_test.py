import pytest
from scripts.prescript_setup import db
from scripts.sync.brokers import DefaultBroker, ChangeLogAwareBroker as WiseBroker
from icmsutils.icmstest.mock import MockPersonMoFactory, MockInstFactory, MockUserFactory, MockPersonFactory
from icmsutils.icmstest.mock import MockActivityFactory, MockChangeLogFactory
from icmsutils.icmstest.ddl import recreate_people_db, recreate_common_db
from icms_orm.cmspeople import Person, Institute, MoData, User, MemberActivity
from icms_orm.metadata import ChangeLog, OperationValues
from datetime import datetime, timedelta
import sqlalchemy as sa
import random
import numbers

# FURTHER TESTS TO DO: really composite keys, deleting entries, cold vs hot start

TOTAL_PEOPLE = 123
TOTAL_INSTS = 12
INVOCATION_PERIOD = 50
DAY_ZERO = datetime(2017, 1, 1, 0, 0)


def populate_db():
    recreate_common_db()
    recreate_people_db()
    MockPersonFactory.reset_overrides(Person)

    for _ in range(0, TOTAL_PEOPLE):
        person = MockPersonFactory.create_instance()
        user = MockUserFactory.create_instance(person=person)
        mo = MockPersonMoFactory.create_instance(person=person)
        db.session.add(person)
        db.session.add(mo)
        db.session.add(user)
        db.session.flush()
        for master in (person, mo, user):
            log = MockChangeLogFactory.create_instance(
                instance_overrides={ChangeLog.last_update: DAY_ZERO, ChangeLog.operation: OperationValues.INSERT}, master_object=master
            )
            db.session.add(log)

    db.session.flush()

    for _ in range(0, TOTAL_INSTS):
        inst = MockInstFactory.create_instance()
        db.session.add(inst)
        db.session.flush()
        log = MockChangeLogFactory.create_instance(instance_overrides={
            ChangeLog.last_update: DAY_ZERO, ChangeLog.operation: OperationValues.INSERT
        }, master_object=inst)
        db.session.add(log)
    db.session.commit()

@pytest.fixture(name='populate_db')
def populate_db_fixture():
    return populate_db()


@pytest.fixture()
def populate_once():
    if hasattr(populate_once, '__expended'):
        return
    setattr(populate_once, '__expended', True)
    populate_db()


def emulate_triggers( t_0 ) :

    changes = {}

    for entity in (Person, Institute, MoData, User):
        session = entity.session()
        keys = entity.primary_keys()
        rows = session.query(*keys).all()
        random.shuffle(rows)
        limit = random.randint(1, int(len(rows)/random.randint(3, 8)))
        changed = rows[0:limit]
        changes[entity] = set(changed)
        for keys in changed:
            moment = t_0 + timedelta(seconds=random.randint(0, 60 * INVOCATION_PERIOD - 1))

            overrides = {
                ChangeLog.last_update: moment,
                ChangeLog.operation: OperationValues.UPDATE,
                ChangeLog.ref_schema: entity.__table__.schema,
                ChangeLog.ref_table: entity.__table__.name,
            }
            if len(keys) == 1 and isinstance(keys[0], numbers.Number):
                overrides[ChangeLog.ref_id] = keys[0]
            else:
                overrides[ChangeLog.ref_hash_id] = sa.func.md5(sa.func.concat_ws('|', *keys))
            log = MockChangeLogFactory.create_instance(instance_overrides=overrides)
            session.add(log)
            session.commit()
    return changes

@pytest.fixture(name='emulate_triggers')
def emulate_triggers_fixture(t_0):
    return emulate_triggers(t_0)

@pytest.mark.parametrize('entity, expected_count', [(Person, TOTAL_PEOPLE), (Institute, TOTAL_INSTS), (MoData, TOTAL_PEOPLE), (User, TOTAL_PEOPLE)])
def test_default_broker_returns_all_rows(populate_once, entity, expected_count):
    broker = DefaultBroker()
    assert len(broker.relay(db.session.query(entity))) == expected_count


@pytest.mark.parametrize('timeframe', [DAY_ZERO + timedelta(minutes=n) for n in range(1, 700, INVOCATION_PERIOD)])
def test_wise_broker_returns_only_the_rows_that_have_changed_since_date_given(populate_once, timeframe):
    ref_changed_pks = emulate_triggers(timeframe)
    broker = WiseBroker(since_when=timeframe)

    total_changed_length = 0

    for pack in [(Person, Person.cmsId), (Institute, Institute.code), (User, User.cmsId), (MoData, MoData.cmsId)]:
        entity = pack[0]
        keys = pack[1:]
        changed_objects = broker.relay(db.session.query(entity))
        assert len(changed_objects) == len(ref_changed_pks[entity])
        changed_pks = set([tuple([getattr(o, key.key) for key in keys]) for o in changed_objects])
        assert len(changed_pks) == len(changed_pks)
        assert len(changed_pks ^ ref_changed_pks[entity]) == 0
        total_changed_length += len(changed_objects)

    assert total_changed_length > 0, 'Something should have changed really...'

    next_broker = WiseBroker(since_when=timeframe + timedelta(minutes=INVOCATION_PERIOD))
    for entity in [Person, Institute, MoData, User]:
        assert 0 == len(next_broker.relay(db.session.query(entity))), 'Broker knows the future although it should not!'


@pytest.mark.parametrize('entity, expected_count', [(Person, TOTAL_PEOPLE), (Institute, TOTAL_INSTS),
                                                    (MoData, TOTAL_PEOPLE), (User, TOTAL_PEOPLE)])
def test_change_log_entries_for_creation_are_saved(populate_once, entity, expected_count):
    assert expected_count == db.session.query(ChangeLog).filter(ChangeLog.ref_schema == entity.__table__.schema).\
        filter(ChangeLog.ref_table == entity.__table__.name).filter(ChangeLog.operation == OperationValues.INSERT).count()


def test_deletes_are_ignored_when_picking_last_updated_rows():
    broker = WiseBroker(since_when=datetime(2018, 1, 1, 0, 0))

    inst = random.choice(db.session.query(Institute).all())

    db.session.delete(inst)
    db.session.add(MockChangeLogFactory.create_instance(master_object=inst,
        instance_overrides={ChangeLog.operation: OperationValues.DELETE, ChangeLog.last_update: datetime(2018, 1, 2, 0, 0)}))
    db.session.commit()

    assert 1 == db.session.query(ChangeLog).filter(ChangeLog.operation == OperationValues.DELETE).count()

    assert 0 == len(broker.relay(db.session.query(Institute)))


@pytest.mark.parametrize('class_name, _cls', [('DefaultBroker', DefaultBroker), ('ChangeLogAwareBroker', WiseBroker)])
def test_retrieving_subclass_by_string(class_name, _cls):
    assert DefaultBroker.get_subclass_by_name(class_name) == _cls

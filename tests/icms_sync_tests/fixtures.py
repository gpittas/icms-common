from datetime import date
from scripts import sync, mockables
from icmsutils.icmstest.mock import set_mock_date

BRN, GVA, ZRH = ('BRN', 'GVA', 'ZRH')
inst_codes = (GVA, ZRH, BRN)

FIRST_DAY = date(2018, 2, 1)
SECOND_DAY = date(2018, 3, 17)
THIRD_DAY = date(2018, 6, 22)
FOURTH_DAY = date(2019, 3, 14)
FIFTH_DAY = date(2019, 11, 9)
SIXTH_DAY = date(2020, 5, 27)

FA_CH_1 = 'Switzerland-1'
FA_CH_2 = 'Switzerland-2'
FA_CH_3 = 'Switzerland-3'
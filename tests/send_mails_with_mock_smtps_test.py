#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import pytest
from scripts import send_mails
from icms_orm.common import EmailMessage, EmailLog
from icmsutils.icmstest.ddl import recreate_common_db
from scripts.prescript_setup import db, config
from scripts import send_mails
from icmsutils.mailutils import SMTP
from icmsutils.regexutils import extract_all_email_addresses


@pytest.fixture(name='recreate_common_db')
def recreate_common_db_fixture():
    return recreate_common_db()

class GrumpySmtp(SMTP):
    def send_email(self, **kwargs):
        raise RuntimeError('I just do not like sending emails')


class HappySmtp(SMTP):
    def send_email(self, **kwargs):
        pass


mail_params = dict(
        sender=config['mailing']['sender'],
        to="fake_r'ecipient@nu.ll",
        subject='A very test message!',
        body='Hello there, testing is under way. Cheers, iCMS',
        bcc='nobody@nowhere.null someone@somewhere.null',
        cc='theother.one.null',
        source_app='icms-common TEST',
        reply_to='no-reply@never.ever',
        remarks='This message is a product of automated testing procedure',
    )


@pytest.mark.parametrize('input, output', [
    ('oneand@gmail.com,twoand@cern.ch',
     ['oneand@gmail.com', 'twoand@cern.ch']),
    ('threeo\'clock@yahoo.com', ["threeo'clock@yahoo.com"]),
    ('me@he.re you@the.re', ['me@he.re', 'you@the.re'])
])
def test_emails_extraction_regex(input, output):
    assert extract_all_email_addresses(input) == output


def test_no_emails_in_db(recreate_common_db):
    assert db.session.query(EmailMessage).count() == 0
    assert db.session.query(EmailLog).count() == 0

    EmailMessage.compose_message(db_session=db.session, **mail_params)

    # id = Column(Integer, primary_key=True, autoincrement=True)
    # sender = Column(String(256))
    # reply_to = Column(String(256))
    # to = Column(Text, nullable=False)
    # cc = Column(Text)
    # bcc = Column(Text)
    # subject = Column(Text, nullable=False)
    # body = Column(Text, nullable=False)
    # # some meta-info
    # hash = Column(String(32), nullable=False)
    # source_application = Column(String(32))
    # remarks = Column(Text)
    # last_modified = Column(DateTime, nullable=

    message = EmailMessage.query.first()

    assert message.sender == mail_params['sender']
    assert message.reply_to == mail_params['reply_to']
    assert message.subject == mail_params['subject']
    assert message.to == mail_params['to']
    assert message.cc == mail_params['cc']
    assert message.bcc == mail_params['bcc']
    assert message.body == mail_params['body']
    assert message.source_application == mail_params['source_app']
    assert message.remarks == mail_params['remarks']


def test_check_sending_failing_and_succeeding_on_resend(recreate_common_db):
    EmailMessage.compose_message(db_session=db.session, **mail_params)

    assert db.session.query(EmailMessage).count() == 1
    assert db.session.query(EmailLog).count() == 1

    send_mails.main(GrumpySmtp)

    assert db.session.query(EmailMessage).count() == 1
    assert db.session.query(EmailLog).count() == 2

    mail_params['body'] = 'Some other message this time!'
    EmailMessage.compose_message(db_session=db.session, **mail_params)

    assert db.session.query(EmailMessage).count() == 2
    assert db.session.query(EmailLog).count() == 3

    assert db.session.query(EmailLog).filter(EmailLog.status == EmailLog.Status.NEW).count() == 2
    assert db.session.query(EmailLog).filter(EmailLog.status == EmailLog.Status.FAILED).count() == 1

    send_mails.main(HappySmtp)

    assert db.session.query(EmailMessage).count() == 2
    assert db.session.query(EmailLog).count() == 5
    assert db.session.query(EmailLog).filter(EmailLog.status == EmailLog.Status.SENT).count() == 2
    assert db.session.query(EmailLog).filter(EmailLog.status == EmailLog.Status.NEW).count() == 2
    assert db.session.query(EmailLog).filter(EmailLog.status == EmailLog.Status.FAILED).count() == 1


def test_mass_queueing(recreate_common_db):
    assert db.session.query(EmailMessage).count() == 0
    assert db.session.query(EmailLog).count() == 0

    for i in range(0, 120):
        mail_params['body'] = 'Test message number %d' % i
        mail_params['subject'] = '%d test message' % i
        EmailMessage.compose_message(db_session=db.session, **mail_params)

    assert db.session.query(EmailMessage).count() == 120
    assert db.session.query(EmailLog).count() == 120

    bunch_size = 22
    config['mailing']['bunch_size'] = str(bunch_size)

    send_mails.main(HappySmtp)

    assert db.session.query(EmailMessage).count() == 120
    assert db.session.query(EmailLog).count() == 120 + bunch_size

    def pull_from_db(status):
        return db.session.query(EmailMessage, EmailLog).join(EmailLog, EmailLog.email_id == EmailMessage.id).filter(EmailLog.status == status).all()

    def get_subjects(r):
        return {mail.subject for mail, log in r}

    sent = pull_from_db(EmailLog.Status.SENT)
    sent_subjects = get_subjects(sent)

    assert len(sent_subjects) == bunch_size
    for subject in {'%d test message' % i for i in range(0, bunch_size)}:
        assert subject in sent_subjects

    # now let's fail
    send_mails.main(GrumpySmtp)
    failed = pull_from_db(EmailLog.Status.FAILED)
    failed_subjects = get_subjects(failed)
    assert len(failed_subjects) == bunch_size
    for subject in {'%d test message' % i for i in range(bunch_size, 2*bunch_size)}:
        assert subject in failed_subjects
        assert subject not in sent_subjects

    # then let's succeed and make sure that those that had previously failed now did succeed
    send_mails.main(HappySmtp)
    sent = pull_from_db(EmailLog.Status.SENT)
    sent_subjects = get_subjects(sent)
    assert len(sent_subjects) == 2*bunch_size
    for subject in {'%d test message' % i for i in range(0, 2*bunch_size)}:
        assert subject in sent_subjects



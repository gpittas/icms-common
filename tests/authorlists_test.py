#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import pytest, random

from icmsutils.exceptions import AuthorListException
from scripts.prescript_setup import db
from icmsutils.businesslogic.cadi.authorlists import AuthorListModel
from icms_orm.cmspeople import Person, Institute, MemberActivity, User, PersonHistory
from icms_orm.cmsanalysis import CadiAnalysis, PaperAuthor, PaperAuthorHistory as PAH
from icms_orm.cmsanalysis import PaperAuthorHistoryActions as PAHActions, PaperAuthorStatusValues as PAStatus
from icmsutils.icmstest.mock import MockPersonFactory, MockInstFactory, MockUserFactory, MockActivityFactory
from icmsutils.icmstest.ddl  import recreate_people_db, recreate_old_cadi_db
from icmsutils.businesslogic.cadi import authorlists as al_logic
from icmsutils.icmstest.assertables import assert_attr_val, count_in_db
from icmsutils.funcutils import Cache
from contextlib import contextmanager


class MockData(object):

    _instance = None

    def __init__(self, inst_codes=None, total_people=200, total_authors=77, oddity_factor=0.1):
        """
        :param inst_codes: array of institute codes to use
        :param total_people: number of people to generate randomly
        :param total_authors: number of authors (to start with)
        :param oddity_factor: number [0, 1] used when picking some random subsets
        """
        MockData._instance = self
        self._inst_codes = inst_codes or ['PARIS', 'BERLIN', 'NYC', 'BEIRUT', 'MOSCOW', 'CADIZ']
        self._total_people = total_people
        self._total_authors = min(total_authors, total_people)
        self._activities = MockActivityFactory.create_all()
        self._oddity = max(min(oddity_factor, 0.99), 0.01)
        self._people_map = dict()
        self._users_map = dict()
        self._analysis_codes = ['TST-12-001', 'POT-13-123', 'XYZ-19-998', 'ABC-16-617', 'EVO-17-001', 'EVO-17-002', 'EVO-17-003', 'ABC-14-402', 'ECO-17-042']

    def _draw_n_elems(self, n=None, pool=None):
        pool = pool or list(range(0, self._total_people))
        n = n is not None and n or int(len(pool) * self._oddity)
        random.shuffle(pool)
        return pool[0:int(n)]

    @staticmethod
    def _random_string(length=11, charset='abcdefghijklmnoprstuvwxyz'):
        return ''.join([random.choice(charset) for _ in range(0, length)])

    @classmethod
    def get_instance(cls):
        return cls._instance or cls() and cls._instance

    def _mock_activities(self):
        for activity in self._activities:
            yield activity

    def _mock_institutes(self):
        for code in self._inst_codes:
            yield MockInstFactory.create_instance(instance_overrides={Institute.code: code})

    def _mock_people(self):
        # authorship and other lotteries ...
        _ixs_signing = self._draw_n_elems(self._total_authors)
        _ixs_infn = self._draw_n_elems()
        _ixs_sign_name = self._draw_n_elems()
        _ixs_also_at = self._draw_n_elems()
        # lotteries done

        for idx in range(0, self._total_people):
            mock_person, mock_user = self._mock_person_and_user_generator(
                is_author=(idx in _ixs_signing),
                infn=('' if idx in _ixs_infn else None),
                univ_other=('' if idx in _ixs_infn else None),
                name_sign=('' if idx in _ixs_sign_name else None)
            )

            self._people_map[mock_person.get(Person.cmsId)] = mock_person
            self._users_map[mock_user.get(User.cmsId)] = mock_user
            yield mock_person
            yield mock_user

    def get_cms_ids(self):
        return self._people_map.keys()

    def get_author_cms_ids(self):
        return [k for k, v in self._people_map.items() if v.get(Person.isAuthor)]

    def get_non_author_cms_ids(self):
        return [k for k, v in self._people_map.items() if not v.get(Person.isAuthor)]

    def get_infn(self, cms_id):
        return self._people_map.get(cms_id).get(Person.infn)

    def get_sign_name(self, cms_id):
        _v = self._users_map.get(cms_id).get(User.nameSignature)
        if not _v:
            _p = self._people_map.get(cms_id)
            # this is done in a silly manner and it's supposed to so that the test code and tested code differ
            _first = '.'.join([_s[0] for _s in _p.get(Person.firstName).split(' ')])
            _v = '{0}. {1}'.format(_first, _p.get(Person.lastName))
        return _v

    def get_univ_other(self, cms_id):
        return self._people_map.get(cms_id).get(Person.univOther)

    def get_inspire_id(self, cms_id):
        return self._people_map.get(cms_id).get(Person.authorId)

    @property
    def mocks(self):
        for generator in (self._mock_institutes, self._mock_activities, self._mock_people):
            for result in generator():
                yield result

    @property
    def test_data_analysis_codes(self):
        # just a list of tuples with analysis codes [(code,), (code2,) ... ]
        return [(code,) for code in self._analysis_codes]

    @property
    @Cache.permanent
    def test_data_some_lists_open_some_not(self):
        _open = self._draw_n_elems(n=len(self._analysis_codes) / 2, pool=self._analysis_codes)
        _data = [(code, code in _open and 'open' or None) for code in self._analysis_codes]
        return _data

    @property
    @Cache.permanent
    def test_data_some_lists_open_some_closed_some_neither(self):
        """
        :return: tuple (code, status ['open'/'closed'/None])
        """
        _open_or_closed = self._draw_n_elems(n=len(self._analysis_codes) / 2, pool=self._analysis_codes)
        _open = self._draw_n_elems(n=len(_open_or_closed) / 2, pool=_open_or_closed)
        _data = [(code, code in _open and 'open' or code in _open_or_closed and 'closed' or None) for code in self._analysis_codes]
        return _data

    @property
    @Cache.permanent
    def extra_non_author_mock_data(self):
        """
        :return: list of tuples [(person_mock, user_mock), (another_person_mock, another_user_mock) etc...]
        """
        _mock_pairs = [
            self._mock_person_and_user_generator(is_author=False, name_sign='J.P. Panda',),
            self._mock_person_and_user_generator(is_author=False, name_sign='', infn='', univ_other='')
        ]
        return _mock_pairs

    def _mock_person_and_user_generator(self, inst_code=None, is_author=None, activity_id=None, infn=None, univ_other=None, name_sign=None):
        """
        a simple rule: for each None passed, a random value is assigned. otherwise the input is passed to the mock
        """
        _mock_person = MockPersonFactory.create_instance(instance_overrides={
            Person.instCode: inst_code is not None and inst_code or random.choice(self._inst_codes),
            Person.isAuthor: is_author is not None and is_author or bool(random.getrandbits(1)),
            Person.activityId: activity_id is not None and activity_id or random.choice(self._activities).get(MemberActivity.id),
            Person.infn: infn is not None and infn or self._random_string(1, 'abcd'),
            Person.univOther: univ_other is not None and univ_other or self._random_string(1, 'abcd'),
            Person.authorId: 'INSPIRE-%s' % self._random_string(length=7, charset='0123456789')
        })
        _mock_user = MockUserFactory.create_instance(person=_mock_person, instance_overrides={
            User.nameSignature: self._random_string(random.randint(11, 17)) if name_sign == '' else name_sign
        })
        return _mock_person, _mock_user

    @staticmethod
    @contextmanager
    def change_authors_count(target_count):
        """
        A context manager that TEMPORARILY alters the number of signing authors (restores all in cleanup part)
        :param target_count: desired number of authors
        """
        _current_count = db.session.query(Person).filter(Person.isAuthor == True).count()
        _flipped_cms_ids = set()
        if _current_count != target_count:
            q = db.session.query(Person)
            if _current_count < target_count:
                q = q.filter(Person.isAuthor == False).limit(target_count - _current_count)
            elif _current_count > target_count:
                q = q.filter(Person.isAuthor == True).limit(_current_count - target_count)
            for e in q.all():
                e.set(Person.isAuthor, target_count > _current_count)
                _flipped_cms_ids.add(e.get(Person.cmsId))
                db.session.add(e)
            db.session.commit()
            yield
            # inverting the previous condition to revert the authors count
            for p in db.session.query(Person).filter(Person.cmsId.in_(_flipped_cms_ids)).all():
                p.set(Person.isAuthor, target_count <= _current_count)
                db.session.add(p)
            db.session.commit()
        return

    @staticmethod
    @contextmanager
    def add_cms_person(p, u):
        """
        TEMPORARILY adds indicated person into the PeopleDB
        :param p: a mock Person instance
        :param u: a mock User instance
        :return:
        """
        print('Now actually adding the Person and the User to the DB')
        _cms_id = p.get(Person.cmsId)
        db.session.add(p)
        db.session.add(u)
        db.session.commit()
        yield _cms_id
        print('Now actually adding the Person and the User to the DB')
        _db_objects = db.session.query(Person, PersonHistory, User).join(PersonHistory, Person.cmsId == PersonHistory.cmsId).join(
            User, Person.cmsId == User.cmsId).filter(Person.cmsId == _cms_id).one_or_none()
        if _db_objects:
            print('And now these temporary entries are actually being deleted because they DO EXIST in the DB.')
            for _o in _db_objects:
                db.session.delete(_o)
        db.session.commit()


@pytest.fixture(scope='module')
def setup():
    MockPersonFactory.reset_overrides(Person)
    recreate_people_db()
    recreate_old_cadi_db()
    for mock in MockData.get_instance().mocks:
        db.session.add(mock)
        db.session.flush()
    db.session.commit()


@pytest.fixture(scope='module')
def mock_actor():
    return MockPersonFactory.create_instance(instance_overrides={
        Person.cmsId: 0, Person.hrId: 0, Person.instCode: 'CERN', Person.loginId: 'root'
    })


@pytest.fixture(scope='class')
def fresh_al_start(setup):
    print('Assuring there are no Author Lists in the DB')
    db.session.query(PAH).delete()
    db.session.query(PaperAuthor).delete()
    db.session.commit()


@pytest.fixture(scope='class')
def open_some_lists(fresh_al_start, mock_actor):
    """
    :return the mock data describing which lists have been opened
    """
    print('Generating some ALs')
    _mock_data = MockData.get_instance().test_data_some_lists_open_some_not
    for code, status in _mock_data:
        if status == 'open':
            m = AuthorListModel(code, mock_actor)
            m.open_author_list()
    return _mock_data


@pytest.fixture(scope='class')
def close_some_lists(fresh_al_start, mock_actor):
    # uses MockData's predefined set of closed / open author lists - the same can be later used in tests
    _mock_data = MockData.get_instance().test_data_some_lists_open_some_closed_some_neither
    for code, status in _mock_data:
        if status is not None:
            m = AuthorListModel(code, mock_actor, resync_after_commits=True)
            m.open_author_list()
            if status == 'closed':
                m.close_author_list()
    return _mock_data


@pytest.mark.parametrize('code', MockData.get_instance().test_data_analysis_codes)
def test_no_al_is_open(fresh_al_start, mock_actor, code):
    m = AuthorListModel(code=code, actor=mock_actor)
    assert not m.is_open()


@pytest.mark.parametrize('code', MockData.get_instance().test_data_analysis_codes)
def test_no_al_is_closed(fresh_al_start, mock_actor, code):
    m = AuthorListModel(code=code, actor=mock_actor)
    assert not m.is_closed()


@pytest.mark.parametrize('code', MockData.get_instance().test_data_analysis_codes)
def test_exceptions_are_thrown_when_al_is_not_yet_open(fresh_al_start, mock_actor, code):
    m = AuthorListModel(code=code, actor=mock_actor)
    with pytest.raises(AuthorListException):
        m.add_author({})
    with pytest.raises(AuthorListException):
        m.swap_authors({}, {})
    with pytest.raises(AuthorListException):
        m.add_external_author({})
    with pytest.raises(AuthorListException):
        m.remove_author({})
    with pytest.raises(AuthorListException):
        m.close_author_list()


@pytest.mark.parametrize('code, status', MockData.get_instance().test_data_some_lists_open_some_not)
def test_some_al_are_open(open_some_lists, mock_actor, code, status):
    assert len(AuthorListModel.get_open_author_list_codes()) > 0
    m = AuthorListModel(code, mock_actor)
    assert m.is_open() is (status == 'open')


@pytest.mark.parametrize('code, status', MockData.get_instance().test_data_some_lists_open_some_closed_some_neither)
def test_some_al_are_open_some_closed_some_neither(close_some_lists, mock_actor, code, status):
    assert len(AuthorListModel.get_open_author_list_codes()) > 0
    assert len(AuthorListModel.get_closed_author_list_codes()) > 0
    m = AuthorListModel(code, mock_actor)
    assert m.is_open() is (status == 'open')
    assert m.is_closed() is (status == 'closed')


class TestALGenerationInSequence(object):
    @pytest.mark.parametrize('code, authors_no, lists_no, authors_cumul', [
        ('TST-99-201', 60, 1, 60), ('XYZ-19-201', 80, 2, 140), ('OOO-00-000', 0, 3, 140), ('TDR-99-999', 100, 4, 240)
    ])
    def test_generate_author_list(self, fresh_al_start, mock_actor, code, authors_no, lists_no, authors_cumul):
        with MockData.change_authors_count(authors_no):
            assert authors_no == count_in_db(Person, {Person.isAuthor: True})
            m = AuthorListModel(code=code, actor=mock_actor)
            m.open_author_list()

            # there should only be one author_list_history entry for this one: ListGen
            assert_attr_val(PAH, {PAH.code: code}, {PAH.action: PAHActions.LIST_GEN})
            assert 1 == count_in_db(PAH, {PAH.code: code})
            # there should be :lists_no: of all lists already in the db
            assert lists_no == count_in_db(PAH, {})
            # there should be exactly :authors_no: authors on that list
            assert authors_no == count_in_db(PaperAuthor, {PaperAuthor.code: code, PaperAuthor.status: PAStatus.IN})
            # there should be exactly :authors_cumul: author entries in the DB
            assert authors_cumul == count_in_db(PaperAuthor, {PaperAuthor.status: PAStatus.IN})


class TestAddingAuthors(object):
    @pytest.mark.parametrize('person_mock, user_mock', MockData.get_instance().extra_non_author_mock_data)
    def test_add_extra_authors_to_al(self, close_some_lists, person_mock, user_mock, mock_actor):
        al_code = random.choice([_code for _code, _status in close_some_lists if _status=='open'])
        with MockData.add_cms_person(person_mock, user_mock) as added_cms_id:
            print('Adding a special author with CMS ID %d to AL %s' % (added_cms_id, al_code))
            m = AuthorListModel(code=al_code, actor=mock_actor)
            m.add_author(added_cms_id)
            assert 1 == count_in_db(PaperAuthor, {PaperAuthor.cmsid: added_cms_id}), \
                'This person should have been added to one and only one author list'
            assert_attr_val(PaperAuthor, {PaperAuthor.code: al_code, PaperAuthor.cmsid: added_cms_id},
                            {
                                PaperAuthor.nameSignature: user_mock.get(User.nameSignature),
                                PaperAuthor.univOther: person_mock.get(Person.univOther),
                                PaperAuthor.infn: person_mock.get(Person.infn)}
                            )

            assert_attr_val(PAH, {PAH.code: al_code, PAH.instCode: person_mock.get(Person.instCode)},
                            {PAH.action: PAHActions.ADD_MEMBER},
                            lambdas_list=[lambda q: q.filter(PAH.history.like('%{name}%{inst_code}%'.format(
                                inst_code=person_mock.get(Person.instCode),
                                name=user_mock.get(User.nameSignature) or person_mock.get(Person.lastName)
                            )))])

    def test_exceptions_are_raised_when_adding_to_not_open_al(self, close_some_lists, mock_actor):
        for _pick in range(0, 5):
            non_author_cms_id = random.choice(MockData.get_instance().get_non_author_cms_ids())
            al_code = random.choice([_code for _code, _status in close_some_lists if _status != 'open'])
            m = AuthorListModel(code=al_code, actor=mock_actor)
            with pytest.raises(AuthorListException):
                m.add_author(non_author_cms_id)

    def test_exceptions_are_raised_when_adding_an_already_present_author(self, close_some_lists, mock_actor):
        for _pick in range(0, 5):
            author_cms_id = random.choice(MockData.get_instance().get_author_cms_ids())
            al_code = random.choice([_code for _code, _status in close_some_lists if _status == 'open'])
            m = AuthorListModel(code=al_code, actor=mock_actor)
            with pytest.raises(AuthorListException):
                print('%d should already be an author of %s and thus the operation should fail...' % (author_cms_id, al_code))
                m.add_author(author_cms_id)


class TestPreservedAuthorDetails(object):
    @pytest.mark.parametrize('code, status', MockData.get_instance().test_data_some_lists_open_some_closed_some_neither)
    def test_personal_details_in_paper_author_table(self, close_some_lists, code, status):
        mock_data = MockData.get_instance()
        if not status:
            assert 0 == count_in_db(PaperAuthor, {PaperAuthor.code: code})
        else:
            paper_author_data = db.session.query(PaperAuthor.cmsid, PaperAuthor.nameSignature, PaperAuthor.infn,
                                                 PaperAuthor.univOther, PaperAuthor.inspireid). \
                filter(PaperAuthor.code == code).all()
            assert 0 < len(paper_author_data), 'There should be some PaperAuthor entries for %s' % code
            for (cms_id, name_sign, infn, univ_other, inspire_id) in paper_author_data:
                # stupid DB stores a string!
                cms_id = int(cms_id)
                # assert that everyone has their infn, univ_other, inspire id and sign name propagated correctly
                assert name_sign is not None and len(name_sign) > 0
                assert name_sign == mock_data.get_sign_name(cms_id)
                assert 0 < len(name_sign), 'Signing name cannot be of 0 length!'
                assert infn == mock_data.get_infn(cms_id)
                assert univ_other == mock_data.get_univ_other(cms_id)
                assert inspire_id == mock_data.get_inspire_id(cms_id)


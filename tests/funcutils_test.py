#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import time
from icmsutils.funcutils import Cache
import pytest
import hashlib
import sys

test_data_paramsets = [
    ([1, 2, 3, "someName"], {}), ([], {}), ([], {'name': None, 'status': 'gone'}),
    (['ole!'], {}), (['ole'], {}), ([], {'x': 13}), ([], {'x': 12, 'y': 14}), ([], {'x': 12}), ([1, 2], {})
]


@Cache.permanent
def worker_method(*args, **kwargs):
    txt = 'args: '
    for a in args:
        txt += '%s;' % str(a)
    txt += ' | kwargs: '
    for k, v in kwargs.items():
        txt += '%s/%s;' % (str(k), str(v))
    time.sleep(.1234)
    return hashlib.md5(txt.encode('utf-8')).digest()


@Cache.expiring(timeout=0.1)
def expiring_worker(*args, **kwargs):
    counter = getattr(expiring_worker, 'counter', 0) + 1
    setattr(expiring_worker, 'counter', counter)
    return hash(str(args) + str(kwargs)) + counter

    
@pytest.mark.parametrize('args, kwargs', test_data_paramsets)
def test_cached(args, kwargs):

    start_time = time.time()
    first_result = worker_method(*args, **kwargs)
    first_exec_time = time.time() - start_time

    for i in range(0, 10):
        start_time = time.time()
        result = worker_method(*args, **kwargs)
        exec_time = time.time() - start_time
        assert result == first_result
        assert exec_time < 0.01 * first_exec_time, 'Cached execution time: %.8f s, initial execution time: %.8f s' % \
                                              (exec_time, first_exec_time)


@pytest.mark.parametrize('args, kwargs', test_data_paramsets)
def test_expiring_cache(args, kwargs):
    first_result = expiring_worker(*args, **kwargs)
    for i in range(0, 10):
        time.sleep(0.001)
        assert expiring_worker(*args, **kwargs) == first_result
    time.sleep(0.1)
    assert expiring_worker() != first_result



@pytest.mark.parametrize('args, kwargs', test_data_paramsets)
def test_args_roundtrip(args, kwargs):
    assert Cache._from_hashable(Cache._to_hashable(args, kwargs)) == (args, kwargs)



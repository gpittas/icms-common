#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from icmsutils.icmstest.ddl import recreate_people_db
from icmsutils.icmstest.mock import MockInstFactory, MockPersonFactory
from scripts.prescript_setup import db, config
from icmsutils.businesslogic import teamleaders as logic
import pytest
import datetime
import random
from icms_orm.cmspeople import Person, Institute
from icmsutils.exceptions import IcmsException, IcmsInsufficientRightsException


GVA = 'GVA'
ZRH = 'ZRH'
inst_codes = [GVA, ZRH]

def cbi(inst):
    return db.session.query(Person).join(Institute, Institute.cbiCmsId == Person.cmsId).filter(Institute.code == inst).one()


def suspended_ppl(inst):
    return db.session.query(Person).filter(Person.instCode == inst).filter(Person.isAuthorSuspended == True).all()


def not_suspended_not_authors(inst):
    return db.session.query(Person).filter(Person.instCode == inst).filter(Person.isAuthorSuspended == False).filter(Person.isAuthor == False).all()


def authors(inst):
    return db.session.query(Person).filter(Person.instCode == inst).filter(Person.isAuthor == True).all()


@pytest.fixture
def setup_stage():
    recreate_people_db()
    gva = MockInstFactory.create_instance({Institute.code: GVA, Institute.country: 'Switzerland', Institute.name: 'Geneva'})
    zrh = MockInstFactory.create_instance({Institute.code: ZRH, Institute.country: 'Switzerland', Institute.name: 'Zurich'})

    db.session.add(gva)
    db.session.add(zrh)
    db.session.flush()

    MockPersonFactory.set_overrides(Person, {Person.instCode: GVA})
    cbi_gva = MockPersonFactory.create_instance()
    db.session.add(cbi_gva)
    db.session.flush()
    gva.cbiCmsId = cbi_gva.cmsId
    db.session.flush()

    cbi_zrh = MockPersonFactory.create_instance({Person.instCode: ZRH})
    db.session.add(cbi_zrh)
    db.session.flush()
    zrh.cbiCmsId = cbi_zrh.cmsId
    db.session.flush()

    ## CREATE SOME DEPTH BY ADDING VARIOUS PEOPLE
    total = 1000
    while total > 0:
        total -= 1
        mock_person = MockPersonFactory.create_instance(instance_overrides={
            Person.instCode: random.choice(inst_codes),
            Person.isAuthorSuspended: bool(random.getrandbits(1)),
            Person.isAuthor: bool(random.getrandbits(1)),
        })
        mock_person.isAuthor = mock_person.isAuthor and not mock_person.isAuthorSuspended
        db.session.add(mock_person)
    db.session.commit()


@pytest.mark.parametrize('inst_code', inst_codes)
def test_cbi_can_suspend_their_people(setup_stage, inst_code):
    the_cbi = cbi(inst_code)
    people = not_suspended_not_authors(inst_code)

    initial_count = len(people)
    assert initial_count > 0

    input = [r.cmsId for r in random.sample(people, 3)]

    logic.members_epr_change_suspension(db.session, input, the_cbi.cmsId, True)

    for p in db.session.query(Person).filter(Person.cmsId.in_(input)).all():
        assert getattr(p, Person.isAuthorSuspended.key) == True

    assert len(not_suspended_not_authors(inst_code)) == initial_count - len(input)


@pytest.mark.parametrize('inst_code', inst_codes)
def test_cbi_can_unsuspend_their_people(setup_stage, inst_code):
    the_cbi = cbi(inst_code)
    people = suspended_ppl(inst_code)

    initial_count = len(people)
    assert initial_count > 0

    input = [r.cmsId for r in random.sample(people, 4)]

    logic.members_epr_change_suspension(db.session, input, the_cbi.cmsId, False)

    for p in db.session.query(Person).filter(Person.cmsId.in_(input)).all():
        assert getattr(p, Person.isAuthorSuspended.key) == False

    assert len(suspended_ppl(inst_code)) == initial_count - len(input)


@pytest.mark.parametrize('inst_code', inst_codes)
def test_foreign_cbis_cannot_change_suspensions(setup_stage, inst_code):
    other_cbi = cbi([ic for ic in inst_codes if ic != inst_code][0])
    people = suspended_ppl(inst_code)

    sample = [p.cmsId for p in random.sample(people, 2)]

    with pytest.raises(Exception) as e:
        logic.members_epr_change_suspension(db.session, actor_cms_id=other_cbi.cmsId, suspendable_cms_ids=sample, up_or_down=False)
    assert isinstance(e.value, IcmsInsufficientRightsException)


@pytest.mark.parametrize('inst_code', inst_codes)
def regular_team_members_connot_change_suspensions(setup_stage, inst_code):
    member = random.choice(db.session.query(Person).join(Institute, Person.instCode == Institute.code).
                           filter(Person.cmsId != Institute.cbiCmsId).filter(Person.instCode == inst_code).all())
    people = not_suspended_not_authors(inst_code)

    with pytest.raises(Exception) as e:
        logic.members_epr_change_suspension(db.session, actor_cms_id=member.cmsId, suspendable_cms_ids=[p.cmsId for p in people], up_or_down=True)
    assert isinstance(e.value, IcmsInsufficientRightsException)


# def test_admins_can_change_suspensions(setup_stage):
#     # not implemented yet!
#     pass


@pytest.mark.parametrize('inst_code', inst_codes)
def test_cannot_suspend_an_author(setup_stage, inst_code):
    the_cbi = cbi(inst_code)
    some_authors = random.sample(authors(inst_code), 5)

    with pytest.raises(Exception) as exc_info:
        logic.members_epr_change_suspension(db.session, actor_cms_id=the_cbi.cmsId,
                                            suspendable_cms_ids=[p.cmsId for p in some_authors], up_or_down=True)

    assert isinstance(exc_info.value, IcmsException)


@pytest.mark.parametrize('inst_code', inst_codes)
def test_cannot_unsuspend_too_soon(setup_stage, inst_code):
    the_cbi = cbi(inst_code)
    some_people = random.sample(suspended_ppl(inst_code), 5)
    for p in some_people:
        setattr(p, Person.dateAuthorSuspension.key, datetime.date.today() - datetime.timedelta(days=21))
        db.session.add(p)
    db.session.commit()

    with pytest.raises(Exception) as exc_info:
        logic.members_epr_change_suspension(db.session, actor_cms_id=the_cbi.cmsId,
                                            suspendable_cms_ids=[p.cmsId for p in some_people], up_or_down=False)
    assert isinstance(exc_info.value, IcmsException)
    assert all([x in str(exc_info.value) for x in ['changed', 'recently']])


@pytest.mark.parametrize('inst_code', inst_codes)
def test_cannot_suspend_too_soon(setup_stage, inst_code):
    the_cbi = cbi(inst_code)
    some_people = random.sample(not_suspended_not_authors(inst_code), 5)
    for p in some_people:
        setattr(p, Person.dateAuthorUnsuspension.key, datetime.date.today() - datetime.timedelta(days=21))
        db.session.add(p)
    db.session.commit()

    with pytest.raises(Exception) as exc_info:
        logic.members_epr_change_suspension(db.session, actor_cms_id=the_cbi.cmsId,
                                            suspendable_cms_ids=[p.cmsId for p in some_people], up_or_down=True)
    assert isinstance(exc_info.value, IcmsException)
    assert all([x in str(exc_info.value) for x in ['changed', 'recently']])

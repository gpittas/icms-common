#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from icmsutils.dbutils import pg_parse_login_pass_host_db
import pytest

conn_strs_data = [
    ('postgresql+psycopg2://epr_test:icms_test@localhost/icms_test', 'epr_test', 'icms_test', 'localhost', 'icms_test'),
    ('postgresql+psycopg2://icms_test:icms_test@localhost/icms_test', 'icms_test', 'icms_test', 'localhost', 'icms_test'),
    ('postgresql+psycopg2://toolkit_test:icms_test_pwd123@localhost/icms_test', 'toolkit_test', 'icms_test_pwd123', 'localhost', 'icms_test')
]


@pytest.mark.parametrize('s, usr, pwd, hst, db', conn_strs_data)
def test_parse_connection_string(s, usr, pwd, hst, db):
    r = pg_parse_login_pass_host_db(s)
    assert usr == r[0]
    assert pwd == r[1]
    assert hst == r[2]
    assert db == r[3]




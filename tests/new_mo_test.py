#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from scripts.prescript_setup import db
from icmsutils.businesslogic.mo_new import MoModel
from icms_orm.common import Person as NPerson, Institute as NInst, FundingAgency as NFA, MO as NMO, MoStatusValues, MO
from icms_orm.cmspeople import Person, Institute, MoData, PseudoEnum, MemberStatusValues

from scripts.sync import ForwardSyncManager, CountriesSyncAgent, InstitutesSyncAgent, PeopleSyncAgent, FinalMoSyncAgent, \
    FundingAgenciesSyncAgent
from icmsutils.icmstest.mock import MockPersonFactory, MockPersonMoFactory, MockInstFactory, MockUserFactory, MockMoFactory
from icmsutils.icmstest.assertables import count_in_db
from icmsutils.icmstest.ddl import recreate_common_db, recreate_people_db
import pytest
import copy


class Data(object):

    class Country(PseudoEnum):
        RUSSIA = 'Russia'
        AUSTRALIA = 'Australia'
        USA = 'United States'

    class IC(PseudoEnum):
        SOUTH = 'SOUTH-INST'
        WEST = 'WEST-INST'
        EAST = 'EAST-INST'
        NORTH = 'NORTH-INST'
        MOSCOW = 'MOSCOW-INST'
        SYDNEY = 'SYDNEY-INST'

    class FA(PseudoEnum):
        SE = 'United States-NSEFA'
        W = 'United States-WFA'
        N = 'United States-NFA'
        RU = 'Russia-1'
        AU = 'Australia'

    @staticmethod
    def _inst_country(ic=None):
        return {Data.IC.MOSCOW: Data.Country.RUSSIA, Data.IC.SYDNEY: Data.Country.AUSTRALIA}.get(ic, Data.Country.USA)

    @staticmethod
    def _inst_fa(ic=None):
        return {Data.IC.SOUTH: Data.FA.SE, Data.IC.WEST: Data.FA.W, Data.IC.NORTH: Data.FA.N, Data.IC.EAST: Data.FA.SE,
                Data.IC.MOSCOW: Data.FA.RU, Data.IC.SYDNEY: Data.FA.AU}.get(ic)

    @staticmethod
    def _paid_phds_count(ic=None):
        return {
            Data.IC.NORTH: 7, Data.IC.SOUTH: 0, Data.IC.WEST: 2, Data.IC.EAST: 3, Data.IC.MOSCOW: 1, Data.IC.SYDNEY: 17
        }.get(ic, 0)

    @staticmethod
    def _post_approval_swaps_count(ic=None):
        return {Data.IC.SYDNEY: 3, Data.IC.WEST: 1}.get(ic, 0)

    @staticmethod
    def _late_comers_free_mo_count(ic=None):
        return {Data.IC.SOUTH: 3, Data.IC.WEST: 1, None: 5}.get(ic, 0)

    @staticmethod
    def _count_paid_phds_per_fa(fa):
        return sum([Data._paid_phds_count(_ic) for _ic in Data.IC.values() if Data._inst_fa(_ic) == fa])

    __instance = None

    def __init__(self, year=2019):
        self._insts = {}
        self._people = {}
        self.__swap_data={}
        self._users = {}
        self._mo = {}
        self._phd_fa_by_cms_id = {}
        self._year = year
        self._ic_by_cms_id = {}
        self._paid_for_cms_ids_by_inst = {}
        self._unpaid_for_cms_ids_by_inst = {}
        self._late_free_cms_ids_by_inst = {}
        self.__init_insts()
        self.__init_people()
        self.__init_swap_data()

    def _get_phd_fa_by_cms_id(self, cms_id):
        return self._inst_fa(self._ic_by_cms_id.get(cms_id))

    def _get_paid_for_cms_ids_by_ic(self, ic):
        return self._paid_for_cms_ids_by_inst.get(ic, [])

    def _get_unpaid_for_cms_ids_by_ic(self, ic):
        return self._unpaid_for_cms_ids_by_inst.get(ic, [])

    def __init_insts(self):
        for _ic in Data.IC.values():
            self._insts[_ic] = MockInstFactory.create_instance(instance_overrides={
                Institute.code: _ic,
                Institute.country: Data._inst_country(_ic),
                Institute.fundingAgency: Data._inst_fa(_ic)
            })

    def __init_people(self):
        (mo_col, phd_mo_col, free_mo_col, phd_ic_col, phd_fa_col) = MoData.get_columns_for_year(self._year)
        for _ic in (Data.IC.values() + [None]):
            self._paid_for_cms_ids_by_inst[_ic] = []
            self._unpaid_for_cms_ids_by_inst[_ic] = []
            self._late_free_cms_ids_by_inst[_ic] = []

            for _count_fn in [Data._paid_phds_count, Data._post_approval_swaps_count, Data._late_comers_free_mo_count]:
                for _i in range(0, _count_fn(_ic)):
                    _p = MockPersonFactory.create_instance({Person.instCode: _ic})
                    _u = MockUserFactory.create_instance({}, person=_p)
                    _mo = MockPersonMoFactory.create_instance(
                        instance_overrides=(_count_fn == Data._paid_phds_count and {mo_col: 'YES', phd_mo_col: 'YES', free_mo_col: 'NO', phd_ic_col: _ic, phd_fa_col: Data._inst_fa(_ic)} or {}),
                        person=_p)
                    self._people[_p.get(Person.cmsId)] = _p
                    self._users[_p.get(Person.cmsId)] = _u
                    self._mo[_p.get(Person.cmsId)] = _mo
                    self._ic_by_cms_id[_p.get(Person.cmsId)] = _ic
                    (
                        _count_fn == Data._paid_phds_count and self._paid_for_cms_ids_by_inst or
                        _count_fn == Data._post_approval_swaps_count and self._unpaid_for_cms_ids_by_inst or
                        _count_fn == Data._late_comers_free_mo_count and self._late_free_cms_ids_by_inst
                     )[_ic].append(_p.get(Person.cmsId))

    def __init_swap_data(self):
        for _ic in self.IC.values():
            _swaps = self._post_approval_swaps_count(_ic)
            if _swaps:
                _cms_ids_to_swap_out = self._get_paid_for_cms_ids_by_ic(_ic)[0:_swaps]
                _cms_ids_to_swap_in = self._get_unpaid_for_cms_ids_by_ic(_ic)[0:_swaps]
                for _id_out, _id_in in zip(_cms_ids_to_swap_out, _cms_ids_to_swap_in):
                    self.__swap_data[_id_out] = _id_in

    def grant_post_sync_free_latecomers(self):
        for _ic in self.IC.values() + [None]:
            for _cms_id in self._late_free_cms_ids_by_inst.get(_ic, []):
                _mock = MockMoFactory.create_instance(
                    {NMO.cms_id: _cms_id, NMO.status: MoStatusValues.APPROVED_FREE_LATECOMER, NMO.year: self.year,
                     NMO.inst_code: _ic})
                db.session.add(_mock)
        db.session.commit()

    @property
    def late_free_cms_ids_with_ic(self):
        _res = set()
        for _ic in (self.IC.values()):
            _res.update(set(self._late_free_cms_ids_by_inst.get(_ic, {})))
        return _res

    @property
    def late_free_cms_ids_without_ic(self):
        return set(self._late_free_cms_ids_by_inst.get(None, {}))

    def make_post_sync_swaps(self):
        """
        Putting it here disrupts the structure of tests but the swap records can only be written after the sync
        """
        _fa_map = {_fa.get(NFA.name): _fa.get(NFA.id) for _fa in db.session.query(NFA).all()}
        for _id_out, _id_in in self.__swap_data.items():
            _ic = self._ic_by_cms_id.get(_id_out)
            _fa_id = _fa_map.get(self._get_phd_fa_by_cms_id(_id_out))
            self.__swap_data[_id_out] = _id_in
            _mock_out = MockMoFactory.create_instance(
                {NMO.cms_id: _id_out, NMO.status: MoStatusValues.SWAPPED_OUT, NMO.year: self.year,
                 NMO.inst_code: _ic, NMO.fa_id: _fa_id})
            _mock_in = MockMoFactory.create_instance(
                {NMO.cms_id: _id_in, NMO.status: MoStatusValues.APPROVED_SWAPPED_IN, NMO.year: self.year,
                 NMO.inst_code: _ic, NMO.fa_id: _fa_id})
            db.session.add(_mock_out)
            db.session.add(_mock_in)
        db.session.commit()

    @property
    def swapped_out_cms_ids(self):
        return self.__swap_data.keys()

    @property
    def swapped_in_cms_ids(self):
        return self.__swap_data.values()

    @classmethod
    def populate_db(cls):
        _data = cls.i()

        for _dict in [_data._insts, _data._people, _data._users, _data._mo]:
            for _i in _dict.values():
                # adding a deep copy to the DB so that the mock can remain in its 'pristine' state (to be reusable)
                _c = copy.deepcopy(_i)
                db.session.add(_c)
            db.session.flush()

        db.session.commit()

    @classmethod
    def i(cls):
        if not Data.__instance:
            Data.__instance = Data()
        return Data.__instance

    @property
    def year(self):
        return self._year


@pytest.fixture(scope='class')
def setup():
    # Data.reset()
    print( 'SETUP INVOKED!' )
    recreate_people_db()
    recreate_common_db()
    Data.populate_db()

    ForwardSyncManager.launch_sync(
        agent_classes_override=[CountriesSyncAgent, FundingAgenciesSyncAgent, InstitutesSyncAgent, PeopleSyncAgent, FinalMoSyncAgent])

    # swaps and such should only be done after the sync
    _data = Data.i()
    return _data


@pytest.fixture(scope='class')
def setup_with_post_sync_swaps(setup):
    _data = setup
    _data.make_post_sync_swaps()
    return _data


@pytest.fixture(scope='class')
def setup_with_free_mo(setup):
    _data = setup
    _data.grant_post_sync_free_latecomers()
    return _data


@pytest.fixture(scope='class')
def fa_map(setup):
    return {r[0]: r[1] for r in db.session.query(NFA.name, NFA.id).all()}


@pytest.fixture(scope='class')
def mo_model(setup):
    assert isinstance(setup, Data)
    return MoModel.get_instance(year=setup.year, mode=MoModel.Mode.OFFICIAL)


@pytest.fixture(scope='class')
def pre_app_mo_model(setup):
    assert isinstance(setup, Data)
    return MoModel.get_instance(year=setup.year, mode=MoModel.Mode.PRE_APPROVAL)


@pytest.fixture(scope='class')
def post_app_mo_model(setup_with_post_sync_swaps):
    assert isinstance(setup_with_post_sync_swaps, Data)
    return MoModel.get_instance(year=setup_with_post_sync_swaps.year, mode=MoModel.Mode.POST_APPROVAL)


@pytest.fixture(scope='class')
def post_latecomers_mo_model(setup_with_free_mo):
    assert isinstance(setup_with_free_mo, Data)
    return MoModel.get_instance(year=setup_with_free_mo.year, mode=MoModel.Mode.POST_APPROVAL)


class TestMoModel(object):
    @pytest.mark.parametrize('inst_code', Data.IC.values())
    def test_inst_has_correct_number_of_paid_phds(self, mo_model, inst_code):
        assert isinstance(mo_model, MoModel)
        assert Data._paid_phds_count(inst_code) == mo_model.count_by_inst_code(inst_code)

    @pytest.mark.parametrize('fa_name', Data.FA.values())
    def test_fa_has_correct_number_of_paid_phds(self, mo_model, fa_map, fa_name):
        assert isinstance(mo_model, MoModel)
        assert Data._count_paid_phds_per_fa(fa_name) == mo_model.count_by_fa_name(fa_name)

    @pytest.mark.parametrize('fa_name', Data.FA.values())
    def test_fa_records_count_matches(self, mo_model, fa_name):
        assert isinstance(mo_model, MoModel)
        assert sum([Data._paid_phds_count(_ic) for _ic in Data.IC.values() if Data._inst_fa(_ic) == fa_name]) == len(mo_model.get_records(inst_code=None, fa_name=fa_name))

    @pytest.mark.parametrize('inst_code', Data.IC.values())
    def test_inst_fa_records_count_matches(self, mo_model, inst_code):
        assert isinstance(mo_model, MoModel)
        _recs = mo_model.get_records(fa_name=Data._inst_fa(inst_code), inst_code=inst_code)
        assert Data._paid_phds_count(inst_code) == len(_recs)

    def test_total_records_count_matches(self, mo_model):
        assert isinstance(mo_model, MoModel)
        _records = mo_model.get_records()
        assert sum([Data._count_paid_phds_per_fa(_fa) for _fa in Data.FA.values()]) == len(_records)

    def test_total_records_count_still_matches_after_swaps(self, setup_with_post_sync_swaps, post_app_mo_model):
        assert isinstance(post_app_mo_model, MoModel)
        _records = post_app_mo_model.get_records()
        assert sum([Data._count_paid_phds_per_fa(_fa) for _fa in Data.FA.values()]) == len(_records)

    @pytest.mark.parametrize('cms_id', Data.i().swapped_out_cms_ids)
    def test_swapped_out_do_not_show_in_post_app_model(self, post_app_mo_model, cms_id):
        assert isinstance(post_app_mo_model, MoModel)
        assert cms_id not in {_r.person.cms_id for _r in post_app_mo_model.get_records()}

    @pytest.mark.parametrize('cms_id', Data.i().swapped_in_cms_ids)
    def test_swapped_in_do_show_in_post_app_model(self, post_app_mo_model, cms_id):
        assert isinstance(post_app_mo_model, MoModel)
        assert cms_id in {_r.person.cms_id for _r in post_app_mo_model.get_records()}


class TestMoModelWithFreeLatecomers(object):
    @pytest.mark.parametrize('cms_id', Data.i().late_free_cms_ids_with_ic)
    def test_free_latecomers_with_ic_show_up_in_post_approval_mo_model(self, cms_id, post_latecomers_mo_model):
        _model = post_latecomers_mo_model
        assert isinstance(_model, MoModel)
        assert cms_id in {_r.person.cms_id for _r in _model.get_records()}

    @pytest.mark.parametrize('cms_id', Data.i().late_free_cms_ids_without_ic)
    def test_free_latecomers_without_ic_show_up_in_post_approval_mo_model(self, cms_id, post_latecomers_mo_model):
        _model = post_latecomers_mo_model
        assert isinstance(_model, MoModel)
        assert cms_id in {_r.person.cms_id for _r in _model.get_records()}

    @pytest.mark.parametrize('inst_code, free_late_count', [(_ic, Data._late_comers_free_mo_count(_ic)) for _ic in Data.IC.values() + [None]])
    def test_if_latecomers_are_in_the_db(self, inst_code, free_late_count):
        assert free_late_count == count_in_db(MO, {
            MO.year: Data.i().year, MO.inst_code: inst_code,
            MO.status: MoStatusValues.APPROVED_FREE_LATECOMER
        })


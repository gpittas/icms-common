import pytest
from icms_orm.common import Person, OrgUnit, OrgUnitType, OrgUnitTypeName, PositionName, Position, Tenure
from icmsutils.icmstest.ddl import recreate_common_db
from icmsutils.icmstest.mock import MockNewPersonFactory
from scripts import populate_org_unit_tables


@pytest.fixture(scope='class')
def reset_db():
    recreate_common_db()


@pytest.fixture(scope='class')
def create_people():
    for wrap in populate_org_unit_tables.Data.get_wraps():
        OrgUnit.session().add(MockNewPersonFactory.create_instance({Person.cms_id: wrap.cms_id}))
    OrgUnit.session().commit()


@pytest.fixture(scope='class')
def insert_org_units_as_of_2018(reset_db):
    return populate_org_unit_tables.insert_org_units_as_of_2018()


@pytest.fixture(scope='class')
def insert_positions(insert_org_units_as_of_2018):
    unit_types = insert_org_units_as_of_2018[1]
    return populate_org_unit_tables.insert_positions(unit_types)


@pytest.fixture(scope='class')
def insert_2018_tenures(insert_org_units_as_of_2018, insert_positions, create_people):
    return populate_org_unit_tables.insert_2018_tenures(units=insert_org_units_as_of_2018[0], positions=insert_positions)


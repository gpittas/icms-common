"""
This package contains UTILITIES FOR TESTING, NOT TESTS FOR UTILITIES
"""
from icmscommon import DbGovernor, ConfigGovernor


def _check_test_config_enabled():
    """
    A check method that is invoked below.
    Will raise an exception whenever this package is imported and config is not set for testing.
    """
    if not ConfigGovernor.check_test_mode():
        raise RuntimeError('Testing option disabled in the config. Please enable it if you are indeed trying to run the '
                           'tests. Otherwise please neither use nor import the testutils module.')

# RUN IT AS SOON AS THE INTERPRETER CAN - THIS CODE SHOULD ONLY RUN OR BE IMPORTED DURING TESTS.
_check_test_config_enabled()

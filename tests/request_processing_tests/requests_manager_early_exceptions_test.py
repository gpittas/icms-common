from icmsutils.businesslogic.requests import IllegalRequestExecutorClassException
from icmsutils.businesslogic.requests import IllegalRequestProcessorClassException, RequestProcessingException
from icmsutils.icmstest.assertables import count_in_db, assert_attr_val
from .fixtures import *


class TestRequestRegistrationExceptions(object):
    @pytest.mark.parametrize('name, attempt', [('DUMMY', 1), ('SILENT', 1), ('DUMMY', 2), ('AWESOME', 1), ('DUMMY', 3)])
    def test_that_re_registering_request_type_raises_exception(self, name, attempt):
        if attempt > 1:
            with pytest.raises(RequestProcessingException):
                RequestsManager.register_request_type_name_with_processor_and_executor(
                    executor_class=LazyRequestExecutor, processor_class=LazyRequestProcessor, request_type_name=name
                )
        else:
            RequestsManager.register_request_type_name_with_processor_and_executor(
                executor_class=LazyRequestExecutor, processor_class=LazyRequestProcessor, request_type_name=name
            )

        assert name in RequestsManager.get_registered_request_types()

    # Passing a formally correct executor and a problematic processor
    @pytest.mark.parametrize('processor_class', [None, str, LazyRequestExecutor, BaseRequestProcessor])
    def test_that_registering_unsuitable_processor_raises_exception(self, processor_class):
        with pytest.raises(IllegalRequestProcessorClassException):
            RequestsManager.register_request_type_name_with_processor_and_executor(
                executor_class=BaseRequestExecutor, request_type_name='ILLEGAL_REQUEST', processor_class=processor_class
            )
        assert 'ILLEGAL_REQUEST' not in RequestsManager.get_registered_request_types()

    # Passing a valid (formally at least) processor with an unsupported executor
    @pytest.mark.parametrize('executor_class', [None, str, BaseRequestProcessor, RequestsManager])
    def test_that_registering_unsuitable_executor_raises_exception(self, executor_class):
        with pytest.raises(IllegalRequestExecutorClassException):
            RequestsManager.register_request_type_name_with_processor_and_executor(
                executor_class=executor_class, request_type_name='ILLEGAL_REQUEST', processor_class=LazyRequestProcessor
            )
        assert 'ILLEGAL_REQUEST' not in RequestsManager.get_registered_request_types()


class TestRequestProcessingExceptions(object):
    @pytest.mark.parametrize('cms_id', [1, 13, 7])
    def test_that_request_creation_constraints_work(self, setup_for_request_processing, cms_id):
        actor = ActorData(cms_id=cms_id)
        work = lambda : RequestsManager.create_request('CREATABLE_BY_13_REQUEST', actor)
        if cms_id == 13:
            work()
            assert 1 == count_in_db(Request, {Request.type: 'CREATABLE_BY_13_REQUEST'})
        else:
            with pytest.raises(RequestProcessingException):
                work()

    def test_that_submitting_request_of_unsupported_type_raises_exception(self, setup_for_request_processing):
        with pytest.raises(RequestProcessingException):
            RequestsManager.create_request('FAKE_TYPE', ActorData(cms_id=1), remarks='')

    def test_that_failed_request_execution_raises_exception_and_leaves_db_in_correct_state(self, setup_for_request_processing):
        with pytest.raises(Exception):
            RequestsManager.create_request('BOTCHED_REQUEST', ActorData(cms_id=1), remarks='')
        assert 1 == count_in_db(Request, {Request.type: 'BOTCHED_REQUEST'})
        assert_attr_val(Request, {Request.type: 'BOTCHED_REQUEST'}, {Request.status: RequestStatusValues.FAILED_TO_EXECUTE})


class TestUnimplementedMethodsRaiseExceptions(object):
    @pytest.mark.parametrize('req_type', ['BROKEN_REQUEST', 'STILL_BROKEN_REQUEST'])
    def test_that_processors_with_abstract_methods_wont_get_instantiated(self, setup_for_request_processing, req_type):
        with pytest.raises(TypeError):
            RequestsManager.create_request(req_type, ActorData(cms_id=7), remarks='This should be going nowhere!')

    # def test_something_else(self):

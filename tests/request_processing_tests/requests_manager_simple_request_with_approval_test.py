from sqlalchemy.orm import joinedload, subqueryload
from .fixtures import *
from icmsutils.businesslogic.requests import RequestProcessingException
from icmsutils.icmstest.assertables import count_in_db, assert_attr_val


# duplicating the dictionaries so that one gets passed to the fixture, another as test function argument

_basic_requests_data = [
        {'request_type': 'NEW_INST_REQUEST', 'actor_cms_id': 7, 'code': 'AVU', 'name': 'A Very University', 'decision_maker_id': 11},
        {'request_type': 'NEW_INST_REQUEST', 'actor_cms_id': 2, 'code': 'AFU', 'name': 'A Fine University', 'decision_maker_id': 7},
        {'request_type': 'NEW_INST_REQUEST', 'actor_cms_id': 12, 'code': 'UOE', 'name': 'University of Everything', 'decision_maker_id': 11},
        {'request_type': 'NEW_INST_REQUEST', 'actor_cms_id': 1, 'code': 'TGU', 'name': 'The Great University', 'decision_maker_id': 11},
    ]

_new_insts_request_data = [(x, x) for x in _basic_requests_data]


class TestAcceptingNewInstRequest(object):

    @pytest.mark.parametrize('submit_inst_create_request, req_params', _new_insts_request_data, indirect=['submit_inst_create_request'])
    def test_authorized_people_can_accept_request(self, submit_inst_create_request, req_params):
        decision_maker_id = req_params['decision_maker_id']
        new_inst_code = req_params['code']
        req_id = submit_inst_create_request

        RequestsManager.approve_step(request_id=req_id, actor_data=ActorData(cms_id=decision_maker_id), remarks='Test must go on!')
        assert_attr_val(RequestStep, {RequestStep.request_id: req_id},
                        {RequestStep.status: RequestStepStatusValues.APPROVED, RequestStep.remarks: 'Test must go on!'})
        assert_attr_val(Request, {Request.id: req_id}, {Request.status: RequestStatusValues.EXECUTED})
        assert 1 == count_in_db(Institute, {Institute.code: new_inst_code})
        # todo: test for request status, test for all the details like who approved etc.

    @pytest.mark.parametrize('submit_inst_create_request, req_params', _new_insts_request_data, indirect=['submit_inst_create_request'])
    def test_steps_are_joined_to_request(self, submit_inst_create_request, req_params):
        req_id = submit_inst_create_request
        request = RequestsManager.get_request_data(req_id)
        assert 1 == len(request.steps), 'There should be exactly 1 step for this request so far, but there are %d' % len(request.steps)

    @pytest.mark.parametrize('submit_inst_create_request, req_params', _new_insts_request_data, indirect=['submit_inst_create_request'])
    def test_non_authorized_people_cannot_accept_request(self, submit_inst_create_request, req_params):
        decision_maker_id = (req_params['decision_maker_id'] + 1) % 11
        new_inst_code = req_params['code']
        req_id = submit_inst_create_request

        with pytest.raises(RequestProcessingException):
            RequestsManager.approve_step(request_id=req_id, actor_data=ActorData(cms_id=decision_maker_id), remarks='Test must go on!')

        assert_attr_val(RequestStep, {RequestStep.request_id: req_id}, {RequestStep.status: RequestStepStatusValues.PENDING})
        assert_attr_val(Request, {Request.id: req_id}, {Request.status: RequestStatusValues.PENDING_APPROVAL})


class TestRejectingNewInstRequest(object):
    @pytest.mark.parametrize('submit_inst_create_request, req_params', _new_insts_request_data, indirect=['submit_inst_create_request'])
    def test_authorized_people_can_reject_request(self, submit_inst_create_request, req_params):
        decision_maker_id = req_params['decision_maker_id']
        new_inst_code = req_params['code']
        req_id = submit_inst_create_request

        RequestsManager.reject_step(request_id=req_id, actor_data=ActorData(cms_id=decision_maker_id), remarks='Test must stop!')
        assert_attr_val(RequestStep, {RequestStep.request_id: req_id}, {RequestStep.status: RequestStepStatusValues.REJECTED, RequestStep.remarks: 'Test must stop!'})
        assert_attr_val(Request, {Request.id: req_id}, {Request.status: RequestStatusValues.REJECTED})
        assert 0 == count_in_db(Institute, {Institute.code: new_inst_code})


class TestListingPending(object):
    @pytest.mark.parametrize('bulk_submit_requests', [_basic_requests_data, ], indirect=['bulk_submit_requests'])
    def test_all_pending_steps_show_up_when_not_filtering_by_cms_id(self, bulk_submit_requests):
        assert len(_basic_requests_data) == len(RequestsManager.get_request_steps_pending_action())

    @pytest.mark.parametrize('bulk_submit_requests', [_basic_requests_data, ], indirect=['bulk_submit_requests'])
    def test_steps_show_up_as_pending_for_authorized_people(self, bulk_submit_requests):
        _cms_ids = {_e['decision_maker_id'] for _e in _basic_requests_data}
        _counts = {_id: len([_e for _e in _basic_requests_data if _e.get('decision_maker_id') == _id]) for _id in _cms_ids}
        for cms_id in _cms_ids:
            assert _counts[cms_id] == len(RequestsManager.get_request_steps_pending_action(ActorData(cms_id=cms_id)))


class TestCreatingSlightlyComplexRequest(object):
    @pytest.mark.parametrize('submit_inst_create_request, req_params', _new_insts_request_data, indirect=['submit_inst_create_request'])
    def test_creating_complex_request_leaves_steps_in_db(self, submit_inst_create_request, req_params):
        _req_id = submit_inst_create_request
        _actor_cms_id = req_params['actor_cms_id']

        _req = Request.session().query(Request).filter(Request.creator_cms_id == _actor_cms_id).\
            filter(Request.type == 'NEW_INST_REQUEST').options(joinedload('steps')).one()
        assert 1 == count_in_db(RequestStep, {RequestStep.request_id: _req_id}), 'There should be exactly one RequestStep entry associated with request ID %d' % _req_id
        assert 1 == len(_req.get(Request.steps))
        assert _req.get(Request.steps)[0].get(RequestStep.status) == RequestStepStatusValues.PENDING
        assert _req.get(Request.status) == RequestStatusValues.PENDING_APPROVAL


class TestCreatingDelayedExecutionRequests(object):
    def test_creating_simple_delayed_execution_request(self, submit_delayed_request):
        _req_id = submit_delayed_request
        assert (RequestStatusValues.SCHEDULED_FOR_EXECUTION, ) == Request.session().query(Request.status).filter(Request.id == _req_id).one()


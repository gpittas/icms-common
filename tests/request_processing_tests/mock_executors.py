from icms_orm.common import Request, Institute
from sqlalchemy import and_

from icmsutils.businesslogic.requests import BaseRequestExecutor
from icms_orm.common import Affiliation
from datetime import date


class LazyRequestExecutor(BaseRequestExecutor):
    pass


class IneptRequestExecutor(BaseRequestExecutor):
    def execute(self):
        print('Division by zero in 3..2..1..%d' % (1 / 0))


class InstituteCreatingRequestExecutor(BaseRequestExecutor):
    def __init__(self, **kwargs):
        super(InstituteCreatingRequestExecutor, self).__init__(**kwargs)
        self.code = kwargs.get('code')
        self.name = kwargs.get('name')

    def execute(self):
        _ssn = Request.session()
        new_inst = Institute.from_ia_dict({Institute.code: self.code, Institute.name: self.name})
        _ssn.add(new_inst)
        _ssn.commit()


class TransferRequestExecutor(BaseRequestExecutor):

    def __init__(self, **kwargs):
        super(TransferRequestExecutor, self).__init__(**kwargs)

    @property
    def cms_id(self):
        return self._args['cms_id']

    @property
    def inst_code_from(self):
        return self._args['inst_code_from']

    @property
    def inst_code_to(self):
        return self._args['inst_code_to']

    @property
    def effective_date(self):
        return self._args.get('effective_date', None)

    def execute(self):
        _ssn = Affiliation.session()
        super(TransferRequestExecutor, self).execute()
        # closing the previous affiliation
        prev_aff = _ssn.query(Affiliation).filter(and_(Affiliation.cms_id == self.cms_id, Affiliation.end_date == None,
                                                       Affiliation.inst_code == self.inst_code_from)).one()
        prev_aff.set(Affiliation.end_date, date.today())
        _ssn.add(prev_aff)
        # opening the new one
        new_aff = Affiliation.from_ia_dict({
            Affiliation.cms_id: self.cms_id,
            Affiliation.inst_code: self.inst_code_to,
            Affiliation.is_primary: prev_aff.get(Affiliation.is_primary),
            Affiliation.start_date: prev_aff.get(Affiliation.end_date),
        })
        _ssn.add(new_aff)

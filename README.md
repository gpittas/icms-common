[![pipeline status](https://gitlab.cern.ch/cms-icmsweb/icms-common/badges/master/pipeline.svg)](https://gitlab.cern.ch/cms-icmsweb/icms-common/-/commits/master)
[![coverage report](https://gitlab.cern.ch/cms-icmsweb/icms-common/badges/master/coverage.svg)](https://gitlab.cern.ch/cms-icmsweb/icms-common/-/commits/master)

  
# icms-common

The project contains pieces of code that would otherwise have to be copy-pasted across the new iCMS software, such as library methods or certain iCMS-specific magic numbers. 
On top of that there are scripts running directly against the iCMS databases, such as: authorship checks (TBD), CADI notifications (TBD), cross-db syncs (TBD) etc.

## Usage

### Creating new scripts
Just so that the config is read before anything config-dependent might get loaded, an import will needed first, like:
```python
from scripts.prescript_setup import db, config
```
Sorry for the inconvenience. By the way, the `prescript_setup` internally depends on the `ConfigGovernor` 
implementation registered at the moment. For scripts within this project it's not very relevant as the default
implementation does what's necessary. Things change, when this project serves as a library to another one.

### Incorporating in client projects

Client code needs to provide some means of communicating its config to the icms-common parts.
Registering a `ConfigGovernor` implementation establishes the necessary bridge, like in the example below.
Again, this needs to precede any imports of objects that might depend on config values.

```python
import flask
from util import const
from icmscommon import ConfigGovernor

class WebappConfigGovernor(ConfigGovernor):

    def __init__(self, app: flask.Flask):
        self._app = app
        self._config = app.config

    def _check_test_mode(self) -> bool:
        return self._config.get(const.OPT_NAME_TESTING)

    def _get_db_owner_pass(self):
        pass

    def _get_db_owner_user(self):
        pass

    def _get_epr_db_string(self):
        return self._get_db_string('epr')

    def _get_db_string(self, db: str) -> str:
        _binds = self._config.get(const.OPT_NAME_BINDS)
        if 'epr' in db.lower():
            return _binds.get(const.OPT_NAME_BIND_EPR)
        elif 'common' in db.lower():
            return _binds.get(const.OPT_NAME_BIND_COMMON)
        elif 'legacy' in db.lower():
            return _binds.get(const.OPT_NAME_BIND_LEGACY)
        elif 'toolkit' in db.lower():
            return _binds.get(const.OPT_NAME_BIND_TOOLKIT)
        raise ValueError('Failed to determine the DB string for %s DB' % db)
        
        
def main(web_app):
    ConfigGovernor.set_instance(WebappConfigGovernor(web_app))
    pass
```


## Configuration
### Databases
Ideally, both MySQL and Postgres should be used for testing (although there are some known issues with such setup and SQLAlchemy).

#### Postgres
##### DB creation
```sql
CREATE ROLE serve NOSUPERUSER CREATEDB CREATEROLE LOGIN ENCRYPTED PASSWORD 'protect';

CREATE DATABASE icms_test owner serve;

CREATE ROLE toolkit_test login encrypted password 'icms_test' ROLE serve;
CREATE ROLE epr_test login encrypted password 'icms_test' ROLE serve;
CREATE ROLE icms_test login encrypted password 'icms_test' ROLE serve;
CREATE ROLE notes_test login encrypted password 'icms_test' ROLE serve;

--! Important: set icms_test_reader so that all the garbage created by db.create_all can be removed regardless of which psql bind is active at the moment
CREATE SCHEMA toolkit AUTHORIZATION icms_test_reader;
CREATE SCHEMA epr AUTHORIZATION icms_test_reader;
CREATE SCHEMA notes AUTHORIZATION icms_test_reader;

alter role toolkit_test set search_path = toolkit, public;
alter role epr_test set search_path = epr, public;
alter role notes_test set search_path = notes, public;

CREATE ROLE icms_test_reader;
GRANT CONNECT ON DATABASE icms_test to icms_test_reader;
GRANT USAGE ON SCHEMA epr to icms_test_reader;
GRANT USAGE ON SCHEMA toolkit to icms_test_reader;
GRANT USAGE ON SCHEMA notes to icms_test_reader;
GRANT USAGE ON SCHEMA public to icms_test_reader;
GRANT SELECT ON ALL TABLES IN SCHEMA epr to icms_test_reader;
GRANT SELECT ON ALL TABLES IN SCHEMA toolkit to icms_test_reader;
GRANT SELECT ON ALL TABLES IN SCHEMA notes to icms_test_reader;
GRANT SELECT, INSERT, UPDATE, REFERENCES ON ALL TABLES IN SCHEMA public to icms_test_reader;
GRANT SELECT, USAGE ON ALL SEQUENCES IN SCHEMA public to icms_test_reader;
GRANT icms_test_reader to epr_test, toolkit_test, icms_test, notes_test;
```

##### Getting a fresh re-start

```sql
-- the following is needed for the tests to execute (SQLAlchemy creates mysql enums in psql schemas
-- and then there are access violation issues whenever some other role tries to drop them)
drop schema epr cascade;
drop schema public cascade;
drop schema toolkit cascade;

create schema epr authorization icms_test_reader;
create schema public authorization icms_test_reader;
create schema toolkit authorization icms_test_reader;

GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO public;
COMMENT ON SCHEMA public IS 'standard public schema';
```

## Testing

Pytest is needed for running tests and it's best to have a separate `config_test_override.ini` file in project's directory to setup things like DB connections and whatever else from `config.ini` that might need overwriting.

`pytest.ini` file stores some pytest-specific settings, like which tests to run etc.

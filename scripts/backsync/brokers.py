class BasicBacksyncBroker(object):
    def relay(self, query):
        # todo: use a query and store the previously seen db hash to prevent dummy runs:
        # select md5(string_agg(cast((mo.*) as text), '-')) from mo;
        return query.all()

from scripts.prescript_setup import db
from scripts.backsync.agents.backsync_agents_common import BaseBacksyncAgent
from icms_orm.cmspeople import MoData as MoOld
from icms_orm.common import MO as MoNew, MoStatusValues as MoStatusNew, FundingAgency


class MoBacksyncAgent(BaseBacksyncAgent):
    def __init__(self, backsync_broker, dry_run=False):
        super().__init__(backsync_broker, dry_run)
        self._fas = {_r[0]: _r[1] for _r in db.session.query(FundingAgency.id, FundingAgency.name).all()}

    def sync(self):
        sources_q = db.session.query(MoNew)
        sources = self._broker.relay(sources_q)

        # this one will be used to copy over the values of a "fresh start" for each person
        reference = MoOld.from_ia_dict({})

        targets = {_m.get(MoOld.cmsId): _m for _m in db.session.query(MoOld).all()}
        # reset all the columns to defaults
        for _key, _value in targets.items():
            for ia in MoOld.ia_list():
                if ia.key != MoOld.cmsId.key:
                    _value.set(ia, reference.get(ia))
            db.session.add(_value)

        # now go over all the sources from the new database and copy the info accordingly

        _src: MoNew
        for _src in sources:
            self.set_fields_accordingly(targets.get(_src.cms_id), _src)

        if self._dry_run is True:
            db.session.rollback()
        else:
            db.session.commit()

    def get_fa_name(self, fa_id):
        return self._fas.get(fa_id)

    def set_fields_accordingly(self, old_mo: MoOld, new_mo: MoNew):
        YES = 'YES'
        NO = 'NO'

        (mo_col, phd_mo_col, free_mo_col, phd_ic_col, phd_fa_col) = MoOld.get_columns_for_year(new_mo.year)

        # initialize the values as if no MO was to be assigned
        mo_val, phd_mo_val, free_mo_val, phd_ic_val, phd_fa_val = (NO, NO, NO, None, None)

        if new_mo.status == MoStatusNew.PROPOSED:
            mo_val = YES
        elif new_mo.status == MoStatusNew.APPROVED:
            mo_val = YES
            phd_mo_val = YES
            phd_fa_val = self.get_fa_name(new_mo.fa_id)
            phd_ic_val = new_mo.inst_code
        elif new_mo.status == MoStatusNew.REJECTED or new_mo.status == MoStatusNew.CANCELLED:
            # no depth in the old DB to express that - simply no MO then
            pass
        elif new_mo.status == MoStatusNew.APPROVED_LATE:
            # sync back as free mo (cannot interfere with PhD mo, pure MO is wishful-only - no other option left)
            mo_val = YES
            free_mo_val = YES
            # these will be synced back although in the old system there is no official representation of that state
            phd_fa_val = self.get_fa_name(new_mo.fa_id)
            phd_ic_val = new_mo.inst_code
        elif new_mo.status == MoStatusNew.APPROVED_FREE_GENERIC:
            mo_val = YES
            free_mo_val = YES
        elif new_mo.status == MoStatusNew.APPROVED_FREE_LATECOMER:
            mo_val = YES
            free_mo_val = YES
        elif new_mo.status == MoStatusNew.APPROVED_FREE_NEW_INST:
            mo_val = YES
            free_mo_val = YES
        elif new_mo.status == MoStatusNew.APPROVED_FREE_EXTENDED_RIGHTS:
            mo_val = YES
            free_mo_val = YES
        elif new_mo.status == MoStatusNew.APPROVED_SWAPPED_IN:
            mo_val = YES
            free_mo_val = YES
        elif new_mo.status == MoStatusNew.SWAPPED_OUT:
            mo_val = NO
            phd_mo_val = YES
            phd_fa_val = self.get_fa_name(new_mo.fa_id)
            phd_ic_val = new_mo.inst_code

        old_mo.set(mo_col, mo_val)
        old_mo.set(phd_mo_col, phd_mo_val)
        old_mo.set(free_mo_col, free_mo_val)
        old_mo.set(phd_ic_col, phd_ic_val)
        old_mo.set(phd_fa_col, phd_fa_val)


#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from scripts.prescript_setup import db, ConfigProxy
from icms_orm.common import EmailMessage
from icms_orm.cmspeople import Person, PersonHistory, User
from icms_orm.toolkit import AuthorshipApplicationCheck as AAC
from email_templates import EmailTemplateAutoSuspension
import sqlalchemy as sa
import logging
from icmsutils.prehistory.prehistory_writer import PersonAndPrehistoryWriter as Writer


class FailedApplicantsSuspender(object):

    def __init__(self):
        (self._last_run_number,) = db.session.query(sa.func.max(AAC.runNumber).label('last_run')).one()
        self._failed_checks = {aac.get(AAC.cmsId): aac for aac in db.session.query(AAC).
            filter(AAC.runNumber == self._last_run_number).filter(AAC.failed == True).all()}
        self._failed_cms_ids = {aac.get(AAC.cmsId) for aac in self._failed_checks.values()}
        self._people = {p.get(Person.cmsId): p for p, u in db.session.query(Person, User).join(PersonHistory,
                Person.cmsId == PersonHistory.cmsId).join(User, Person.cmsId == User.cmsId).
                filter(Person.cmsId.in_(self._failed_cms_ids)).all()}

    def execute(self):
        logging.info('The last run number was %d' % self._last_run_number)
        logging.info('Found %d failing applicants' % len(self._failed_cms_ids))
        dist = [0] * 20
        for aac in self._failed_checks.values():
            dist[int(aac.get(AAC.workedSelf))] += 1
            logging.debug('%d has accumulated %.2f of own EPR contributions over the %d days of their application.'
                         % tuple([aac.get(a) for a in [AAC.cmsId, AAC.workedSelf, AAC.daysCount]]))
            if aac.get(AAC.workedSelf) == 0:
                logging.debug('%d surely should be suspended' % aac.get(AAC.cmsId))
                self.suspend(aac.get(AAC.cmsId))
            elif aac.get(AAC.workedSelf) >= 5:
                logging.debug('%d has accumulated a lot of work but the deadline has passed... ' % aac.get(AAC.cmsId))
            elif aac.get(AAC.workedSelf) >= 3:
                logging.debug('%d has accumulated some work but the deadline has passed... ' % aac.get(AAC.cmsId))
            elif aac.get(AAC.workedSelf) > 0:
                logging.debug('%d has accumulated very little work and the deadline has passed... ' % aac.get(AAC.cmsId))
        logging.info('Contrib-dist: %s' % ', '.join('up to %d - %d' % (i, v) for i, v in enumerate(dist)))

    def suspend(self, cms_id):
        person = self._people.get(cms_id)
        aac = self._failed_checks.get(cms_id)
        assert isinstance(person, Person)
        if ConfigProxy.get_suspend_timed_out_applicants():
            if person.isAuthorSuspended is True:
                logging.warn('%d is already suspended! Aborting.' % cms_id)
                return
            actor = Person.from_ia_dict({Person.cmsId: 0})
            writer = Writer(db_session=db.session, subject_person=person, actor_person=actor)
            writer.set_new_value(Person.isAuthorSuspended, True)
            writer.apply_changes(do_commit=False)
            if person.status.startswith('CMS'):
                email = EmailTemplateAutoSuspension(recipient_name='%s %s' % (person.firstName, person.lastName),
                                                    recipient_email=person.user.get_email(),
                                                    app_duration=aac.get(AAC.daysCount),
                                                    work_done=aac.get(AAC.workedSelf),
                                                    sender_email=ConfigProxy.get_email_sender()
                                                    )
                email.generate_message(store_in_db=True)
            logging.info('%d just got auto-suspended. Days as applicant: %d, EPR delivered: %2.f.' %
                         (cms_id, aac.get(AAC.daysCount), aac.get(AAC.workedSelf)))
        else:
            logging.info('Suspensions disabled! %d avoided suspension after %d days as applicant and %.2f delivered work' %
                         (cms_id, aac.get(AAC.daysCount), aac.get(AAC.workedSelf)))

    @staticmethod
    def main():
        suspender = FailedApplicantsSuspender()
        suspender.execute()


if __name__ == '__main__':
    FailedApplicantsSuspender.main()

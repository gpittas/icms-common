#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from scripts.prescript_setup import db, ConfigProxy
from icms_orm.cmsanalysis import PaperAuthorHistory as PAH, PaperAuthorHistoryActions as PAHActions
from icms_orm.cmspeople import Person
from icmsutils.businesslogic.cadi.authorlists import mark_in_db_as_having_al_files_generated
from helpers import get_script_actor_person
import logging
import re
import traceback
from icmsutils.remotefileutils import SshFileTools


def main():
    path = ConfigProxy.get_active_papers_properties_remote_path()
    host = ConfigProxy.get_active_papers_properties_remote_host()
    if path and host:
        main_with_remote_file(host=host, path=path)
    else:
        main_with_local_file()


def main_with_local_file():
    logging.info('Path to activepapers: %s' % ConfigProxy.get_active_papers_properties_path())
    app_info = ActivePapersParser(path_to_file=ConfigProxy.get_active_papers_properties_path())

    closed_in_db = {x[0] for x in db.session.query(PAH.code).filter(PAH.action.ilike('%%%s%%' % PAHActions.LIST_CLOSE)).all()}
    logging.debug('Found %d papers already appearing in the DB as closed.' % len(closed_in_db))
    closed_in_file = app_info.get_closed()
    logging.debug('Found %d papers already appearing in the file as closed.' % len(closed_in_file))

    actor_person = get_script_actor_person()

    for code in closed_in_file:
        if code in closed_in_db:
            logging.debug('Analysis %s already marked as having the final AL generated' % code)
            continue
        logging.info('Marking %s as ~closed, in the DB.' % code)
        mark_in_db_as_having_al_files_generated(code, actor_person, defer_commit=True)
        db.session.commit()


def main_with_remote_file(host, path):
    logging.info('Accessing remote file for a cross-sync of CLOSED papers (open will be overwritten on either side).')
    raw_lines = SshFileTools.read_file(host, path)
    parser = ActivePapersRemoteParser(host=host, path=path)

    closed_in_db = {x[0] for x in db.session.query(PAH.code).filter(PAH.action.ilike('%%%s%%' % PAHActions.LIST_CLOSE)).all()}
    logging.debug('Found %d papers already appearing in the DB as closed.' % len(closed_in_db))
    closed_in_file = parser.get_closed()
    logging.debug('Found %d papers already appearing in the file as closed.' % len(closed_in_file))

    actor_person = get_script_actor_person()
    for code in closed_in_file:
        if code in closed_in_db:
            logging.debug('Analysis %s already marked as having the final AL generated.' % code)
            continue
        logging.info('Marking %s as ~closed, in the DB.' % code)
        mark_in_db_as_having_al_files_generated(code, actor_person, defer_commit=True)
        db.session.commit()

    for code in closed_in_db:
        if parser.is_open(code):
            logging.info('AL for %s appears as open in the properties file.' % code)
            parser.close_paper(code)
            logging.info('AL for %s has been closed in the remote file.' % code)


class ActivePapersParser(object):

    _line_pattern = r'^(\w{3}-\d{2}-\d{3})=(open|closed)$'

    def __init__(self, path_to_file):
        self._path = path_to_file
        self._raw_lines = []
        self._papers_map = {}
        with open(self._path, 'r') as f:
            for line in f:
                self._raw_lines.append(line)
        self._parse_raw_lines()

    def _parse_raw_lines(self):
        for line in self._raw_lines:
            m = re.match(ActivePapersParser._line_pattern, line.strip())
            if m:
                self._papers_map[m.group(1)] = m.group(2)

    def is_open(self, code):
        return self._papers_map.get(code, '').lower() == 'open'

    def is_closed(self, code):
        return self._papers_map.get(code, '').lower() == 'closed'

    def get_closed(self):
        return sorted([code for code, value in self._papers_map.items() if value.lower() == 'closed'])

    def get_open(self):
        return sorted([code for code, value in self._papers_map.items() if value.lower() == 'open'])

    def open_paper(self, code):
        pass

    def close_paper(self, code):
        pass

    def remove_paper(self, code):
        pass


class ActivePapersRemoteParser(ActivePapersParser):
    def __init__(self, host, path):
        self._host = host
        self._path = path
        self._papers_map = {}
        self._raw_lines = SshFileTools.read_file(host, path).decode('utf-8')
        if self._raw_lines:
            self._raw_lines = self._raw_lines.split('\n')
        self._parse_raw_lines()

    def close_paper(self, code):
        needle = '%s=open' % code
        replacement = '%s=closed' % code
        SshFileTools.replace_in_file(host=self._host, path=self._path, needle=needle, replacement=replacement)

    def open_paper(self, code):
        needle = '%s=closed' % code
        replacement = '%s=open' % code
        SshFileTools.replace_in_file(host=self._host, path=self._path, needle=needle, replacement=replacement)


if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        logging.error(traceback.format_exc())

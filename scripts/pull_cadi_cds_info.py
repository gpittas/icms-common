#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from scripts.prescript_setup import db, config
from icmsutils.businesslogic.cadi import get_cds_papers
from icms_orm.cmsanalysis import CadiAnalysis
from icms_orm.common import ApplicationAsset
import logging


def main():
    try:
        # get the CDS data: record_id -> AN CODE
        cds_data = get_cds_papers()
        # store that as JSON in "variables" table
        ApplicationAsset.store(name='cadi_cds_records', value=cds_data, application='common')

    except Exception as e:
        logging.exception('Failed to retrieve CDS data!')


if __name__ == '__main__':
    main()

#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

# Setup needs to be imported first. It configures icms_orm (bind names, schema names, db strings) and sets up sys.path
from scripts.prescript_setup import db, config
from icms_orm.cmspeople import Person, User
from icmsutils.ldaputils import LdapProxy, LdapPerson
import logging
import sqlalchemy as sa
from icmsutils.prehistory import PersonAndPrehistoryWriter as Writer


def main(per_page=60):
    update_empty_logins(per_page=per_page)
    update_cern_email(per_page=per_page)


def update_empty_logins(per_page):
    q = db.session.query(Person).filter(sa.or_(Person.loginId == None, Person.loginId == '')).\
        filter(Person.status != 'EXMEMBER').paginate(per_page=per_page)
    ldap = LdapProxy.get_instance()
    total_updated = 0
    while q.items:
        no_logins = q.items
        logging.debug('Found %d people with no valid login' % len(no_logins))
        cms_people = {p.hrId: p for p in no_logins}
        ldap_results = LdapProxy.find_people_by_hrids(ldap, [x.hrId for x in no_logins])
        logging.debug('Found %d LDAP people...' % len(ldap_results))
        ldap_people = {p.hrId: p for p in ldap_results}
        cured = 0
        for hrId, entry in ldap_people.items():
            if entry.login:
                person = cms_people.get(hrId)
                person.loginId = entry.login
                db.session.add(person)
                cured += 1
                logging.info('%s %s will now have the login %s assigned' % (person.firstName, person.lastName, person.loginId))
        logging.debug('Updated logins for %d/%d people in current batch' % (cured, len(cms_people)))
        total_updated += cured
        q = q.next()
    db.session.commit()
    logging.info('Updated %d out of %d qualified entries' % (total_updated, q.total))


def update_cern_email(per_page):
    updater_person = db.session.query(Person).filter(Person.cmsId == config['scripts']['updater_cms_id']).one()
    q = db.session.query(Person, User).join(User, Person.cmsId == User.cmsId).filter(
        User.mailWhere.notilike('%inst%')).filter(Person.status.notilike('%EXMEMBER%')).paginate(per_page=per_page)
    ldap = LdapProxy.get_instance()
    total_updated = 0
    while q.items:
        hr_ids = {p.hrId for p, u in q.items}
        logging.debug(hr_ids)
        ldap_people = {p.hrId: p for p in LdapProxy.find_people_by_hrids(ldap, hrids=hr_ids)}
        for person, user in q.items:
            if person.hrId not in ldap_people:
                logging.warn('HR ID %d of %s %s not found in LDAP!' % (person.hrId, person.firstName, person.lastName))
            else:
                ldap_email = ldap_people[person.hrId].mail
                if (getattr(user, User.emailCern.key) or 'None').lower() != ldap_people[person.hrId].mail.lower():
                    logging.info('Updating CERN email for %s %s from %s to %s.' % (person.firstName, person.lastName, user.emailCern, ldap_email))
                    total_updated += 1
                    writer = Writer(db_session=db.session, subject_person=person, actor_person=updater_person, user=user)
                    writer.set_new_value(User.emailCern, ldap_email)
                    writer.apply_changes(do_commit=False)
        db.session.commit()
        q = q.next()
    logging.info('Updated %d emails.' % total_updated)


if __name__ == '__main__':
    main(per_page=60)

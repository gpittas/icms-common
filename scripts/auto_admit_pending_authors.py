"""
This script's role is to auto-admit any pending author that hasn't been decided about.
The default waiting time is 14 days so once the "authors ready" email reaches that age,
the candidate will be picked up by this script and set as author-worthy* automatically.

* all blocks lifted, but the acual signing rights will still depend on things like M&O for example
"""

#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from scripts.prescript_setup import db, ConfigProxy
import logging
from datetime import datetime, date, timedelta as dt
from icms_orm.common import EmailMessage as Email, EmailLog
from icms_orm.toolkit import AuthorshipApplicationCheck as Check
from icms_orm.cmspeople import Person
from icmsutils.businesslogic.authorship import suspend_epr
from icmsutils.businesslogic.authorship import cbi_set_author_yes

LOOKBACK_INTERVAL = 60
DECISION_TIMEOUT = 14


def main():
    ssn = db.session
    admin_cms_id = ConfigProxy.get_updater_cms_id_for_scripts()
    admin = ssn.query(Person).filter(Person.cmsId == admin_cms_id).one()
    # Query for Checks that have been followed by an email notification (positive or negative)
    rows = ssn.query(Check, Email, EmailLog).join(Email, Check.notificationId == Email.id). \
        join(EmailLog, EmailLog.email_id == Email.id).filter(EmailLog.status == EmailLog.Status.SENT). \
        filter(EmailLog.timestamp > date.today() - dt(days=LOOKBACK_INTERVAL)). \
        filter(EmailLog.timestamp < date.today() - dt(days=DECISION_TIMEOUT)).all()
    logging.info('Found %d notifications sent between %d and %d days ago' % (len(rows), DECISION_TIMEOUT, LOOKBACK_INTERVAL))

    for check, email, email_log in rows:
        logging.debug('For CMSid %d the email was sent on %s [check took place on %s]' % (check.cmsId, email_log.timestamp, check.datetime))

    passing_cms_ids = [r[0].cmsId for r in rows if r[0].get(Check.passed) == True]
    blocked_cms_ids = [r[0] for r in ssn.query(Person.cmsId).filter(Person.isAuthorBlock == True).all()]

    logging.info('%d cases have not been addressed yet' % len(blocked_cms_ids))

    # Admission of new authors (unblocking)
    for cms_id in passing_cms_ids:
        if cms_id in blocked_cms_ids:
            author = ssn.query(Person).filter(Person.cmsId == cms_id).one()
            cbi_set_author_yes(db_session=ssn, author_to_be=author, cbi=admin)
            logging.info('Setting %s %s [CMS ID %d] as ready for authorship' % (author.firstName, author.lastName, cms_id))


if __name__ == '__main__':
    main()

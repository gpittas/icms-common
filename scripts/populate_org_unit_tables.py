#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import sys
from scripts.prescript_setup import db as db, config
from datetime import date
from icms_orm.common import PersonStatus, OrgUnit, OrgUnitType, OrgUnitTypeName, PositionName, Position, Tenure
from typing import Dict, Tuple
from datetime import date

"""
Methods in this file started out as pytest fixtures, but since they are based on real data, they were moved here in order
to make them callable also outside the testing environment and thus allow speedy population of prod DB.
"""


class DatumWrap(object):
    """
    Wrapper for the input data describing someone's involvement, like:
    X was a chairman of Y commitee between A and B
    """
    _units_data = None
    _unit_types_data = None
    _positions_data = None

    def __init__(self, cms_id, unit_domain, unit_type_name, position_name, appointed_on=date(2017, 9, 1), steps_down_on=None):
        self.cms_id = cms_id
        self.unit_domain = unit_domain
        self.unit_type_name = unit_type_name
        self.position_name = position_name
        self.appointed_on = appointed_on
        self.steps_down_on = steps_down_on


    @classmethod
    def get_units_data(cls):
        if cls._units_data is None:
            # Org Units by (OrgUnit.domain, OrgUnit.OrgUnitType.name)
            cls._units_data = {(r[0].domain, r[1]): r[0] for r in db.session.query(OrgUnit, OrgUnitType.name).join(OrgUnitType, OrgUnit.type_id == OrgUnitType.id).all()}
        return cls._units_data

    @classmethod
    def get_unit_types_data(cls):
        if cls._unit_types_data is None:
            cls._unit_types_data = {None: x for x in db.session.query(OrgUnitType).all()}
        return cls._unit_types_data

    @classmethod
    def get_positions_data(cls):
        if cls._positions_data is None:
            # Positions by (Position.name, OrgUnitType.name) tuple
            cls._positions_data = {(r[0].get(Position.name), r[1]): r[0] for r in db.session.query(Position, OrgUnitType.name).join(OrgUnitType, Position.unit_type_id == OrgUnitType.id).all()}
        return cls._positions_data

    def get_unit_type_id(self):
        pass

    def get_unit_id(self):
        unit = self.get_units_data().get((self.unit_domain, self.unit_type_name))
        return unit.id if unit else None

    def get_position_id(self):
        position = self.get_positions_data().get((self.position_name, self.unit_type_name))
        return position.id if position else None

    def create_tenure(self):
        position_id = self.get_position_id()
        unit_id = self.get_unit_id()

        assert position_id is not None
        assert unit_id is not None

        return Tenure.from_ia_dict({
            Tenure.cms_id: self.cms_id,
            Tenure.start_date: self.appointed_on,
            Tenure.position_id: position_id,
            Tenure.unit_id: unit_id
        })

class Data(object):
    class UnitDomain(object):
        COLLABORATION = 'Collaboration'
        PUBLICATIONS = 'Publications'
        COMMUNICATION = 'Communication'
        # ----
        BRIL = 'BRIL'
        TRACKER = 'TRACKER'
        ECAL = 'ECAL'
        HCAL = 'HCAL'
        MUDT = 'MUDT'
        MUCSC = 'MUCSC'
        MURPC = 'MURPC'
        MTD = 'MTD'
        PPS = 'PPS'
        DAQ = 'DAQ'

    _data = [
        (510, UnitDomain.COLLABORATION, OrgUnitTypeName.BOARD, PositionName.CHAIRPERSON),
        (4846, UnitDomain.COLLABORATION, OrgUnitTypeName.BOARD, PositionName.DEPUTY),
        (1342, UnitDomain.COLLABORATION, OrgUnitTypeName.BOARD, PositionName.SECRETARY),
        (5220, UnitDomain.PUBLICATIONS, OrgUnitTypeName.COMMITTEE, PositionName.CHAIRPERSON),
        (3580, UnitDomain.COMMUNICATION, OrgUnitTypeName.COMMITTEE, PositionName.CHAIRPERSON),
        (8546, UnitDomain.BRIL, OrgUnitTypeName.SUBDETECTOR, PositionName.MANAGER),
        (774, UnitDomain.BRIL, OrgUnitTypeName.SUBDETECTOR, PositionName.DEPUTY),
        (1059, UnitDomain.TRACKER, OrgUnitTypeName.SUBDETECTOR, PositionName.MANAGER),
        (2929, UnitDomain.TRACKER, OrgUnitTypeName.SUBDETECTOR, PositionName.DEPUTY),
        (5035, UnitDomain.TRACKER, OrgUnitTypeName.SUBDETECTOR, PositionName.RESOURCE_MANAGER),
        (282, UnitDomain.TRACKER, OrgUnitTypeName.SUBDETECTOR, PositionName.TECHNICAL_COORDINATOR),

        (1280, UnitDomain.ECAL, OrgUnitTypeName.SUBDETECTOR, PositionName.MANAGER),
        (4345, UnitDomain.ECAL, OrgUnitTypeName.SUBDETECTOR, PositionName.DEPUTY),
        (5079, UnitDomain.HCAL, OrgUnitTypeName.SUBDETECTOR, PositionName.MANAGER),
        (7789, UnitDomain.HCAL, OrgUnitTypeName.SUBDETECTOR, PositionName.DEPUTY),
        (247, UnitDomain.MUDT, OrgUnitTypeName.SUBDETECTOR, PositionName.MANAGER),
        (632, UnitDomain.MUCSC, OrgUnitTypeName.SUBDETECTOR, PositionName.MANAGER),
        (811, UnitDomain.MUCSC, OrgUnitTypeName.SUBDETECTOR, PositionName.DEPUTY),
        (3771, UnitDomain.MURPC, OrgUnitTypeName.SUBDETECTOR, PositionName.MANAGER),
        (1391, UnitDomain.MURPC, OrgUnitTypeName.SUBDETECTOR, PositionName.DEPUTY)
    ]

    @classmethod
    def get_data(cls):
        return cls._data

    @classmethod
    def get_wraps(cls):
        return [DatumWrap(*_d) for _d in cls._data]


def insert_org_units_as_of_2018():
    ssn = OrgUnit.session()

    type_board = OrgUnitType.from_ia_dict({OrgUnitType.name: OrgUnitTypeName.BOARD, OrgUnitType.level: 1})
    type_committee = OrgUnitType.from_ia_dict({OrgUnitType.name: OrgUnitTypeName.COMMITTEE, OrgUnitType.level: 1})
    type_office = OrgUnitType.from_ia_dict({OrgUnitType.name: OrgUnitTypeName.OFFICE, OrgUnitType.level: 1})
    type_coord = OrgUnitType.from_ia_dict({OrgUnitType.name: OrgUnitTypeName.COORDINATION_AREA, OrgUnitType.level: 2})
    type_detector = OrgUnitType.from_ia_dict({OrgUnitType.name: OrgUnitTypeName.SUBDETECTOR, OrgUnitType.level: 2})


    _unit_types = {x.get(OrgUnitType.name): (ssn.add(x) or x) for x in (type_board, type_committee, type_office, type_coord, type_detector)}

    cb = OrgUnit.from_ia_dict({OrgUnit.type: type_board, OrgUnit.domain: 'Collaboration'})
    ssn.add(cb)
    units = {'Collaboration Board': cb}
    ssn.commit()

    other_units = list()
    for _dict in [
        {OrgUnit.type: type_office, OrgUnit.domain: 'Diversity', OrgUnit.superior_unit_id: cb.id},
        {OrgUnit.type: type_office, OrgUnit.domain: 'Engagement', OrgUnit.superior_unit_id: cb.id},
        {OrgUnit.type: type_office, OrgUnit.domain: 'Resources', OrgUnit.superior_unit_id: cb.id},
        {OrgUnit.type: type_office, OrgUnit.domain: 'Spokesperson', OrgUnit.superior_unit_id: cb.id},

        {OrgUnit.type: type_board, OrgUnit.domain: 'Advisors', OrgUnit.superior_unit_id: cb.id},

        {OrgUnit.type: type_committee, OrgUnit.domain: Data.UnitDomain.PUBLICATIONS, OrgUnit.superior_unit_id: cb.id, },
        {OrgUnit.type: type_committee, OrgUnit.domain: 'Conference', OrgUnit.superior_unit_id: cb.id, },
        {OrgUnit.type: type_committee, OrgUnit.domain: Data.UnitDomain.COMMUNICATION, OrgUnit.superior_unit_id: cb.id, },
        {OrgUnit.type: type_committee, OrgUnit.domain: 'Authorship', OrgUnit.superior_unit_id: cb.id, },
        {OrgUnit.type: type_committee, OrgUnit.domain: 'Thesis Awards', OrgUnit.superior_unit_id: cb.id, },
        {OrgUnit.type: type_committee, OrgUnit.domain: 'Industrial Awards', OrgUnit.superior_unit_id: cb.id, },
        {OrgUnit.type: type_committee, OrgUnit.domain: 'Career Committee', OrgUnit.superior_unit_id: cb.id, },
        {OrgUnit.type: type_committee, OrgUnit.domain: 'International Committee', OrgUnit.superior_unit_id: cb.id, },
        {OrgUnit.type: type_committee, OrgUnit.domain: 'Schools Committee', OrgUnit.superior_unit_id: cb.id, },
        {OrgUnit.type: type_committee, OrgUnit.domain: 'Search Committee', OrgUnit.superior_unit_id: cb.id, },

        {OrgUnit.type: type_coord, OrgUnit.domain: 'Physics'},
        {OrgUnit.type: type_coord, OrgUnit.domain: 'Physics Performance & Datasets'},
        {OrgUnit.type: type_coord, OrgUnit.domain: 'Trigger - HLT'},
        {OrgUnit.type: type_coord, OrgUnit.domain: 'Run'},
        {OrgUnit.type: type_coord, OrgUnit.domain: 'Upgrade'},

        {OrgUnit.type: type_detector, OrgUnit.domain: 'L1 Trigger'},
        {OrgUnit.type: type_detector, OrgUnit.domain: Data.UnitDomain.TRACKER},
        {OrgUnit.type: type_detector, OrgUnit.domain: Data.UnitDomain.BRIL},
        {OrgUnit.type: type_detector, OrgUnit.domain: Data.UnitDomain.PPS},
        {OrgUnit.type: type_detector, OrgUnit.domain: Data.UnitDomain.DAQ},
        {OrgUnit.type: type_detector, OrgUnit.domain: Data.UnitDomain.ECAL},
        {OrgUnit.type: type_detector, OrgUnit.domain: Data.UnitDomain.MURPC},
        {OrgUnit.type: type_detector, OrgUnit.domain: Data.UnitDomain.MUCSC},
        {OrgUnit.type: type_detector, OrgUnit.domain: Data.UnitDomain.MUDT},
        {OrgUnit.type: type_detector, OrgUnit.domain: Data.UnitDomain.HCAL},
        {OrgUnit.type: type_detector, OrgUnit.domain: Data.UnitDomain.MTD},
    ]:
        _u = OrgUnit.from_ia_dict(_dict)
        ssn.add(_u)
        units['%s %s' % (_u.get(OrgUnit.domain), _u.get(OrgUnit.type).get(OrgUnitType.name))] = _u

    ssn.commit()
    return units, _unit_types


def insert_positions(unit_types: Dict[str, OrgUnitType]) -> Dict[Tuple[str, str], Position]:
    assert isinstance(unit_types, dict)
    positions = dict()
    _data = [
        (PositionName.MANAGER, 1, OrgUnitTypeName.SUBDETECTOR),
        (PositionName.DEPUTY, 2, OrgUnitTypeName.SUBDETECTOR),
        (PositionName.TECHNICAL_COORDINATOR, 3, OrgUnitTypeName.SUBDETECTOR),
        (PositionName.RESOURCE_MANAGER, 3, OrgUnitTypeName.SUBDETECTOR),
        (PositionName.CHAIRPERSON, 1, OrgUnitTypeName.BOARD),
        (PositionName.DEPUTY, 2, OrgUnitTypeName.BOARD),
        (PositionName.SECRETARY, 2, OrgUnitTypeName.BOARD),
        (PositionName.CHAIRPERSON, 1, OrgUnitTypeName.COMMITTEE),
        # (PositionName.CONVENER, 3, None),
        # (PositionName.COORDINATOR, 1, None),
        # (PositionName.SPOKESPERSON, 1, None),
    ]
    for pos_name, pos_level, unit_type_name in _data:
        positions[(pos_name, unit_type_name)] = Position.from_ia_dict({Position.name: pos_name, Position.level: pos_level,
                                               Position.unit_type_id: unit_types.get(unit_type_name).get(OrgUnitType.id)})
        Position.session().add(positions[(pos_name, unit_type_name)])
    Position.session().commit()
    return positions


def insert_2018_tenures(units, positions):
    ssn = Tenure.session()
    for wrap in Data.get_wraps():
        db.session.add(wrap.create_tenure())
    ssn.commit()


def main():
    ssn = Tenure.session()
    if ssn.query(Tenure).count() == 0:
        units, unit_types = insert_org_units_as_of_2018()
        insert_2018_tenures(units, insert_positions(unit_types))
    else:
        raise Exception('Tenures table is not empty. Aborting! Try passing "purge" as argument to remove existing entries.')


def purge():
    ssn = Tenure.session()
    for table in [Tenure, OrgUnitType, OrgUnit, Position]:
        for _thing in ssn.query(table).all():
            ssn.delete(_thing)
    ssn.commit()




if __name__ == '__main__':
    if 'purge' in sys.argv:
        purge()
    else:
        main()

import traceback
import logging
import abc
import inspect
from scripts.prescript_setup import db, ConfigProxy
from icms_orm.common import EmailMessage


def run_watchdogs():
    _run_subclasses(WatchdogBase)


def _run_subclasses(cls):
    _class_names = set(ConfigProxy.get_watchdog_classes_to_run())
    for sub_cls in cls.__subclasses__():
        logging.debug('Considering Watchdog class %s', sub_cls.__name__)
        _run_subclasses(sub_cls)
        if sub_cls.__name__ not in _class_names:
            continue
        if inspect.isabstract(sub_cls):
            logging.debug('Class %s is abstract, skipping instantiation', sub_cls.__name__)
            continue
        try:
            instance = sub_cls()
            instance.check()
            if ConfigProxy.get_watchdog_report_to():
                instance.email_issues()
            else:
                instance.log_issues()
            if ConfigProxy.get_watchdog_auto_fix_issues().lower() == 'yes':
                if isinstance(instance, AutonomousWatchdogBase):
                    instance.remedy()

        except Exception as e:
            logging.error('Failed to run the checks for %s [reason: %s]', sub_cls.__name__, e)
            logging.error(traceback.format_exc())


class WatchdogBase(object):
    def __init__(self):
        self._issues = []

    def check(self):
        pass

    def email_issues(self):
        if self._issues:
            body = '\n'.join(self._issues)
            EmailMessage.compose_message(sender=ConfigProxy.get_email_sender(), to=ConfigProxy.get_watchdog_report_to(),
                                         subject='iCMS WatchDog Report [%s]' % self.__class__.__name__, body=body, cc=None, bcc=None,
                                         source_app=None, reply_to=None, remarks=None, db_session=db.session)

    def add_issue(self, issue: str):
        self._issues.append(issue)

    def log_issues(self):
        if self._issues:
            logging.warning('{0} found the following {1} issues:'.format(self.__class__.__name__, len(self._issues)))
            for issue in self._issues:
                logging.warning(issue)
        else:
            logging.info('%s found no issues' % self.__class__.__name__)

    def has_found_issues(self) -> bool:
        return len(self._issues) > 0


class WatchdogRemedy(abc.ABC):
    @abc.abstractmethod
    def apply(self):
        pass


class AutonomousWatchdogBase(WatchdogBase):
    """
    A watchdog that supports collecting remedy objects that can subsequently be applied to rectify detected issues
    """
    def __init__(self):
        super().__init__()
        self._remedies = []

    def add_remedy(self, remedy_object: WatchdogRemedy):
        self._remedies.append(remedy_object)

    def remedy(self):
        try:
            for remedy in self._remedies:
                remedy.apply()
        except Exception as e:
            logging.error('Failed to apply remedies from watchdog %s [%s]', self, e)
            logging.error(traceback.format_exc())
            db.session.rollback()
        finally:
            db.session.commit()

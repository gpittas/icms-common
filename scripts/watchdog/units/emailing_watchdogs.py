from scripts.watchdog.units.watchdog_base import AutonomousWatchdogBase
from scripts.watchdog.units.watchdog_base import WatchdogRemedy
from icms_orm.common import EmailMessage, EmailLog
import sqlalchemy as sa
import logging


class StuckEmailsWatchdog(AutonomousWatchdogBase):
    def check(self):
        ssn = EmailMessage.session()
        q_cols = [
            EmailMessage.id,
            sa.func.max(EmailLog.id).label('max_failed_id'),
            sa.func.min(EmailLog.id).label('min_failed_id'),
            sa.func.count(EmailLog.id).label('n_failed')
        ]
        q = ssn.query(*q_cols).join(EmailLog, EmailLog.email_id == EmailMessage.id).filter(
            EmailLog.status == EmailLog.Status.FAILED).group_by(EmailMessage.id)
        results = q.from_self().filter(q_cols[-1] > 2).all()
        logging.debug('Found %d rows', len(results))
        for email_id, max_failed_log_id, min_failed_log_id, n_failed in results:
            if n_failed > 2:
                self.add_issue('Email #{id} has {n_failed} failed delivery attempts recorded (min log id: {min_id}, max log id: {max_id})'.format(
                    id=email_id, n_failed=n_failed, min_id=min_failed_log_id, max_id=max_failed_log_id))
                self.add_remedy(StuckEmailStatusCleanupWatchdog(
                    message_id=email_id, last_before_deleted_log_id=min_failed_log_id, first_after_deleted_log_id=max_failed_log_id))
        

class StuckEmailStatusCleanupWatchdog(WatchdogRemedy):
    def __init__(self, message_id, last_before_deleted_log_id, first_after_deleted_log_id):
        self.message_id = message_id
        self.last_before_deleted_log_id = last_before_deleted_log_id
        self.first_after_deleted_log_id = first_after_deleted_log_id

    def apply(self):
        ssn = EmailLog.session()
        ssn.query(EmailLog).filter(sa.and_(
            EmailLog.email_id == self.message_id, 
            EmailLog.id > self.last_before_deleted_log_id, 
            EmailLog.id < self.first_after_deleted_log_id
        )).delete()

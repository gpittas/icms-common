import sqlalchemy as sa
from scripts.prescript_setup import db
from icms_orm.cmspeople import INST_STATUS_YES, INST_STATUS_ASSOCIATED, INST_STATUS_COOPERATING
from icms_orm.cmspeople import MoData, PeopleFlagsAssociation, Institute, Person
from datetime import date
from scripts.watchdog.units.watchdog_base import WatchdogBase


class TeamLeaderFlagsWatchdog(WatchdogBase):
    """
    Looking for instutute leaders needing a flag or incorrectly flagged as:
    CBI: Institute Representative, member of the CB [need to have 3 PHDs this or past year (but to vote they need to
    "have contributed to the obligations for EPR for the running of CMS at the commensurate level")]
    CBD: CBI deputy
    CBO: CB Observer (associated institute representative)
    CBOD: CBO deputy
    INS: Representative of small Institute, not a CB member |
    INSD: INS deputy                                        |
    """

    def __init__(self):
        super(TeamLeaderFlagsWatchdog, self).__init__()
        self._flags_map = dict()
        self._phd_clear_insts = set()
        self._inst_status_map = dict()
        self._de_facto_leaders = set()
        self._de_facto_deputies = set()
        self._lead_insts_map = dict()
        self._co_lead_insts_map = dict()

        yesteryear = date.today().year - 1
        # get the column for PHD Institute Code for the present year and the past one
        ic_col_present, ic_col_past = (getattr(MoData, MoData.phdInstCode2017.key.replace('2017', str(_year)))
                                       for _year in [yesteryear + 1, yesteryear])

        sq_past = db.session.query(ic_col_past.label('inst'),
                                   sa.func.count(MoData.cmsId).label('past_count')).group_by(ic_col_past).subquery()
        sq_present = db.session.query(ic_col_present.label('inst'),
                                      sa.func.count(MoData.cmsId).label('present_count')).group_by(
            ic_col_present).subquery()

        for inst_code, past, present in db.session.query(sq_present.c.inst, sq_present.c.present_count,
                                                         sq_past.c.past_count). \
                join(sq_past, sq_past.c.inst == sq_present.c.inst). \
                filter(sa.or_(sq_past.c.past_count > 2, sq_present.c.present_count > 2)).all():
            self._phd_clear_insts.add(inst_code)

        # store the flags info

        for flag, cms_id in db.session.query(PeopleFlagsAssociation.flagId, PeopleFlagsAssociation.cmsId).filter(
                PeopleFlagsAssociation.flagId.in_(
                    ['CB_cbi', 'CB_cbd', 'CB_cbo', 'CB_cbod', 'CB_ins', 'CB_insd'])).all():
            self._flags_map[flag] = self._flags_map.get(flag, set())
            self._flags_map[flag].add(cms_id)

        cols = [Institute.cbiCmsId, Institute.cbdCmsId, Institute.cbd2CmsId, Institute.code, Institute.cmsStatus]
        for cbi_id, cbd_id, cbd2_id, inst_code, inst_status in db.session.query(*cols).all():
            self._inst_status_map[inst_code] = inst_status
            cbi_id and self._de_facto_leaders.add(cbi_id)
            self._lead_insts_map[cbi_id] = self._lead_insts_map.get(cbi_id, set())
            self._lead_insts_map[cbi_id].add(inst_code)

            for cms_id in [x for x in [cbd2_id, cbd_id] if x]:
                self._de_facto_deputies.add(cms_id)
                self._co_lead_insts_map[cms_id] = self._co_lead_insts_map.get(cms_id, set())
                self._co_lead_insts_map[cms_id].add(inst_code)

    def get_flag_holders(self, flag_id):
        """
        :param flag_id:
        :return: set of cms ids
        """
        return self._flags_map.get(flag_id, set())

    def get_phd_clear_insts(self):
        """
        :return: set of inst codes
        """
        return self._phd_clear_insts

    def get_inst_status(self, inst_code):
        return self._inst_status_map.get(inst_code)

    def get_insts_represented_as_leader(self, cms_id):
        return self._lead_insts_map.get(cms_id, set())

    def get_insts_represented_as_deputy(self, cms_id):
        return self._co_lead_insts_map.get(cms_id, set())

    def get_all_leaders(self):
        """
        :return: set of cms_ids
        """
        return self._de_facto_leaders

    def get_all_deputies(self):
        """
        :return: set of cms_ids
        """
        return self._de_facto_deputies

    def is_inst_phd_clear(self, inst_code):
        return inst_code in self._phd_clear_insts

    def determine_correct_flag(self, inst_code, primary=True):
        flag = ''
        if self.get_inst_status(inst_code) == INST_STATUS_YES:
            if self.is_inst_phd_clear(inst_code):
                 return primary and 'CB_cbi' or 'CB_cbd'
            else:
                return primary and 'CB_ins' or 'CB_insd'
        elif self.get_inst_status(inst_code) in {INST_STATUS_COOPERATING, INST_STATUS_ASSOCIATED}:
            return primary and 'CB_cbo' or 'CB_cbod'

    def _inst_info(self, inst_code):
        return '%s %s %s' % (inst_code, self.get_inst_status(inst_code) == INST_STATUS_YES and 'is a member' or
                             'is not a member', inst_code in self.get_phd_clear_insts() and
                             'and meets required PHD count' or 'and does not meet the required PHD count')

    def _check_cb_cbi_holders(self):
        for cms_id in self.get_flag_holders('CB_cbi'):
            _all_good = False
            inst_codes = self.get_insts_represented_as_leader(cms_id)
            for inst_code in inst_codes:
                if self.get_inst_status(inst_code) == INST_STATUS_YES and inst_code in self.get_phd_clear_insts():
                    _all_good = True
                    break
            if not _all_good:
                issue = '%d has a CB_cbi flag but is not a leader of any member institute with at least 3 PHDs!' % (cms_id,)
                for inst_code in self.get_insts_represented_as_leader(cms_id):
                    issue += '\n -- ' + self._inst_info(inst_code)
                self.add_issue(issue)

    def _check_cb_cbd_holders(self):
        for cms_id in self.get_flag_holders('CB_cbd'):
            _all_good = False
            inst_codes = self.get_insts_represented_as_deputy(cms_id)
            for inst_code in inst_codes:
                if self.get_inst_status(inst_code) == INST_STATUS_YES and inst_code in self.get_phd_clear_insts():
                    _all_good = True
                    break
            if not _all_good:
                issue = '%d has a CB_cbd flag but is not a deputy leader of any member institute with at least 3 PHDs!'\
                        % (cms_id,)
                for inst_code in self.get_insts_represented_as_deputy(cms_id):
                    issue += '\n -- ' + self._inst_info(inst_code)
                self.add_issue(issue)

    def _check_cb_cbo_holders(self):
        for cms_id in self.get_flag_holders('CB_cbo'):
            _all_good = False
            inst_codes = self.get_insts_represented_as_leader(cms_id)
            for inst_code in inst_codes:
                if self.get_inst_status(inst_code) in {INST_STATUS_ASSOCIATED, INST_STATUS_COOPERATING}:
                    _all_good = True
                    break
            if not _all_good:
                issue = '%d has a CB_cbo flag but is not a leader of any observer institute!' \
                        % (cms_id,)
                for inst_code in self.get_insts_represented_as_leader(cms_id):
                    issue += '\n -- ' + self._inst_info(inst_code)
                self.add_issue(issue)

    def _check_cb_cbod_holders(self):
        for cms_id in self.get_flag_holders('CB_cbod'):
            _all_good = False
            inst_codes = self.get_insts_represented_as_deputy(cms_id)
            for inst_code in inst_codes:
                if self.get_inst_status(inst_code) in {INST_STATUS_ASSOCIATED, INST_STATUS_COOPERATING}:
                    _all_good = True
                    break
            if not _all_good:
                issue = '%d has a CB_cbod flag but is not a deputy leader of any observer institute!' \
                        % (cms_id,)
                for inst_code in self.get_insts_represented_as_deputy(cms_id):
                    issue += '\n -- ' + self._inst_info(inst_code)
                self.add_issue(issue)

    def _check_cb_ins_holders(self):
        for cms_id in self.get_flag_holders('CB_ins'):
            _all_good = False
            inst_codes = self.get_insts_represented_as_leader(cms_id)
            for inst_code in inst_codes:
                if self.get_inst_status(inst_code) == INST_STATUS_YES and inst_code not in self.get_phd_clear_insts():
                    _all_good = True
                    break
            if not _all_good:
                issue = '%d has a CB_ins flag but is not a leader of any member institute with less than 3 PHDs!' % (cms_id,)
                for inst_code in self.get_insts_represented_as_leader(cms_id):
                    issue += '\n -- ' + self._inst_info(inst_code)
                self.add_issue(issue)

    def _check_cb_insd_holders(self):
        for cms_id in self.get_flag_holders('CB_insd'):
            _all_good = False
            inst_codes = self.get_insts_represented_as_deputy(cms_id)
            for inst_code in inst_codes:
                if self.get_inst_status(inst_code) == INST_STATUS_YES and inst_code not in self.get_phd_clear_insts():
                    _all_good = True
                    break
            if not _all_good:
                issue = '%d has a CB_insd flag but is not a deputy leader of any member institute with less than 3 PHDs!'\
                        % (cms_id,)
                for inst_code in self.get_insts_represented_as_deputy(cms_id):
                    issue += '\n -- ' + self._inst_info(inst_code)
                self.add_issue(issue)

    def _check_de_facto_leaders(self):
        for cms_id in self.get_all_leaders():
            correct_flags = set()
            for inst_code in self.get_insts_represented_as_leader(cms_id):
                correct_flags.add(self.determine_correct_flag(inst_code))
            for flag in [f for f in correct_flags if f]:
                if cms_id not in self.get_flag_holders(flag):
                    self.add_issue('%d should have the %s flag!' % (cms_id, flag))

    def _check_de_facto_deputies(self):
        for cms_id in self.get_all_deputies():
            correct_flags = set()
            for inst_code in self.get_insts_represented_as_deputy(cms_id):
                correct_flags.add(self.determine_correct_flag(inst_code, primary=False))
            for flag in [f for f in correct_flags if f]:
                if cms_id not in self.get_flag_holders(flag):
                    self.add_issue('%d should have the %s flag!' % (cms_id, flag))

    def check(self):
        # checking the flags already assigned
        self._check_cb_cbi_holders()
        self._check_cb_cbd_holders()
        self._check_cb_cbo_holders()
        self._check_cb_cbod_holders()
        self._check_cb_ins_holders()
        self._check_cb_insd_holders()
        # checking if flags are in place where they should be
        self._check_de_facto_leaders()
        self._check_de_facto_deputies()

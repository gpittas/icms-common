import sqlalchemy as sa
from datetime import date, datetime
from scripts.prescript_setup import db
from scripts.watchdog.units.watchdog_base import AutonomousWatchdogBase, WatchdogRemedy
from icms_orm.cmspeople import MemberStatusValues as MSV, InstStatusValues as ISV, Person, Institute as Inst, User
from icms_orm.common import EmailMessage
from icmsutils.prehistory.prehistory_writer import PersonAndPrehistoryWriter
import re
import logging


class InfnInstitute():
    def __init__(self, code, name):
        self.code = code
        self.name = name
        self.cells = set(re.findall('(?:\()([abcdefgh])(?:\))', name))


class InfnWatchdog(AutonomousWatchdogBase):
    """
    Checking for people that should have the INFN / UNIVOTHER values set but they don't
    """

    def find_infn_insts(self) -> [InfnInstitute]:
        """
        There's no 100% reliable indicator here but instName like "%b)%" should do!
        """
        return [InfnInstitute(r.code, r.name) for r in Inst.query.filter(Inst.name.like('%b)%')).all()]

    def check(self):
        institutes = self.find_infn_insts()
        logging.debug('Found {0} institutes that come with some INFN entries'
                      .format(len(institutes)))
        for inst in institutes:
            assert isinstance(inst, InfnInstitute)
            logging.debug('{0} has the following cells: {1}'.format(
                inst.code, inst.cells))
            people = Person.query.filter(Person.status == MSV.PERSON_STATUS_CMS).filter(
                Person.instCode == inst.code).filter(Person.isAuthor == True).all()
            for person in people:
                assert isinstance(person, Person)
                assigned_cells = {c for c in (person.infn or '') + (person.univOther or '')}
                if person.infn is None or person.infn == '':
                    self.add_issue('{0} {1} from {2} has no INFN!'.format(person.firstName, person.lastName, person.instCode))
                    # for missing sub-affiliation "a" is a good default
                    self.add_remedy(MissingInfnInfoRemedy(person.cmsId, 'a', person.univOther))
                if assigned_cells.difference(inst.cells):
                    """
                    Not going through all the "consider this, consider that" stuff right now. To be TDD-ed later on if need be.
                    """
                    self.add_issue('NO SOLUTION IMPLEMENTED: {0} {1} from {2} has {3} assigned but only {4} are available!'.format(
                        person.firstName, person.lastName, person.instCode, assigned_cells, inst.cells))


class MissingInfnInfoRemedy(WatchdogRemedy):
    def __init__(self, cms_id, proper_infn='a', proper_univ_other=None):
        self.cms_id = cms_id
        self.infn = proper_infn
        self.univ_other = proper_univ_other

    def apply(self):
        p = Person.query.filter(Person.cmsId == self.cms_id).one()
        assert isinstance(p, Person)
        w = PersonAndPrehistoryWriter(
            actor_person=p, subject_person=p, db_session=Person.session())
        if self.infn != p.infn:
            w.set_new_value(Person.infn, self.infn)
        if self.univ_other != p.univOther:
            w.set_new_value(Person.univOther, self.univ_other)
        w.apply_changes(do_commit=False)

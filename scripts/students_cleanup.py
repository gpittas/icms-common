from scripts.prescript_setup import config, db
from icms_orm.cmspeople import Person, MemberActivity, PersonHistory, Institute
from icms_orm.common import InstituteLeader, Person as NewPerson
from icms_orm.common import EmailMessage
import sqlalchemy as sa
from datetime import date
import logging


class StudentEntry(object):
    def __init__(self, cms_id, first_name, last_name, days, inst_code):
        self.cms_id = cms_id
        self.first_name = first_name
        self.last_name  = last_name
        self.days_count = days
        self.inst_code = inst_code

    def __str__(self):
        return u'CMS ID {0}, {1} {2} - PHD Student since {3} years ago'.format(self.cms_id, self.first_name, self.last_name, self.days_count / 365)

    @property
    def email_string(self):
        return u'  - {0} {1}'.format(self.first_name, self.last_name)


class TeamLeaderEntry(object):
    def __init__(self, first_name, last_name, email_address):
        self.first_name = first_name
        self.last_name = last_name
        self.email_address = email_address

    def __str__(self):
        return u'{0} {1} ({2})'.format(self.first_name, self.last_name, self.email_address)

    @property
    def email_string(self):
        return u'{0} {1}'.format(self.first_name, self.last_name)


class StudentsCleanupAgent(object):
    def __init__(self, years_limit=7, member_insts_only=True):
        self._students = []
        self._member_insts_only = member_insts_only
        self._years_limit = years_limit
        self._all_entries = list()
        self._entries_by_inst = dict()
        self._team_leaders = dict()

    def _find_students(self):
        q = Person.session().query(Person).join(MemberActivity, Person.activityId == MemberActivity.id)
        if self._member_insts_only:
            q = q.join(Institute, Person.instCode == Institute.code).filter(Institute.cmsStatus.ilike('yes'))
        q = q.filter(MemberActivity.name == 'Doctoral Student').filter(Person.status.like('CMS%')).\
            join(PersonHistory).order_by(Person.instCode, Person.lastName)

        ppl = q.all()

        for person in ppl:
            assert isinstance(person, Person)
            activity_changes = person.history.get_activity_changes()
            last_change_date = activity_changes[0][1] if activity_changes else None
            days_as_phd = (date.today() - (last_change_date or person.dateCreation.date())).days
            if days_as_phd >= self._years_limit * 365:
                entry = StudentEntry(person.cmsId, person.firstName, person.lastName, days_as_phd, person.instCode)
                self._all_entries.append(entry)
                self._entries_by_inst[entry.inst_code] = self._entries_by_inst.get(entry.inst_code, list())
                self._entries_by_inst[entry.inst_code].append(entry)

    def _find_team_leaders(self):
        q = NewPerson.session().query(NewPerson.cms_id, NewPerson.email, NewPerson.first_name, NewPerson.last_name, InstituteLeader.inst_code).\
            join(InstituteLeader, InstituteLeader.cms_id == NewPerson.cms_id).\
            filter(InstituteLeader.start_date <= date.today()).\
            filter(sa.or_(InstituteLeader.end_date == None, InstituteLeader.end_date > date.today())).\
            order_by(sa.desc(InstituteLeader.is_primary), InstituteLeader.cms_id, InstituteLeader.inst_code, sa.desc(InstituteLeader.start_date)).\
            distinct(InstituteLeader.is_primary, InstituteLeader.cms_id, InstituteLeader.inst_code)
        for _cms_id, _email, _fname, _lname, _ic in q.all():
            self._team_leaders[_ic] = self._team_leaders.get(_ic, list())
            self._team_leaders[_ic].append(TeamLeaderEntry(_fname, _lname, _email))

    def _notify(self):
        _template = u"""Dear {team_leader},

In CMS, the member database is to be checked for outdated information and cleaned up. In
the course of this review, we ask all institute leaders to check the data of long-term doctoral
students and to correct it if necessary.

The following persons have been registered as CMS doctoral students for **more than {limit} years**:

{list_of_people}

You are listed as CBI for {institute}. Can you tell us about the status of the listed students and whether they are still pursuing a dissertation?

Otherwise, please update the status for the person.

**After this second emails we will change the status of the person to EXMEMBER.**

The Secretariat can help you with any changes (cms.secretariat@cern.ch).

If the information is correct and the persons are still doctoral students, we ask you to send
us a formal request for the long-term doctoral students; please send it to cms-engagement-office@cern.ch.

If you have any questions, please contact cms-engagement-office@cern.ch.

Yours sincerely
Pierluigi Paolucci
on behalf of Engagement Office
"""

        for _ic in self._entries_by_inst.keys():
            _entries = self._entries_by_inst.get(_ic)
            _leaders = self._team_leaders.get(_ic)
            if not _leaders:
                logging.warning('NO TEAM LEADERS FOUND FOR: {0}'.format(_ic))
                continue
            _content = _template.format(
                team_leader = u', '.join([_l.email_string for _l in _leaders]),
                limit = self._years_limit,
                list_of_people = u'\n'.join([_s.email_string for _s in _entries]),
                institute = _ic)
            EmailMessage.compose_message(
                sender='cms-engagement-office@cern.ch',
                to=','.join([_l.email_address for _l in _leaders]),
                subject=u'Students in need of status update', body=_content, cc=None, bcc='icms-support@cern.ch,cms-engagement-office@cern.ch',
                source_app=None, reply_to=None,
                remarks=None, db_session=EmailMessage.session()
            )

    def run(self):
        self._find_students()
        self._find_team_leaders()
        self._notify()


class StudentsCleanupPrintingAgent(StudentsCleanupAgent):
    def _notify(self):
        for _ic in self._entries_by_inst.keys():
            print(' ==== FOR INSTITUTE {0} ===='.format(_ic))
            if self._team_leaders.get(_ic):
                print(u'Team leaders: {0}'.format( str( u', '.join([ str(l) for l in self._team_leaders.get(_ic)])).encode('utf-8') ))
            else:
                logging.warning('NO TEAM LEADERS FOUND FOR: {0}'.format(_ic))
            for _stud in self._entries_by_inst.get(_ic):
                print(u'- {0}'.format( str(_stud).encode('utf-8') ))


if __name__ == '__main__':
    # agent = StudentsCleanupAgent()
    agent = StudentsCleanupPrintingAgent()
    agent.run()

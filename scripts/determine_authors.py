"""
This file will one day replace the old script resetting the author flag each night.
Goals:
- check all the CMS members
- log the changes with some information about their reasons
"""
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from sqlalchemy import or_

from email_templates.author_flag_emails import EmailTemplateAuthorshipStatusChange
from scripts.prescript_setup import db, config, ConfigProxy
from icms_orm.cmspeople import Person, MoData, PeopleFlagsAssociation, Institute, MemberActivity, User
from icms_orm.cmspeople import PERSON_STATUS_CMS, INST_STATUS_YES, PERSON_STATUS_CMSEMERITUS, PERSON_STATUS_DECEASED
from scripts.mockables import today
from scripts.fix_ex_year import ExYearFixer
from icmsutils.prehistory import PersonAndPrehistoryWriter
import logging
import sqlalchemy as sa
from icmsutils.businesslogic.flags import Flag
from datetime import date, datetime
import traceback
import sys


class Checker(object):
    def __init__(self):

        self._cms_ids = []
        self._authors = set()
        self._blocked = set()
        self._suspended = set()
        self._doctorals = set()
        self._master_mo = set()
        self._last_years_master_mo = set()
        self._phd_mo = set()
        self._free_mo = set()
        self._authors_to_become = set()
        self._authors_to_cease = set()
        self._misc_authorno = set()
        self._misc_authornomo = set()
        self._misc_authoryes = set()
        self._active_members = set()
        self._last_signing_days = dict()
        self._departure_dates = dict()
        self._affiliated_with_member_institutes = set()
        self._emeriti = set()
        self._deceased = set()
        self._remarks = dict()
        self._people_statuses = dict()
        self._author_years = dict()
        self._activitity_map = dict()

        master_mo_column = getattr(MoData, MoData.mo2011.key.replace('2011', str(today().year)))
        phd_mo_column = getattr(MoData, MoData.phdMo2011.key.replace('2011', str(today().year)))
        free_mo_column = getattr(MoData, MoData.freeMo2011.key.replace('2011', str(today().year)))

        last_master_mo_column = getattr(MoData, MoData.mo2011.key.replace('2011', str(today().year - 1)))

        queried_columns = [
            Person.cmsId, Person.isAuthor, Person.isAuthorBlock, Person.isAuthorSuspended,
            Person.activityId, sa.case([(master_mo_column.ilike('yes'), True)], else_ = False),
            sa.case([(last_master_mo_column.ilike('yes'), True)], else_=False),
            sa.case([(phd_mo_column.ilike('yes'), True)], else_=False),
            sa.case([(free_mo_column.ilike('yes'), True)], else_=False),
            # sa.case([(Person.status.ilike(PERSON_STATUS_CMS), True)], else_=False),
            # sa.case([(Person.status.ilike(PERSON_STATUS_CMSEMERITUS), True)], else_=False),
            Person.status, Person.dateEndSign, Person.exDate,
            sa.case([(Institute.cmsStatus.ilike(INST_STATUS_YES), True)], else_=False),
            sa.func.concat(*[getattr(Person, col_name) for col_name in [Person.isAuthor2009.key.replace('2009', str(x)) for x in range(2009, 2021)]]),
            MemberActivity.name
        ]
        data = db.session.query(*queried_columns).join(MoData, Person.cmsId == MoData.cmsId).\
            join(Institute, Person.instCode == Institute.code).join(MemberActivity, Person.activityId == MemberActivity.id).all()

        for cms_id, is_author, is_blocked, is_suspended, activity_id, master_mo, last_master_mo, phd_mo, free_mo, \
            status, signing_deadline, departure_date, inst_is_member, author_history_mask, activity_name in data:
            self._cms_ids.append(cms_id)
            self._people_statuses[cms_id] = status
            self._activitity_map[cms_id] = activity_name
            if is_author:
                self._authors.add(cms_id)
            if is_blocked:
                self._blocked.add(cms_id)
            if is_suspended:
                self._suspended.add(cms_id)
            if activity_id == 9:
                self._doctorals.add(cms_id)
            if master_mo:
                self._master_mo.add(cms_id)
            if last_master_mo:
                self._last_years_master_mo.add(cms_id)
            if phd_mo:
                self._phd_mo.add(cms_id)
            if free_mo:
                self._free_mo.add(cms_id)
            if inst_is_member:
                self._affiliated_with_member_institutes.add(cms_id)
            if status and status.lower() == PERSON_STATUS_CMS.lower():
                self._active_members.add(cms_id)
            elif status and status.lower() == PERSON_STATUS_CMSEMERITUS.lower():
                self._emeriti.add(cms_id)
            elif status and status.lower() == PERSON_STATUS_DECEASED.lower():
                self._deceased.add(cms_id)

            if isinstance(signing_deadline, date):
                self._last_signing_days[cms_id] = signing_deadline
            if departure_date:
                try:
                    self._departure_dates[cms_id] = datetime.strptime(departure_date, '%Y-%m-%d').date()
                except Exception as _:
                    logging.warning('Failed to parse the departure date of %s for %d' % (departure_date, cms_id))
                    logging.warning(traceback.format_exc())

            self._author_years[cms_id] = [2009 + i for i in range(0, len(author_history_mask)) if
                                                                                        author_history_mask[i] == '1']

        flags_data = PeopleFlagsAssociation.session().query(PeopleFlagsAssociation.cmsId, PeopleFlagsAssociation.flagId).\
            filter(PeopleFlagsAssociation.flagId.in_([Flag.MISC_AUTHORNO, Flag.MISC_AUTHORNOMO, Flag.MISC_AUTHORYES])).\
            all()

        for cms_id, flag_id in flags_data:
            if flag_id == Flag.MISC_AUTHORYES:
                self._misc_authoryes.add(cms_id)
            elif flag_id == Flag.MISC_AUTHORNOMO:
                self._misc_authornomo.add(cms_id)
            elif flag_id == Flag.MISC_AUTHORNO:
                self._misc_authorno.add(cms_id)

    def _add_remark(self, cms_id, remark):
        self._remarks[cms_id] = ', '.join((s for s in [self._remarks.get(cms_id, None), remark] if s))

    def _get_remarks(self, cms_id):
        return self._remarks.get(cms_id, '')

    def _get_cms_ids(self):
        return self._cms_ids

    def _get_status(self, cms_id):
        return self._people_statuses.get(cms_id, None)

    def _is_current_author(self, cms_id):
        return cms_id in self._authors

    def _is_doctoral(self, cms_id):
        return cms_id in self._doctorals

    def _has_mo(self, cms_id):
        # In the old DB each author with master mo had either free or phd mo enabled as well...
        return cms_id in self._master_mo and (cms_id in self._phd_mo or cms_id in self._free_mo)

    def _had_mo(self, cms_id, years_ago=1):
        if years_ago == 0:
            # Checking only the master MO as such would be the case e.g. for leaving emeriti
            return cms_id in self._master_mo
        elif years_ago == 1:
            return cms_id in self._last_years_master_mo
        else:
            raise ValueError('Only one year lookback is supported at the moment!')

    def _had_mo_while_leaving_cms(self, cms_id):
        if cms_id not in self._departure_dates:
            raise ValueError('No departure date found for CMS ID %d' % cms_id)
        return self._had_mo(cms_id, years_ago=(today().year - self._departure_dates.get(cms_id).year))

    def _was_author_(self, cms_id, year):
        return year in self._author_years.get(cms_id, set())

    def _is_blocked(self, cms_id):
        return cms_id in self._blocked

    def _is_suspended(self, cms_id):
        return cms_id in self._suspended

    def _has_author_no_mo_flag(self, cms_id):
        return cms_id in self._misc_authornomo

    def _has_author_yes_flag(self, cms_id):
        return cms_id in self._misc_authoryes

    def _has_author_no_flag(self, cms_id):
        return cms_id in self._misc_authorno

    def _has_author_yes_flag(self, cms_id):
        return cms_id in self._misc_authoryes

    def _is_active_member(self, cms_id):
        return cms_id in self._active_members

    def _is_emeritus(self, cms_id):
        return cms_id in self._emeriti

    def _is_deceased(self, cms_id):
        return cms_id in self._deceased

    def _get_departure_date(self, cms_id):
        return self._departure_dates.get(cms_id)

    def _get_days_since_departure(self, cms_id):
        if self._get_departure_date(cms_id):
            return (today() - self._get_departure_date(cms_id)).days
        return -1

    def _get_activity_name(self, cms_id):
        return self._activitity_map.get(cms_id) or ''

    def _has_anti_authorship_activity(self, cms_id):
        return not MemberActivity.does_activity_name_allow_signing(self._get_activity_name(cms_id))

    def _get_cms_months_since_departure(self, cms_id):
        """
        Starting the count from the first day of the month following the departure date (value 0), next month 1 etc.
        :return:
        """
        if cms_id in self._departure_dates:
            d = self._departure_dates.get(cms_id)
            count_since = date(*(d.month == 12 and [d.year+1, 1, 1] or [d.year, d.month+1, 1]))
            return max(0, (today().year - count_since.year) * 12 + today().month - count_since.month)
        return -1

    def _get_date_end_sign(self, cms_id):
        return self._last_signing_days.get(cms_id, None)

    def _can_still_sign_after_leaving(self, cms_id):
        if self._get_date_end_sign(cms_id):
            # DateEndSign takes precedence over the implicit grace period - so it has the ultimate say (when present)
            self._add_remark(cms_id, 'Date end sign: %s' % str(self._get_date_end_sign(cms_id)))
            return self._get_date_end_sign(cms_id) >= today()
        else:
            self._add_remark(cms_id, 'No DateEndSign found')
            cms_months_since_deparure = self._get_cms_months_since_departure(cms_id)
            if 0 <= cms_months_since_deparure < 12:
                self._add_remark(cms_id, 'Left CMS %d full CMS months ago' % cms_months_since_deparure)
                if self._had_mo_while_leaving_cms(cms_id):
                    self._add_remark(cms_id, 'Left with MO')
                    return True
                if self._was_author_(cms_id, self._get_departure_date(cms_id).year):
                    self._add_remark(cms_id, 'Left while signing')
                    return True
                elif self._is_doctoral(cms_id):
                    self._add_remark(cms_id, 'Left presumably as a Doctoral Student')
                    return True
                else:
                    self._add_remark(cms_id, 'Left without MO and not as a PHD Student')
            else:
                self._add_remark(cms_id, 'Left CMS %d full CMS months ago' % cms_months_since_deparure)
        return False

    def _is_affiliated_with_cms_member_institute(self, cms_id):
        return cms_id in self._affiliated_with_member_institutes

    def _is_not_affiliated_with_cms_member_institute(self, cms_id):
        return not self._is_affiliated_with_cms_member_institute(cms_id)

    def _check_individual_requirements(self, cms_id):
        """
        :param cms_id:
        :return: True or False depending on whether the person is to be an author or not
        """

        # Quick tests: first one wins and determines the outcome of entire check!
        for test_fn, result, remark in [
            (Checker._has_author_yes_flag, True, 'MISC_authoryes'),
            (Checker._has_author_no_flag, False, 'MISC_authorno'),
            (Checker._is_blocked, False, 'AuthorBlocked'),
            (Checker._is_emeritus, True, 'Has CMSEMERITUS status'),
            (Checker._is_not_affiliated_with_cms_member_institute, False, 'Member of non-member institute'),
            # Disabling the following (didn't match the state in old DB): (Checker._is_suspended, False, 'Suspended'),
            (Checker._has_anti_authorship_activity, False, 'Has an activity preventing authorship')
        ]:
            if test_fn(self, cms_id):
                self._add_remark(cms_id, remark)
                return result

        if self._is_active_member(cms_id):
            self._add_remark(cms_id, 'Is an active CMS member')
            for test_fn, remark in [
                (Checker._has_mo, 'Has MO'),
                (Checker._is_doctoral, 'Is a Doctoral Student'),
                (Checker._has_author_no_mo_flag, 'Has author_no_MO flag')
            ]:
                if test_fn(self, cms_id):
                    self._add_remark(cms_id, remark)
                    return True
        else:
            if self._can_still_sign_after_leaving(cms_id):
                self._add_remark(cms_id, 'Has the extended signing rights')
                return True
        return False

    def _schedule_for_grant(self, cms_id):
        logging.debug('Scheduling CMS ID %d for GRANT' % cms_id)
        self._authors_to_become.add(cms_id)

    def _shcedule_for_revoke(self, cms_id):
        logging.debug('Scheduling CMS ID %d for REVOCATION' % cms_id)
        self._authors_to_cease.add(cms_id)

    def _process_person(self, cms_id):
        logging.debug('Processing CMS ID %d' % cms_id)
        to_be_author = self._check_individual_requirements(cms_id)
        is_an_author = self._is_current_author(cms_id)
        if to_be_author != is_an_author:
            logging.debug('State of %d is about to change' % cms_id)
            if to_be_author:
                self._schedule_for_grant(cms_id)
            else:
                self._shcedule_for_revoke(cms_id)

    def _apply_changes(self):
        # TODO: quite ridiculous, should probably use a different history scheme
        actor_person = None
        if ConfigProxy.get_updater_cms_id_for_scripts() > 0:
            actor_person = db.session.query(Person).filter(Person.cmsId == ConfigProxy.get_updater_cms_id_for_scripts()).one()
        else:
            actor_person = Person.from_ia_dict({Person.cmsId: ConfigProxy.get_updater_cms_id_for_scripts(),
                                                Person.loginId: ConfigProxy.get_updater_login_for_scripts()})
        people_data = db.session.query(Person, User).join(User, Person.cmsId == User.cmsId).filter(
            sa.or_(Person.cmsId.in_(self._authors_to_cease), Person.cmsId.in_(self._authors_to_become))).all()

        # getting the emails of CBI, CBD, CBD2
        leader_mails = dict()
        leaders_data = db.session.query(User, Institute.code).join(Institute,
                or_(*[col == User.cmsId for col in [Institute.cbiCmsId, Institute.cbdCmsId, Institute.cbd2CmsId]]))\
                .filter(Institute.code.in_({person.get(Person.instCode) for person, _ in people_data})).all()
        for user, ic in leaders_data:
            leader_mails[ic] = leader_mails.get(ic, [])
            leader_mails[ic].append(User.get_email(user))

        logging.info(
            'Successfully retrieved the details of %d people whose authorship status is about to be flipped' % len(
                people_data))

        _do_send_mails = ConfigProxy.get_authorship_flag_check_send_emails()

        for person, user in people_data:
            assert isinstance(person, Person)
            assert isinstance(user, User)
            writer = PersonAndPrehistoryWriter(date=today(), db_session=db.session, subject_person=person,
                                               actor_person=actor_person)
            writer.set_new_value(Person.isAuthor, person.get(Person.cmsId) in self._authors_to_become)
            writer.apply_changes(do_commit=False)
            if _do_send_mails and person.status.startswith('CMS'):
                try:
                    # adding email notifications:
                    email = EmailTemplateAuthorshipStatusChange(
                        recipient_name=u'{title}{name} {surname}'.format(title=user.title and u'{title} '.format(title=user.title) or u'', name=person.firstName, surname=person.lastName),
                        recipient_email=user.get_email(),
                        team_leader_emails=' '.join(leader_mails.get(person.instCode, [])),
                        bool_has_rights=person.get(Person.isAuthor))
                    _ = [db.session.add(x) for x in email.generate_message(store_in_db=False)]
                except Exception as e:
                    logging.error('Failed to prepare email message for CMS ID %d' % person.cmsId)
                    _ = [logging.error(_line) for _line in traceback.format_stack()]
                    raise
            else:
                logging.debug('Mail notifications disabled in config.')
        db.session.commit()

    def _person_info_string(self, cms_id):
        return 'CMS ID %d [%s]' % (cms_id, ', '.join(
            [
                # self._is_current_author(cms_id) and 'Author' or 'Non-Author',
                # self._is_active_member(cms_id) and 'Member' or 'Non-Member',
                # self._is_doctoral(cms_id) and 'Doctoral Student' or 'Not a Doctoral Student',
                # self._is_suspended(cms_id) and 'Suspended' or 'Not Suspended',
                'Status: %s' % self._get_status(cms_id),
                'Remarks: %s' % self._get_remarks(cms_id)
            ]))

    def _compare_against_current_state(self):
        logging.info('- - - = = = COMPARING STATES = = = - - -')
        for cms_id in self._authors_to_cease:
            logging.info('REVOKE signing rights from %s' % self._person_info_string(cms_id))
        for cms_id in self._authors_to_become:
            logging.info('GRANT signing rights to %s' % self._person_info_string(cms_id))
        change_count = len(self._authors_to_cease) + len(self._authors_to_become)
        logging.info('- - Records to be changed: %d/%d (%.4f%%)' % (change_count, len(self._cms_ids), change_count * 1.0 / len(self._cms_ids)))
        logging.info('- - - = = = COMPARISON DONE = = = - - -')

    def run(self, only_create_model=False, force_dry_run=False):
        """
        :param only_create_model: skip the final stage (writing DB or printing summary - depending on the config)
        :param force_dry_run: forces the script to run in dry mode (read-only)
        :return:
        """
        for cms_id in self._get_cms_ids():
            self._process_person(cms_id)
        if not only_create_model:
            self._compare_against_current_state()
            if not ConfigProxy.get_authorship_flag_check_dry_run_only() and not force_dry_run:
                self._apply_changes()
            else:
                logging.info('Terminating without preserving changes - dry run enabled in config.')


if __name__ == '__main__':
    ExYearFixer().run()
    checker = Checker()
    checker.run(force_dry_run=any([x in map(str.lower, sys.argv) for x in ['dry', 'dryrun', 'dry-run']]))

import logging
import sqlalchemy as sa
from icms_orm.common import ProjectLifespan, Project, Assignment
from icms_orm.cmspeople import Project as LegacyProject, ProjectOld as LegacyProjectOld, Person as SrcPerson
from sqlalchemy import case
from scripts.prescript_setup import db
from .sync_agents_common import BaseSyncAgent
from datetime import datetime, date
from alchy import Manager


class ProjectsSyncAgent(BaseSyncAgent):
    def __init__(self, sync_broker):
        super().__init__(sync_broker)
    
    def sync(self):
        assert isinstance(db, Manager)
        pre_synced = db.session.query(Project).all()
        pre_synced_by_code = {p.code: p for p in pre_synced}
        logging.info('Found {0} pre-synced projects'.format(len(pre_synced)))

        # In the old iCMS the project definitions would sit in some ServiceProjectsName_OLD table - kind of a dumb snapshot created to hold references only.
        query = db.session.query(LegacyProjectOld.code, LegacyProjectOld.name, LegacyProjectOld.year)
        query = query.order_by(LegacyProjectOld.name, sa.desc(LegacyProjectOld.year))
        query = query.distinct(LegacyProjectOld.code)
        source_project_info = self._broker.relay(query)
        for project_code, project_name, project_year in source_project_info:
            logging.debug('Fetched a source project {0} named {1} from year {2}'.format(project_code, project_name, project_year))
            if project_code in pre_synced_by_code:
                pre_synced_project = pre_synced_by_code.get(project_code)
                assert isinstance(pre_synced_project, Project)
                if pre_synced_project.name != project_name:
                    pre_synced_project.name = project_name
                    db.session.add(pre_synced_project)
            else:
                new_project = Project.from_ia_dict({Project.code: project_code, Project.name: project_name})
                db.session.add(new_project)
        db.session.commit()


class AssignmentsSyncAgent(BaseSyncAgent):
    def __init__(self, sync_broker):
        super().__init__(sync_broker)

    def sync(self):
        pre_synced_project_codes = {_p.code.lower(): _p.code for _p in db.session.query(Project).all()}
        query = db.session.query(SrcPerson.cmsId, SrcPerson.project)
        source_assignments = {_r[0]: _r[1] for _r in self._broker.relay(query)}

        pre_synced_current_by_cms_id = dict()
        for _assignment in db.session.query(Assignment).filter(Assignment.end_date == None).all():
            assert isinstance(_assignment, Assignment)
            pre_synced_current_by_cms_id[_assignment.cms_id] = pre_synced_current_by_cms_id.get(_assignment.cms_id, list())
            pre_synced_current_by_cms_id[_assignment.cms_id].append(_assignment)

        logging.debug('Found {0} source assignments and {1} current pre-synced assignments'.format(len(source_assignments), len(pre_synced_current_by_cms_id)))

        for src_cms_id, src_project_code in source_assignments.items():
            is_up_to_date = False
            if src_cms_id in pre_synced_current_by_cms_id:
                logging.debug('Some pre-synced assignment exists for CMS ID {0}'.format(src_cms_id))
                for _assignment in pre_synced_current_by_cms_id.get(src_cms_id):
                    if _assignment.fraction == 0:
                        logging.debug('Found current default project for {0} to be {1}'.format(_assignment.cms_id, _assignment.project_code))
                        if _assignment.project_code.lower() != (src_project_code or '').lower():
                            _assignment.set(Assignment.end_date, date.today())
                            db.session.add(_assignment)
                        else:
                            is_up_to_date = True
            if not is_up_to_date:
                code_to_be_synced = pre_synced_project_codes.get(src_project_code.lower())
                if code_to_be_synced is None or code_to_be_synced == '':
                    logging.warning('For CMS ID {0} and source project code {1} no suitable project code was found!'.format(src_cms_id, src_project_code))
                else:
                    db.session.add(Assignment.from_ia_dict({
                        Assignment.cms_id: src_cms_id,
                        Assignment.start_date: date.today(),
                        Assignment.fraction: .0,
                        Assignment.project_code: code_to_be_synced,
                    }))
        db.session.commit()

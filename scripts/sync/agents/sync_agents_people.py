import logging

import sqlalchemy as sa
from icms_orm import common
from icms_orm.cmspeople import Institute as SrcInst
from icms_orm.cmspeople import Person as SrcPerson, User as SrcUser, MemberActivity as SrcActivity
from icms_orm.cmspeople import PersonHistory as SrcHistory
from icms_orm.common import InstituteLeader, Affiliation
from icms_orm.common import Person as DstPerson, PersonStatus as DstPersonStatus
from icms_orm.common import PrehistoryTimeline
from sqlalchemy import case

from icmsutils.prehistory import SupersedersResolver
from icmsutils.prehistory.prehistory_model import PrehistoryModel
from scripts.mockables import today
from scripts.prescript_setup import db
from scripts.sync.agents import CountriesSyncAgent
from .sync_agents_common import BaseSyncAgent


class PeopleSyncAgent(BaseSyncAgent):
    def create_a_new_status(self, cms_id, status, is_author, activity, author_block, epr_suspension):
        logging.info('Creating a new status "%s" for CMS ID %d holder.' % (status, cms_id))
        status = status and status.strip() or None
        return DstPersonStatus.from_ia_dict({
            DstPersonStatus.cms_id: cms_id, DstPersonStatus.status: status, DstPersonStatus.start_date: today(),
            DstPersonStatus.activity: activity, DstPersonStatus.is_author: is_author,
            DstPersonStatus.epr_suspension: epr_suspension, DstPersonStatus.author_block: author_block
        })

    def sync(self):
        ssn = db.session
        existing_data = ssn.query(DstPerson, DstPersonStatus).outerjoin(DstPersonStatus, sa.and_(
            DstPerson.cms_id == DstPersonStatus.cms_id, DstPersonStatus.end_date == None)).all()

        existing = {getattr(r[0], DstPerson.cms_id.key): r[0] for r in existing_data}
        statuses = {getattr(r[1], DstPersonStatus.cms_id.key): r[1] for r in existing_data if r[1]}
        logging.info('Found %d existing entries' % len(existing))
        # unless the mailWhere column says institute, prioritize the emailCern!

        queried = [SrcPerson.cmsId, SrcPerson.hrId, SrcPerson.loginId, SrcPerson.lastName, SrcPerson.firstName,
                   sa.func.coalesce(
                       case(whens=[(SrcUser.mailWhere.ilike('institute'), None)], else_=SrcUser.emailCern),
                       SrcUser.email1, SrcUser.email2, SrcUser.emailCern
                   ), SrcUser.sex, SrcPerson.nationality, SrcPerson.status, SrcPerson.instCode, SrcPerson.isAuthor, SrcActivity.name,
                   SrcPerson.isAuthorBlock, SrcPerson.isAuthorSuspended]

        query = ssn.query(*queried).join(SrcUser, SrcPerson.cmsId == SrcUser.cmsId).\
            join(SrcActivity, SrcActivity.id == SrcPerson.activityId, isouter=True)

        sources = {r[0]: r for r in self._broker.relay(query)}
        logging.info('Found %d source entries' % len(sources))

        inserted = [DstPerson.cms_id, DstPerson.hr_id, DstPerson.login, DstPerson.last_name, DstPerson.first_name,
                    DstPerson.email, DstPerson.gender, DstPerson.nationality]

        for cms_id, data in sources.items():
            if cms_id not in existing:
                attr_dict = {}
                for i, col in enumerate(inserted):
                    attr_dict[col] = self.__translate(col, data[i])
                ssn.add(DstPerson.from_ia_dict(attr_dict))
                ssn.flush()
                logging.info('Adding a new user with CMS ID %d' % cms_id)
            else:
                counterpart = existing[cms_id]
                changed = False
                for i, col in enumerate(inserted):
                    translation = self.__translate(col, data[i])
                    if translation != getattr(counterpart, col.key):
                        setattr(counterpart, col.key, translation)
                        changed = True
                if changed:
                    logging.info('Entry for CMS ID %d has changed! Updating...' % cms_id)
                    ssn.add(counterpart)
                    ssn.flush()

            # sync the status
            fetched_status = data[queried.index(SrcPerson.status)] or None
            fetched_is_author = data[queried.index(SrcPerson.isAuthor)]
            fetched_activity = data[queried.index(SrcActivity.name)]
            fetched_suspended = data[queried.index(SrcPerson.isAuthorSuspended)]
            fetched_blocked = data[queried.index(SrcPerson.isAuthorBlock)]
            if cms_id not in statuses:
                ssn.add(self.create_a_new_status(cms_id, fetched_status, fetched_is_author, fetched_activity,
                                                 fetched_blocked, fetched_suspended))
            else:
                current_status_entry = statuses.get(cms_id)
                if any([current_status_entry.get(_col) != _fetched for _fetched, _col in [
                    (fetched_activity, DstPersonStatus.activity), (fetched_blocked, DstPersonStatus.author_block),
                    (fetched_is_author, DstPersonStatus.is_author), (fetched_status, DstPersonStatus.status),
                    (fetched_suspended, DstPersonStatus.epr_suspension)
                ]]):
                    current_status_entry.set(DstPersonStatus.end_date, today())

                    ssn.add(statuses.get(cms_id))
                    ssn.add(self.create_a_new_status(cms_id, fetched_status, fetched_is_author, fetched_activity,
                                                     fetched_blocked, fetched_suspended))
        ssn.commit()

    def __translate(self, dst_column_type, src_value):
        if dst_column_type == DstPerson.gender:
            return {'F': common.GenderValues.FEMALE, 'M': common.GenderValues.MALE}.get(src_value and src_value.upper(), None)
        elif dst_column_type == DstPerson.nationality:
            # trying to express the old DB's nationality info through a reference to new DB's country table
            return CountriesSyncAgent.resolve_country(src_value)
        return src_value


class AffiliationsSyncAgent(BaseSyncAgent):
    def sync(self):
        synced = {r.cms_id: r for r in db.session.query(Affiliation).filter(Affiliation.end_date == None)}
        source = {r[0]: r[1] for r in self._broker.relay(db.session.query(SrcPerson.cmsId, SrcPerson.instCode)) if r[0] is not None}
        logging.info('Found %d affiliation records to cross chceck with pre-synchronised data' % len(source))
        for cms_id in source:
            old_record = synced.get(cms_id, None)
            if old_record is None or old_record.inst_code != source[cms_id]:
                new_record = Affiliation.from_ia_dict({
                    Affiliation.cms_id: cms_id,
                    Affiliation.is_primary: True,
                    Affiliation.inst_code: SupersedersResolver.get_present_value(SrcInst.code, source[cms_id]),
                    Affiliation.start_date: today()
                })
                db.session.add(new_record)
                if cms_id in synced:
                    synced[cms_id].date_end = today()
                    db.session.add(synced[cms_id])
            if old_record and old_record.inst_code != source[cms_id]:
                # closing the previous affiliation entry
                setattr(old_record, Affiliation.end_date.key, today())
                db.session.add(old_record)

        db.session.commit()


class PrehistorySyncAgent(BaseSyncAgent):

    @staticmethod
    def are_sets_of_timelines_identical(set_a, set_b):
        empty_count = sum(not x for x in [set_a, set_b])
        if empty_count == 2:
            logging.debug('TL sets identical (both empty)')
            return True
        elif empty_count == 1:
            logging.debug('Only one TL set is empty (so they differ)')
            return False

        if len(set_a) != len(set_b):
            logging.debug('Different length of time-line-sets')
            return False
        else:
            # probably same length, need element-by-element comparison...
            for el_a, el_b in zip(set_a, set_b):
                for field in [PrehistoryTimeline.start_date, PrehistoryTimeline.end_date, PrehistoryTimeline.cms_id,
                              PrehistoryTimeline.status_cms, PrehistoryTimeline.inst_code,
                              PrehistoryTimeline.activity_cms]:
                    # set of unique elements should have a length one if these elements are identical
                    if len({el.get(field) for el in [el_a, el_b]}) != 1:
                        logging.debug('Time-lines seem to differ wrt value of %s' % field.key)
                        return False
        logging.debug('Time-line sets seem to be identical')
        return True

    def sync(self):
        # get the sources and sort them by CMS IDs
        sources = {x.get(SrcHistory.cmsId) : x for x in self._broker.relay(db.session.query(SrcHistory))}
        people = {p.get(SrcPerson.cmsId): p for p in SrcPerson.session().query(SrcPerson).filter(SrcPerson.cmsId.in_(sources.keys())).all()}
        pre_synced = PrehistoryTimeline.session().query(PrehistoryTimeline).filter(PrehistoryTimeline.cms_id.in_(sources.keys())).order_by(PrehistoryTimeline.start_date, PrehistoryTimeline.id).all()
        sorted_pre_synced = dict()
        logging.debug('Found %d pre-synced time lines for %d relevant CMS ids' % (len(pre_synced), len(sources)))
        for h in pre_synced:
            sorted_pre_synced[h.cms_id] = sorted_pre_synced.get(h.cms_id, [])
            sorted_pre_synced[h.cms_id].append(h)
        pre_synced = sorted_pre_synced

        for cms_id in sources.keys():
            pm = PrehistoryModel(cms_id=cms_id, person=people.get(cms_id), history=sources.get(cms_id))
            possible_replacement_time_lines = pm.get_prehistory_time_lines()
            existing_time_lines = pre_synced.get(cms_id)
            if PrehistorySyncAgent.are_sets_of_timelines_identical(existing_time_lines, possible_replacement_time_lines):
                logging.debug('Prehistory time lines sets are already up to date for CMS ID %d' % cms_id)
                continue
            else:
                logging.debug('Going to update prehistory time lines for CMS ID %d' % cms_id);
                # ignore output, but execute the list comprehension
                _ = [PrehistoryTimeline.session().delete(tl) for tl in existing_time_lines or []]
                _ = [PrehistoryTimeline.session().add(tl) for tl in possible_replacement_time_lines or []]
        PrehistoryTimeline.session().commit()


from scripts.prescript_setup import db, ConfigProxy
from .agents import *
from .helpers import ForwardSyncManager
import logging


def launch(agent_classes=None):
    if not agent_classes:
        subclassess = BaseSyncAgent.__subclasses__()
        for subclass in subclassess:
            for subsub in subclass.__subclasses__():
                subclassess.append(subsub)
        subclass_names = [s.__name__ for s in subclassess]
        logging.debug('Discovered the following SyncAgent implementations: %s' % str(subclass_names))
        configured_agent_names = ConfigProxy.get_sync_agent_class_names()
        logging.debug('Program configured to use the following SA implementations: %s' % str(configured_agent_names))
        agent_classes = [eval(x) for x in configured_agent_names if x in subclass_names]
    ForwardSyncManager.launch_sync(agent_classes_override=agent_classes)

"""
This should execute just once upon a launch of any script
"""
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import os
import sys
import re
from configparser import RawConfigParser, ConfigParser, ExtendedInterpolation
from icms_orm.orm_interface import OrmManager as Manager
import icms_orm
import logging
from datetime import datetime
from icmscommon import DbGovernor, ConfigGovernor
import socket

# so that the scripts can be called with the top directory as CWD
sys.path.append('/'.join(os.path.realpath(__file__).split('/')[:-2]))

# set the root dir
root_dir = __file__.split('scripts')[0]

# read the config
config = ConfigParser(interpolation=ExtendedInterpolation())
config_files = [root_dir + _fname for _fname in ('config.ini', os.environ.get('ICMS_COMMON_CONFIG_OVERRIDE', 'config_override.ini'))]
config.read(config_files)

# Determine what we're running here and put it in the log name
script_name = 'unknown'
if sys.argv and '.py' in sys.argv[0]:
    script_name = sys.argv[0].split('/')[-1].split('.py')[0]

time_string = datetime.now().strftime('%Y-%m-%d_%H:%M:%S')


# SET UP LOGGING
if not config.has_option('logging', 'format'):
    config.set('logging', 'format', '%(asctime)s [%(levelname)s]: %(message)s (%(module)s.%(funcName)s:%(lineno)d)')

if not config.has_option('logging', 'level'):
    config.set('logging', 'level', 'DEBUG')

if not config.has_option('logging', 'enable_console'):
    config.set('logging', 'enable_console', 'yes')

log_format = config['logging']['format']
level = config['logging']['level']
enable_console = config.getboolean('logging', 'enable_console')
filename = None if not config.has_option('logging','filename') else config['logging']['filename']
filename = filename and '%s_%s_%s' % (filename, script_name, time_string) or None

root_logger = logging.getLogger()
root_logger.setLevel(level)
handler = logging.StreamHandler()
logging_formatter = logging.Formatter(fmt=config['logging']['format'])
handler.setFormatter(logging_formatter)
root_logger.addHandler(handler)

if filename:
    file_handler = logging.FileHandler(filename, 'w')
    file_handler.setFormatter(logging.Formatter(fmt=config['logging']['format']))
    root_logger.addHandler(file_handler)
logging.info('Launching script %s on %s', script_name, socket.gethostname())

# SET UP DB CONNECTIONS
icms_orm.toolkit_schema_name.override(config['db']['toolkit_schema_name'])
icms_orm.epr_schema_name.override(config['db']['epr_schema_name'])
icms_orm.cms_common_schema_name.override(config['db']['cms_common_schema_name'])
icms_orm.cms_people_schema_name.override(config['db']['cms_people_schema_name'])
icms_orm.cadi_schema_name.override(config['db']['cadi_schema_name'])
icms_orm.old_notes_wf_schema_name.override(config['db']['old_notes_wf_schema_name'])
icms_orm.old_notes_schema_name.override(config['db']['old_notes_schema_name'])
icms_orm.metadata_schema_name.override(config['db']['metadata_schema_name'])
icms_orm.epr_role_name.override(re.findall('://(\w+)[:@]', config['db']['epr'])[0])
icms_orm.icms_reader_role.override(config['db']['icms_reader_role'])
icms_orm.cms_common_role_name.override(re.findall('://(\w+)[:@]', config['db']['cms_common'])[0])


def create_db_manager(main_db_bind_uri=config['db'].get(config['db'].get('main_bind', 'cms_common'))) -> Manager:
    db_binds_map = {
        icms_orm.epr_bind_key(): config['db']['epr'],
        icms_orm.cms_common_bind_key(): config['db']['cms_common'],
        icms_orm.toolkit_bind_key(): config['db']['toolkit'],
        icms_orm.cms_people_bind_key(): config['db']['legacy'],
        icms_orm.cadi_bind_key(): config['db']['legacy'],
        icms_orm.metadata_bind_key(): config['db']['legacy'],
        icms_orm.news_bind_key(): config['db']['legacy']
    }

    return Manager(
        config={
            'SQLALCHEMY_BINDS': db_binds_map,
            'SQLALCHEMY_DATABASE_URI': main_db_bind_uri
        }, 
        session_options={'autoflush': False, 'autocommit': False}
    )


db = create_db_manager()


class ConfigProxy(object):
    """
    A simple class exposing config options through getters
    """
    @classmethod
    def get_raw_config(cls):
        return config

    @classmethod
    def get_updater_cms_id_for_scripts(cls):
        return cls.get_raw_config().getint('scripts', 'updater_cms_id')

    @classmethod
    def get_updater_login_for_scripts(cls):
        return cls.get_raw_config().get('scripts', 'updater_login')

    @classmethod
    def get_suspend_timed_out_applicants(cls):
        return cls.get_raw_config().getboolean('scripts', 'auto_suspend_applicants_past_deadline')

    @classmethod
    def get_email_sender(cls):
        return cls.get_raw_config().get('mailing', 'sender')

    @classmethod
    def get_watchdog_report_to(cls):
        return cls.get_raw_config().get('scripts', 'watchdog_report_to')

    @classmethod
    def get_watchdog_epr_sync_grace_period(cls):
        """
        :return: Number of days that can go without an update to EPR timelines before an alarm is raised
        """
        return cls.get_raw_config().getint('scripts', 'watchdog_epr_sync_grace_period')

    @classmethod
    def get_watchdog_classes_to_run(cls):
        return cls.get_raw_config().get('scripts', 'watchdog_classes_to_run').split('|')

    @classmethod
    def get_watchdog_auto_fix_issues(cls):
        return cls.get_raw_config().get('scripts', 'watchdog_auto_fix_issues')

    @classmethod
    def get_epr_force_import_cms_ids(cls):
        return {int(x) for x in cls.get_raw_config().get('scripts', 'epr_force_import_users').split('|') if x.isdigit()}

    @classmethod
    def get_sync_agent_class_names(cls):
        return [s.strip() for s in cls.get_raw_config().get('sync', 'agent_classes').split('|')]

    @classmethod
    def get_sync_broker_class_name(cls):
        return cls.get_raw_config().get('sync', 'fwd_sync_broker_class').strip()

    @classmethod
    def set_sync_broker_class_name(cls, value):
        cls.get_raw_config().set('sync', 'fwd_sync_broker_class', value=value)

    @classmethod
    def get_authorship_flag_check_dry_run_only(cls):
        return cls.get_raw_config().getboolean(section='scripts', option='determine_authors_dry_run')

    @classmethod
    def get_backsync_dry_run_only(cls):
        return cls.get_raw_config().getboolean(section='sync', option='backsync_dry_run')

    @classmethod
    def get_authorship_flag_check_send_emails(cls):
        return cls.get_raw_config().getboolean(section='scripts', option='determine_authors_send_mails')

    @classmethod
    def get_prehistory_auto_apply_fixes(cls):
        return cls.get_raw_config().getboolean(section='scripts', option='prehistory_auto_apply_fixes')

    @classmethod
    def get_active_papers_properties_path(cls):
        return cls.get_raw_config().get('scripts', 'active_papers_properties_path')

    @classmethod
    def get_active_papers_properties_remote_path(cls):
        return cls.get_raw_config().get('scripts', 'active_papers_properties_remote_path')

    @classmethod
    def get_active_papers_properties_remote_host(cls):
        return cls.get_raw_config().get('scripts', 'active_papers_properties_remote_host')

    @classmethod
    def get_epr_aggregates_earliest_update_year(cls):
        return int(cls.get_raw_config().get('scripts', 'epr_aggregates_earliest_update_year', fallback='2000'))

# Set them governors!
DbGovernor.set_db_manager_getter(lambda: db)


class ScriptConfigGovernor(ConfigGovernor):

    def __init__(self, config: RawConfigParser):
        self._config = config

    def _check_test_mode(self):
        return self._config['main']['testing'] == 'enabled'

    def _get_db_owner_pass(self):
        return self._config['db']['db_owner_pass']

    def _get_db_owner_user(self):
        return self._config['db']['db_owner_user']

    def _get_epr_db_string(self):
        return self._config['db']['epr']

    def _get_db_string(self, db):
        return self._config['db'][db]


ConfigGovernor.set_instance(ScriptConfigGovernor(config=config))

logging.debug('iCMS-common pre-script setup\'s end. Root directory: {0}, filenames: {1}'.format(root_dir, config_files))